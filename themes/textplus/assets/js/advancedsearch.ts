import { Search, useDialogStore, createApp } from "textplus-fcs-vuetify";

createApp(Search).mount("#advanced-search");

// expose open dialog functionality to vanillaJS
const dialog = useDialogStore();
const openDialog = dialog.open;
const switchLocale = dialog.switchLocale;
const advancedsearch = { openDialog, switchLocale };
Object.assign(window, { advancedsearch });

// switch to language for dialog based on <html lang="de">
const language = document.documentElement.lang;
if (!!language) {
  advancedsearch.switchLocale(language);
}

// bind to button(s)
const setupSearchButton = () => {
  const searchButtons = document.getElementsByClassName("btn-search");
  for (var button of searchButtons) {
    button?.addEventListener("click", () => {
      advancedsearch.openDialog()
    })
  }

  const dataSearchButtons = document.getElementsByClassName("btn-data-search");
  for (var button of dataSearchButtons) {
    button?.addEventListener("click", () => {
      advancedsearch.openDialog({tab: 'content'})
    })
  }
}

setupSearchButton();

// instant search box opening on initial page opening using hash url
const checkURlAndOpenSearchModal = () => {
  const hashUrl = document.location.hash.substring(1)
  if (!hashUrl) return;
  if (hashUrl === "action-open-search-content") {
    advancedsearch.openDialog({tab: 'content'})
  } else if (hashUrl === "action-open-search-website") {
    advancedsearch.openDialog({tab: 'website'})
  }
}

checkURlAndOpenSearchModal();
