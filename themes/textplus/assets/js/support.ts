import { getCookie,setCookie } from "./util";

const endpoint = "9751f4bc-0258-4a5f-b276-1f3fa5d9fc0f";
const supportBackendUrl = "https://api.micros.academiccloud.de/support/api/v1/request"
// const supportBackendUrl = "https://stage.micros.gwdg.de/supports/api/v1/request"


// TODO: i18n in error messages
const getFreshApiToken = async (endpoint: string): Promise<string | undefined> => {
    return fetch(
        supportBackendUrl + "/token/" + endpoint, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => {
            console.log(response)
            if (response.status == 200) return response.text()
            else if (response.status == 429) {
                showErrorMessage('{{ i18n "support-form-error-unable-to-load-token" }}')
                return undefined
            }
        })
        .catch (error => {
            showErrorMessage('{{ i18n "support-form-error-default-message" }}')
            return undefined
        })
}

const sendSupportRequest = async (endpoint: string, apiToken: string, name: string, mail: string, subject: string, message: string) => {
    fetch(supportBackendUrl + "/public/" + endpoint, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-API-Key': apiToken
        },
        body: JSON.stringify(
            {
                "fullname": name,
                "emailAddress": mail,
                "requestTitle": subject,
                "requestBody": message
            })
    })
        .then(response => {
            if (response.status == 201) {
                // success
                showSuccessMessage();
            }
            else if (response.status == 401) {
                // expired token, request new one
                updateApiCookie(endpoint)
                showErrorMessage('{{ i18n "support-form-error-token-invalid" }}')
            }
            else if (response.status == 429) {
                // too many requests (limit is 2 requests per 3 mins)
                showErrorMessage('{{ i18n "support-form-error-too-many-requests" }}')
            }
            else if (response.status == 500) {
                // server error
                showErrorMessage('{{ i18n "support-form-error-server-error" }}')
            }
        })
        .catch (error => {
            showErrorMessage('{{ i18n "support-form-error-default-message" }}')
            return undefined
        })
}

const initApiTokenCookie = async (endpoint: string) => {
    const cookiename = "apitoken_" + endpoint;

    // check if cookie set (and valid) otherwise obtain 
    if (getCookie(cookiename).length == 0) {
        updateApiCookie(endpoint)
    }

    // update cookie every 4 minutes to make sure it is always valid
    setInterval(() => updateApiCookie(endpoint), 4 * 60 * 1000)
}

const updateApiCookie = async (endpoint: string) => {
    const cookiename = "apitoken_" + endpoint;
    const cookievalue = await getFreshApiToken(endpoint);
    const timevalid = 300;

    if (cookievalue !== undefined) {
        // write delayed because token gets valid after 10 seconds
        setTimeout(() => setCookie(cookiename, cookievalue, timevalid), 10 * 1000);
    }
}

const showErrorMessage = (message: string) => {
    hideSuccessMessage();
    const errorMessageParent = document.getElementById("support-form-error-field") as HTMLElement;
    errorMessageParent.classList.remove("d-none")
    errorMessageParent.textContent = message
}

const showSuccessMessage = () => {
    const successMessageParent = document.getElementById("support-form-success-field") as HTMLElement;
    successMessageParent.classList.remove("d-none")
    successMessageParent.textContent = '{{ i18n "support-form-message-submitted" }}'
}

const hideSuccessMessage = () => (document.getElementById("support-form-success-field") as HTMLElement).classList.add("d-none")

const hideErrorMessage = () => (document.getElementById("support-form-error-field") as HTMLElement).classList.add("d-none")

const validateFormInput = (name: string, mail: string, message: string, dppAccepted: boolean, eventTitle?: string, eventDateTime?: string, eventLocation?: string): string => {
    let errorMessages = Array<string>();
    const simpleEmailRe = /^\S+@\S+\.\S+$/; // see https://stackoverflow.com/a/9204568

    if (name.length < 5 || name.length > 200) {
        errorMessages.push('{{ i18n "support-form-error-name-invalid" }}')
    }

    if (!simpleEmailRe.test(mail)) {
        errorMessages.push('{{ i18n "support-form-error-mail-invalid" }}')
    }

    if (message != undefined && message == "") {
        errorMessages.push('{{ i18n "support-form-error-message-empty" }}')
    }

    if (eventTitle != undefined && eventTitle == "") {
        errorMessages.push('{{ i18n "support-form-error-event-title-empty" }}')
    }
    if (eventDateTime != undefined && eventDateTime == "") {
        errorMessages.push('{{ i18n "support-form-error-event-date-time-empty" }}')
    }

    if (eventLocation != undefined && eventLocation == "") {
        errorMessages.push('{{ i18n "support-form-error-event-location-empty" }}')
    }

    if (!dppAccepted) {
        errorMessages.push('{{ i18n "support-form-error-dpp-not-accepted" }}')
    }

    return errorMessages.join(" ")
}


const submitForm = (name: string, mail: string, subject: string, message: string) => {
    const apiToken = getCookie('apitoken_' + endpoint)
    if (apiToken.length > 0) {
        hideErrorMessage();
        sendSupportRequest(endpoint, apiToken, name, mail, subject, message);
    }
    else {
        showErrorMessage('{{ i18n "support-form-error-token-invalid" }}')
    }
}


const setupSupportForm = () => {
    const supportFormSendButton = document.getElementById("support-form-send-button");
    supportFormSendButton?.addEventListener("click", () => {
        const name = (document.getElementById("support-form-name") as HTMLInputElement).value;
        const mail = (document.getElementById("support-form-email") as HTMLInputElement).value;
        const subject = (document.getElementById("support-form-textplus-area") as HTMLInputElement).value;
        const message = (document.getElementById("support-form-message") as HTMLInputElement).value;
        const dppAccepted = (document.getElementById("support-form-privacy-policy-box") as HTMLInputElement).checked;

        const errorMessage = validateFormInput(name, mail, message, dppAccepted);
        if (errorMessage == "") {
            submitForm(name, mail, subject, message);
        }
        else {
            showErrorMessage(errorMessage)
        }
    });
}

const setupEventForm = () => {
    const eventFormSendButton = document.getElementById("event-form-send-button");
    eventFormSendButton?.addEventListener("click", () => {
        const name = (document.getElementById("event-form-name") as HTMLInputElement).value;
        const mail = (document.getElementById("event-form-email") as HTMLInputElement).value;
        const eventTitle = (document.getElementById("event-form-title") as HTMLInputElement).value;
        const eventDateTime = (document.getElementById("event-form-date-time") as HTMLInputElement).value;
        const eventDesc = (document.getElementById("event-form-desc") as HTMLInputElement).value;
        const eventLocation = (document.getElementById("event-form-location") as HTMLInputElement).value;
        const eventUrl = (document.getElementById("event-form-url") as HTMLInputElement).value;
        const eventImageAvailable = (document.getElementById("event-form-image-usable") as HTMLInputElement).checked;
        const dppAccepted = (document.getElementById("event-form-privacy-policy-box") as HTMLInputElement).checked;

        const errorMessage = validateFormInput(name, mail, eventDesc, dppAccepted, eventTitle, eventDateTime, eventLocation);
        if (errorMessage == "") {
            // TODO: consider creating correct markdown from form for copy-paste into repo (pay attention to date formatting)
            const subject = "Neue Veranstaltung: " + eventTitle;
            const message = "Titel der Veranstaltung: " + eventTitle + "\n" +
                "Datum und Uhrzeit: " + eventDateTime + "\n" +
                "Ort: " + eventLocation + "\n" +
                "URL: " + eventUrl + "\n" +
                "Bild vorhanden auf Infoseite/kann bereit gestellt werden: " + (eventImageAvailable ? "Ja" : "Nein") + "\n" +
                "Beschreibung: " + eventDesc;

            submitForm(name, mail, subject, message);
        }
        else {
            showErrorMessage(errorMessage)
        }
    });
}

const setupForms = () => {
    // const subject -> requestTitle/queue (endpoint) (maybe create seperate field for subject, additional to drop-down for queue)
    initApiTokenCookie(endpoint);
    setupSupportForm();
    setupEventForm();
}

setupForms();