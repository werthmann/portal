import "js/bootstrap.bundle.min.js";

// @ts-ignore: process.env.HUGO_ENVIRONMENT is replaced in the head.html
const isDev: boolean = process.env.HUGO_ENVIRONMENT === "development";

/**
 * Logs a message to the console if we are in development mode.
 */
const logInfo = (...msg: any[]) => {
    if (isDev) console.log(...msg);
};

/**
 * Navigates to another URL.false
 * @param url The URL where the browser should navigate to.
 */
const navigateTo = (url: string) => (window.location.href = url);


// TODO: adopt to redesign
const setupLanguageSwitcher = () => {
    const switcher = document.getElementById("language-switcher");
    const inputs = switcher?.getElementsByTagName("input");

    if (inputs === undefined) {
        console.error("could not find radio buttons for language switching");
        return;
    }

    for (const radio of inputs) {
        radio.addEventListener("change", () => navigateTo(radio.dataset.navigateTo ?? ""));
    }
};
setupLanguageSwitcher();


const setUpScrollingNav = () => {
    let navigation = document.querySelector(".nav-medium");
    window.addEventListener("scroll", () => {
        if (document.body.scrollTop > 140 || document.documentElement.scrollTop > 140) {
            navigation?.classList.add("fadein");
            navigation?.classList.remove("d-none");
        } else {
            navigation?.classList.add("d-none");
        }
    });

};

setUpScrollingNav();