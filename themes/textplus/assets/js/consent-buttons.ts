import { getCookie, setCookie } from "./util";

const YT_COOKIE_NAME = "allow_youtube_gdpr"
const TWITTER_COOKIE_NAME = "allow_twitter_gdpr"

const loadConsentFromCookies = () => {
    if (getCookie(YT_COOKIE_NAME)) {
        showYoutube();
    }
    if (getCookie(TWITTER_COOKIE_NAME)) {
        showTwitter();
    }
}

const hideElements = (elements: HTMLCollection) => {
    for (var element of elements) {
        element.classList.add("d-none")
    }
}

const showElements = (elements: HTMLCollection) => {
    for (var element of elements) {
        element.classList.remove("d-none")
    }
}

const setSrcAfterConsent = (elements: HTMLCollection) => {
    for (var element of elements) {
        const src = element.getAttribute("no-consent-src")
        if (src !== null) {
            element.setAttribute("src", src)
        }
    }
}

const setupConsentButtons = () => {
    const youtubeConsentBanner = document.getElementsByClassName("youtube-consent-banner");
    for (var banner of youtubeConsentBanner) {
        const consentButtons = banner.getElementsByTagName("input")
        for (var button of consentButtons) {
            button?.addEventListener("click", () => {
                showYoutube()
                setCookie(YT_COOKIE_NAME, "true")
            })
        }
    }

    const twitterConsentBanner = document.getElementsByClassName("twitter-consent-banner");
    for (var banner of twitterConsentBanner) {
        const consentButtons = banner.getElementsByTagName("input")
        for (var button of consentButtons) {
            button?.addEventListener("click", () => {
                showTwitter()
                setCookie(TWITTER_COOKIE_NAME, "true")
            })
        }
    }
}

// TODO: try to generalize to include twitter handling
const showYoutube = () => {
    const youtubeEmbed = document.getElementsByClassName("youtube-embed")
    for (var div of youtubeEmbed) {
        const youtubeIframeContainers = div.getElementsByClassName("youtube-iframe-container")
        const youtubeIframes = div.getElementsByClassName("youtube-iframe")

        // move URL from no-consent-src to src so that iframes will be loaded properly now.
        setSrcAfterConsent(youtubeIframes)
        showElements(youtubeIframeContainers);
    }

    // hide consent buttons
    hideElements(document.getElementsByClassName("youtube-consent-banner"));
}

const showTwitter = () => {
    const twitterEmbedDivs = document.getElementsByClassName("twitter-embed")
    for (var div of twitterEmbedDivs) {
        const twitterScriptElements = div.getElementsByTagName("script")
        const twitterEmbedDivs = div.getElementsByClassName("twitter-gdpr-embed")

        // move URL from no-consent-src to src so that iframes will be loaded properly now.
        setSrcAfterConsent(twitterScriptElements)
        showElements(twitterEmbedDivs);
    }

    // hide consent buttons
    hideElements(document.getElementsByClassName("twitter-consent-banner"));
}

loadConsentFromCookies();
setupConsentButtons();