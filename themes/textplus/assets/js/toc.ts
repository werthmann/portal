// auto scrolling TOC
const setUpScrollingToc = () => {
    let sticky = document.getElementById("toc-wrapper")
    if (sticky) {
        let stickyAnchor = sticky.parentNode as Element
        let state = false

        function getAnchorOffset() {
            return stickyAnchor.getBoundingClientRect().top
        }

        const updateSticky = () => {
            // We need to get the width of the toc div and apply it to the
            // sticky class to prevent a change in width when postion:sticky is set
            let tocWrapper = document.getElementById("toc-wrapper")

            if (sticky && tocWrapper) {
                var tocDivWidth = tocWrapper.offsetWidth
                if (!state && getAnchorOffset() < 51) {
                    sticky.classList.add("is-sticky")
                    sticky.style.width = tocDivWidth + "px"
                    state = true
                } else if (state && getAnchorOffset() >= 51) {
                    sticky.classList.remove("is-sticky")
                    sticky.style.width = ""
                    state = false
                }
            }
        }

        window.addEventListener("scroll", updateSticky)
        window.addEventListener("resize", updateSticky)

        updateSticky()
    }
}

// observe current section and update TOC

const clearActiveStatesInTableOfContents = () => {
    document.querySelectorAll("nav li a").forEach((section) => {
        section.setAttribute("aria-current", "false");
    });
}

const setUpToc = () => {
    const observerForTableOfContentActiveState = new IntersectionObserver((entries) => {
        entries.forEach((entry) => {
            const id = entry.target.getAttribute("id");
            if (entry.intersectionRatio > 0) {
                clearActiveStatesInTableOfContents();
                document.querySelector(`nav li a[href="#${id}"]`)?.setAttribute("aria-current", "true");
            }
        });
    });

    document.querySelectorAll("h2[id]").forEach((section) => {
        observerForTableOfContentActiveState.observe(section);
    });
}

setUpScrollingToc()
setUpToc();