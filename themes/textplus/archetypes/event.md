---
title: "{{ replace .Name "-" " " | title }}"
start_date: {{ .Date }}
end_date:  {{ .Date }}
all_day: false

type: event
---
