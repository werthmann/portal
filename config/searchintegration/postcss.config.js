const prefixer = require("postcss-prefix-selector");

module.exports = {
  plugins: [
    prefixer({
      prefix: "#advanced-search",
      // html, body, *, ::before/::after
      // /\.v-/
      transform: function (prefix, selector, prefixedSelector, filePath, rule) {
        // keep same if nested or with attribute or id or vuetify-prefix
        if (/>|\[|^\.v-|#/.test(selector)) return selector;
        // remove global by prefixing nonsense
        if (/\*/.test(selector)) return `#totalnonse > ${selector}`;
        // if from bootstrap
        if (/^\.border$/.test(selector)) return prefixedSelector;
        let ruleContent = rule.toString();
        // contains variable
        if (/--v-/.test(ruleContent)) return selector;
        // remove certain reset rules for buttons
        if (/button/.test(selector)) {
          // `font: inherit;`
          if (/font:\s*inherit/.test(ruleContent)) return `#totalnonse > ${selector}`;
          // `text-transform: none;`
          if (/text-transform:\s*none/.test(ruleContent)) return `#totalnonse > ${selector}`;
          // border/background reset
          if (/background-color:\s*transparent/.test(ruleContent) && /border-style:\s*none/.test(ruleContent)) return `#totalnonse > ${selector}`;
        }
        // default with prefix
        return prefixedSelector;
      },
    }),
    // add some overrides to fix stuff
    //(function (root) {
    //  root.append(` /* put your stuff here */ `)
    //}),
  ],
};
