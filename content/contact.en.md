---
title: "Contact"

aliases:
- /contact

---

# Contact and Support

For individual support and advice on all services and tools offered by Text+, as well as additional offerings, our Helpdesk is at your disposal. It provides guidance in all phases of the data lifecycle: from assistance in developing specific data management plans for new research projects in the Text+ data domains to the "FAIRification" and "CAREification" of existing data sets.

Our Helpdesk consists of a diverse network of experts from the applying and participating institutions of Text+. They offer their expertise in the three data domains as well as in the Infrastructure/Operation area. Depending on your needs, we are also happy to connect you with national and international associations and alliances.

To get in touch, you can use our web form. Your request will be promptly forwarded to the staff of the Helpdesk. If you do not receive a confirmation of your request, please send an email to: `textplus-support[at]gwdg.de`

(Please note that for security reasons, the form can only be submitted after ten seconds and that this can be done a maximum of twice within five minutes. Otherwise, it will trigger an error message.)

## Contact Us
{{<support-form>}}

### The Text+ Helpdesk is Diverse
#### General
Answers general and cross-project enquiries and provides information on how to participate in Text+.

#### Data Domain Collections
Provides support for language- and text-based collection data, e.g. advice on the digitization of analog text and language objects as well as text and data mining. Advice on collection-specific services and data management plans.

#### Data Domain Lexical Resources
Provides support for lexical resources, e.g. standards-compliant digitization and data curation of legacy collections, and empirical resource use. Advice on specific services and data management plans for lexical resources.

#### Data Domain Editions
Provides support for all types of editions from various research fields and all phases of the editorial process. Offers advice on edition-specific offerings and data management plans.

#### Infrastructure/Operation
Support for infrastructure offerings and cross-cutting issues such as data quality, interoperability, long-term archival, re-usability, registries, and linked open data.

#### Technical Support
Provides technical assistance to our service portfolio.

#### Legal and Ethical
Provides information on legal and ethical issues in text- and language-based research, such as copyright (no legal counseling).
