---
title: "FAIR principles in the Humanities"

type: user-story

slug: user-story-608

aliases:
- /en/research-data/user-story-608

dfg_areas:
- 102 History
- 106 Social and Cultural Anthropology, Non-European Cultures, Jewish Studies and Religious Studies

text_plus_domains:
- Comprehensive

authors:
- Kevin Wunsch (Technische Universität Darmstadt)

---




#### Motivation 


Research data currently has the character of a catchphrase in historical studies. When planning various projects, I had to realize that no project is eligible for funding if a certain amount of attention is not paid to the research data and its preparation. At the same time, information services and existing independent infrastructures are not widely used and almost unknown outside of expert circles. **F****AIR****data** can also be mentioned here as a keyword – projects must commit themselves to a **FAIR data policy**in order to make use of various funding opportunities. At various institutes there are various projects to make research data usable. Often, however, these are still in their infancy. How do I handle my research data? This question is not always in the foreground at the beginning of various projects, although it is of great importance. In the project “European Religious Peace Digital” this is to be different.  

#### Objectives 


I hope Text+ contributes to the adoption of critical infrastructure developments. Digital projects from historical sciences have similar requirements to those from related humanities, such as philosophy, art history, and modern philologies, but also more specific requirements. The requirements are therefore similar in ERPD (European Religious Peace Digital): the research data, i.e. the result of the research, must follow the so-called FAIR principles. The data must therefore be **(1) findable (2) accessible (3) interoperable and (4) reusable**. **Findable** is generally understood to mean the sustainable hosting of the data on central servers, which can be accessed via persistent identifiers, independent of any changes in the prerequisites**. Accessible**, i.e. available means research data is easily findable (via PIDs) but also has minimal access restrictions (ideally open access). **Interoperability** is given if open standards, such as those of the TEI, are used to structure and record the data. The fourth point, **reusability,** is a combination of the previous considerations. In order to make research data reusable, it must be both findable and accessible. However, interoperability must also be guaranteed. The implementation of the **FAIR****-principles** is to be steered centrally by Text+, or more precisely: the creation of infrastructures that promote the implementation of these principles and guarantee them without additional expense. If Text+ took over this task, on the one hand it would help the project members who can deal with project content without having to worry about a research data management plan, on the other hand a central infrastructure would also benefit researchers outside the project. Finally, research data would be collected at a central (virtual) location. The possibility to search the research data, at least one metadata search is desirable and would further maximize the benefit for the historical sciences, after all, the components of the project are of relevance. The Augsburg Religious Peace Treaty seems to be well researched and already edited, while a first edition of other texts is to be prepared, which will bring the various texts into the focus of scholarly study. 

#### Solutions 


Text+ should take into account the previous remarks and assist the project in developing the research data management plan, which will be implemented in cooperation with Text+ in the infrastructure created at ULB Darmstadt. The documents are transcribed according to TEIv5 and are of different provenance. They will be collected and made available at a central location. These data are to be collected and transcribed using an editing framework, namely WDB+, in accordance with the developed standards. Metadata are to be labelled according to Dublin Core, for example. In summary, Text+ should promote the creation of standards, support the design of infrastructure and finally, create awareness for research data management. 

#### Challenges 


Besides all the help Text+ can hardly influence or improve the structures in some areas. Especially with existing frameworks for the implementation of specific tasks, such as digital editions, Text+ can only exert limited influence. Only by promoting certain technologies can Text+ influence these frameworks. In this case, Text+ would fulfil the role of a supporter, but would intervene to a large extent in individual research. Whether WDB+, TextGrid or any other framework is used in a project should not be in the hands of uninvolved parties. Some projects can manage without a framework – this is especially the case for individual projects, such as edition projects that are implemented over the course of a doctorate. Text+ may therefore provide crucial information on basic technologies and standards or assist in setting up the necessary infrastructure, but the project staff can and should be given a free hand when implementing the software.  

