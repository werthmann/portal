---
title: "Old High German Dictionary: Supplements"

type: user-story

slug: user-story-517

aliases:
- /en/research-data/user-story-517

dfg_areas:
- 104 Sprachwissenschaften

text_plus_domains:
- Lexikalische Ressourcen

authors:
- Uwe Kretschmer
- Brigitte Bulitta (Sächsische Akademie der Wissenschaften zu Leipzig)

---




#### Motivation


The “Althochdeutsches Wörterbuch” (AWB) is a ten-volume dictionary project that attempts to make the earliest known German words from all types of texts accessible. Since 2017 the AWB is partly available online and is constantly being expanded. The online version is (except for a few letter corrections) identical to the printed version and can be quoted by the number of volumes and columns. All previous references of the AWB are interpreted and listed in full, while the contexts are cited according to their meaningfulness or difficulty in order to illustrate and verify the way a word is used.  

#### Objectives 


We would like to integrate new findings and corrigenda that have not yet been published or can only be viewed in the document archive into the online version as soon as possible. Their online availability would represent a considerable added value for dictionary users due to the desired completeness of the references. This would also ensure that they are in line with the current state of research, even with regard to classically published word lists. However, the dictionary’s citation capability must be maintained. 

#### Solutions 


Our story is a common problem of printed dictionaries, which make their way into a digital form without removing the direct reference to the printed work. 

In our opinion, recommendations for the organizational and technical implementation of such projects would be helpful. Besides the handling of versioning and storage of data, specifications (also regarding standards/interfaces) would be helpful for the aggregation of holdings from different sources. Possible problematic aspects of persistence also need to be considered. 

#### Review by Community


We are happy to use solutions proposed or worked out by Text+ for the problems described and look forward to an exchange of experiences. 

