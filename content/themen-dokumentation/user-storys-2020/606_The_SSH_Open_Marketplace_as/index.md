---
title: "The SSH Open Marketplace as discovery portal showcasing humanities resources and access point to the EOSC context"

type: user-story

slug: user-story-606

aliases:
- /en/research-data/user-story-606

dfg_areas:
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft
- 111 Sozialwissenschaften

text_plus_domains:
- Domänenübergreifend

authors:
- Stefan Buddenbohm (SUB Göttingen)

---




#### Motivation


Where can I find the suitable resources and workflows for my research question? Can I trust the information? Are there similar resources that may also be of interest for me?

Due to a wide range of research areas, methods and a heterogenity of research data in the humanities the researcher is confronted with a fragmented landscape of information bits spread over the internet. Platforms like CLARIAH-DE or DARIAH-EU are known to me and indeed very useful but cover only certain areas of the landscape I’m overviewing. I have to browse many sites always having to fear that I might overlook relevant sources, and after this, sort and evaluate all findings by myself, which is necessary on the one hand but bears the potential for the loss of valuable time if the information bits have to be collected from elsewhere. My search gets a bit more challenging when I take into account that it applies both to the German national perspective and the European perspective. The same may be true for the other national research communities, and with regard to the importance of language and cultural foundations for the humanities, it may be a real loss for me not to learn about foreign language resources.

*Other possible motivations:*
* *SSH Open Marketplace as place to expose humanities-related resources with German provenance, particularly offerings by Text+*
* *no need for Text+ to develop an own information hub to showcase CLARIN-D and DARIAH-DE resources*
* *SSH Open Marketplace as user-friendly and humanities-related access point to the European Open Science Cloud and its offerings, e.g. the EOSC Marketplace but also other research projects and initiatives*

#### Objective


I’d like to have a discovery portal containing as many resources from the humanities realm as possible in a trustworthy, standardized and contextualized way.

This discovery portal should be made available by Text+, ideally in an integrated way, e.g. by allowing sorting functionalities considering Text+ specific aspects like the data domains or the German provenance of resources. It is not necessary that the portal itself is provided and maintained by Text+.

The content of the discovery portal may cover humanities-relevant resources from categories like services, tools, methods, standards or documentation. It may be structured along data domains like collections, editions, lexicographic resources or around methodological premises or any other category of interest in research (user-specific lists or even virtual collection registry-like). Research is developing fast, and the discovery portal has to keep pace, indicating clearly the date and version history of individual entries. It may also include community aspects, be it some kind of interaction between users or the user-generated enrichment of information as this contributes to reputation and trust.

*Additional comment:*
* *the institutional structure or affiliation is possibly not of major interest at all for the researchers but he or she will appreciate trust-adding aspects, e.g. a recommendation of the SSH Open Marketplace by Text+*

#### Solution


The SSH Open Marketplace may be the suitable discovery portal for me when I search for trustworthy, actual and comprehensive information. The SSH Open Marketplace is part of the European Open Science Cloud (EOSC) and not Text+ but through CLARIN-D and DARIAH-DE closely related to each other.

As a German digital humanist I find the SSH Open Marketplace showcasing the (partly known) portfolios of CLARIN, DARIAH and other humanities-relevant collections in one place and find them embedded in a wide array of other sources. The service not only presents the resources in a comprehensive and up-to-date way, but also tries to contextualise them, e.g. by links, relations, comments or recommendations. This is important for me as it contributes to the level of trust and information granularity. For questions or even support, information on provenance is useful, ideally referring directly to the developers or providers.

Apart from this, the embedment of the SSH Open Marketplace in EOSC increases the level of trust and transdisciplinarity. It is not only well-connected within the DARIAH community but also beyond to the social sciences and with EOSC even in the broader context of European research, e.g. through the EOSC Marketplace.

*Additional comment:*
* *particularly the EOSC relation is of interest to the researcher as he or she has an access to the European Open Science Cloud through Text+*
* *Text+ as research infrastructure may use the marketplace to expose its own offerings to community beyond the German-speaking ones*

#### Challenges


The risks for the SSH Open Marketplace are:
* Will it be sustainable? This is important for me with look at the transparency and reproducability of my research.
* How deep or flexible is the integration of the marketplace within Text+? I really like the idea of synergies but see also the related risks.
* Will it gain the critical mass of content and community uptake? The service has to appear alive and busy to be attractive, so I will always check on my visits on aspects like „last update“ or „latest comments by the community“.

