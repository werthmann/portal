---
title: "fallada-archiv.de: Digital research archive on the author Hans Fallada"

type: user-story

slug: user-story-335

aliases:
- /en/research-data/user-story-335

dfg_areas:
- 105 Literaturwissenschaft

text_plus_domains:
- Collections

authors:
- Stefan Scherer
- Hannes Gürgen, Kristina Kapitel (Institut für Germanistik, Karlsruhe Institute of Technology)

---




#### Motivation 


The basis of *fallada-archiv.de* is the comprehensive, autopsied bibliography which is a vital part of the *Hans-Fallada-Handbuch,*published in 2018. It includes an extensive list of all primary texts, edits, reviews and the current state of research on Fallada, supplemented by an annotation system with metadata on text versions, time of origin, place of storage, etc. As part of the autopsy, we have set up a corpus of digitized versions of the hard-to-access texts from and about Fallada: literary and journalistic articles in newspapers and magazines as well as reviews. 

#### Objectives 


We are looking for a way to convert the Fallada bibliography into a digital version that also functions as a data repository granting access to the original site of publication. We would like to further our corpus of digitized texts and, if necessary, to optimize them qualitatively. Our objectives are to achieve a consolidation of the scattered sites and archives as well as to ascertain the long-term archiving of the texts and finally to make them available for the research community. Furthermore, our aim is to attend the accessible texts with a forum that enables and fosters follow-up research in literary and cultural studies, for example research on journals, reception, edition and text linguistics, as well as in related disciplines such as book and media studies, history and sociology. 

#### Solution 


We are looking for advice and support, especially regarding the technical implementation. Ideally, we would like to have a digital ‘publication instrument’ that includes both an appropriate database system and user access and guarantees quality standards in accordance with the DFG’s practical rules for digitization.  

For the presentation of the texts/digitized material we would need a tool similar to the DFG Viewer. Optionally, tools for the creation of full texts and their presentation would also be desirable. The architecture of the website should also allow the presentation of philological metadata (annotation apparatus). 

It would be helpful for us to have a concrete handbook (help tool/assistant) for non-professionals, indicating how and in what form the data should be published. It would be desirable to have a kind of ‘modular system’, as known from commercial service providers, with the help of which a repository can be built up that guarantees the quality standards (comparable to the DFG’s practice rules for digitization). 

#### Challenges 


Provision of advice on relevant issues relating to installation, usability and rights clearance (copyright, owning archives, etc.), possibly in the form of FAQs and/or a hotline. 

#### Review by Community 


We are happy to examine the services offered by Text+ during the offered funding period. 

