---
title: Medizinische Gutachten des 17. und 18. Jahrhunderts

institutions: 
- Katholische Universität Eichstätt-Ingolstadt

languages:
- Deutsch mit lateinischen und griechischen Einsprengseln

modalities:
- geschrieben

text_plus_domains: 
- Collections

dfg_areas:
- 102 Geschichtswissenschaften
- 103 Kunst-, Musik-, Theater- und Medienwissenschaften

descriptive_areas: 
- Sprachwissenschaft (v.a. Sprachgeschichte, Textlinguistik, Fachsprachenforschung)
- Medizingeschichte
- Wissenschaftsgeschichte
- Rechtsgeschichte
- Kulturgeschichte

type: community-data

---

Es handelt sich um ein Textkorpus, das 150 transkribierte medizinische Gutachten des 17. und 18. Jahrhunderts aus gedruckten medizinischen Fallsammlungen enthält. Die Texte liegen als Plain-Text-Dateien vor, weisen rudimentäre Annotationen (Zeilenumbruch und Seitenumbruch) auf, aber nur knappe bibliographische Angaben (keinen TEI-konformen Header!).