---
title: Bibliothek für Bildungsgeschichtliche Forschung (BBF)

institutions: 
- DIPF | Leibniz-Institut für Bildungsforschung und Bildungsinformation

languages:
- Deutsch

modalities:
- geschrieben

text_plus_domains: 
- Editionen 

dfg_areas:
- 102 Geschichtswissenschaften
- 109 Erziehungswissenschaft und Bildungsforschung

descriptive_areas: 
- Geschichte
- Erziehungswissenschaften

type: community-data

---

Die Bibliothek für Bildungsgeschichtliche Forschung stellt die Briefe Friedrich Fröbels und den Briefwechsel zwischen Eduard Spranger und Käthe Hadlich als Online-Edition bereit. Enthalten sind 6.251 Dokumente aus den Jahren 1799–1852 (Fröbel-Edition) und 1903–1960 (Spranger-Hadlich). Über ein Personen- und Jahresregister kann auf die Briefe zugegriffen werden. Die Texte sind nach den Richtlinien der Text Encoding Initiative (TEI) ausgezeichnet. Derzeit wird die Migration der Edition in den TEI-Publisher vorbereitet.

[Briefedition Friedrich Fröbel](https://editionen.bbf.dipf.de/exist/apps/briefedition-friedrich-froebel/index.html)
[Edition des Briefwechsels zwischen Eduard Spranger und Käthe Hadlich](https://editionen.bbf.dipf.de/exist/apps/briefedition-spranger-hadlich/index.html)