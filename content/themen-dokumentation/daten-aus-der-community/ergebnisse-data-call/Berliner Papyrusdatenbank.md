---
title: Berliner Papyrusdatenbank

institutions: 
- Ägyptisches Museum und Papyrussammlung – Staatliche Museen zu Berlin

languages:
- Deutsch
- Altgriechisch
- Lateinisch

modalities:
- geschrieben

text_plus_domains: 
- Collections 

dfg_areas:
- 101 Alte Kulturen
- 104 Sprachwissenschaften
- 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft
- 113 Rechtswissenschaften

descriptive_areas: 
- Alte Geschichte
- Klassische Philologie
- Ägyptologie
- vergleichende Sprachwissenschaften
- Religionswissenschaften
- Rechtswissenschaften

type: community-data

---

Als Teil des weltweiten papyrologischen Datenbanknetzwerks ist die Berliner Papyrusdatenbank von zentraler Bedeutung für alle Fächer des Bereichs 101 „Alte Kulturen“ der DFG-Fächersystematik (insb. Alte Geschichte, Klassische Philologie, Ägyptologie) und weit darüber hinaus (z.B. vergleichende Sprachwissenschaften, Religionswissenschaften, Rechtswissenschaften u.ä.).

Es handelt sich um eine ständig erweiterte und aktualisierte Datenbank der griechisch- und lateinischsprachigen Bestände der Berliner Papyrussammlung, die die größte ihrer Art in Deutschland und zu den fünf größten weltweit zählt. Neben den Metadaten (z.B. Inhalt, Datierung, Herkunft, Publikationen und Erwerbungsgeschichte) und hochauflösenden Bildern werden Verlinkungen zu weiterführenden Informationen anderer Datenbanken und Projekte angeboten.

https://berlpap.smb.museum/