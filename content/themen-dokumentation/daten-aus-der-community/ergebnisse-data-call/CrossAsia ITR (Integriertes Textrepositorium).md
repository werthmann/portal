---
title: CrossAsia ITR (Integriertes Textrepositorium)

institutions: 
- CrossAsia und Fachinformationsdienst Asien, Staatsbibliothek zu Berlin – Preußischer Kulturbesitz

languages:
- Chinesisch
- Englisch
- Japanisch
- Deutsch
- Niederländisch
- Französisch
- Spanisch
- Koreanisch
- Thai
- Laotisch

modalities:
- geschrieben

text_plus_domains: 
- Collections 

dfg_areas:
- 103  Kunst-, Musik-, Theater- und Medienwissenschaften
- 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft

descriptive_areas: 
- alle Geistes- und Sozialwissenschaften mit Asienbezug, insb. Asienwissenschaften (Sinologie, Japanologie, Koreanistik, Südostasienwissenschaften, Zentralasienwissenschaften (Tibetologie, Mongolistik, Uyghur studies), Südasienwissenschaften, Indologie), bzw. Regionalstudien mit Asienbezug, Reliogionswissenschaften (Buddhologie), ostasiatische Kunstgeschichte, etc.

type: community-data

---

Im „Integrierten Text-Repositorium“ CrossAsia ITR werden Bild- und Textdaten, der für den FID Asien und CrossAsia lizenzierten Datenbanken, für die Hosting-, Indexierungs- und Textmining-Rechte vereinbart werden konnten, sowie gemeinfreie Texte und Bilddaten wie Fotographien zusammen mit ihren Erschließungsdaten sicher und nachhaltig archiviert, mit dem Ziel, diese gleichberechtigt nebeneinander gemäß der FAIR-Prinzipien anbieten zu können. Enthalten sind aus dem Textbereich mit aktuellem Stand (August 2020) Volltexte von ca. 335.000 Titeln mit 53 Mio. Seiten aus 26 verschiedenen überwiegend chinesisch- und englischsprachigen lizenzpflichtigen Datenbanken sowie gemeinfreie Texte der Asienkollektion der Digitalisierten Sammlungen der SBB-PK in westlichen sowie asiatischen Sprachen. Eine Ressourcenliste findet sich hier.

[Beschreibung](https://blog.crossasia.org/crossasia-itr-was-schon-ist-und-was-noch-kommt/)
[Volltextsuche](https://crossasia.org/service/crossasia-lab/crossasia-itr/)
[Explorer](https://crossasia.org/de/service/crossasia-lab/crossasia-itr/itr-explorer/#/)