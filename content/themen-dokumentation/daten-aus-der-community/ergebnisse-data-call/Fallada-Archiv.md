---
title: Fallada-Archiv

institutions: 
- Karlsruher Institut für Technologie, Institut für Germanistik

languages:
- Deutsch
- teilweise Englisch

modalities:
- geschrieben

text_plus_domains: 
- Collections 
- Editionen

dfg_areas:
- 101-02 Klassische Philologie
- 102 Geschichtswissenschaften
- 103 Kunst-, Musik-, Theater- und Medienwissenschaften
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft
- 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft
- 108 Philosophie
- 111 Sozialwissenschaften

descriptive_areas: 
- Literaturwissenschaft
- Editionsphilologie
- Rezeptionsforschung
- Zeitschriftenforschung
- Kulturwissenschaft
- Textlinguistik
- Geschichtswissenschaft
- Soziologie

type: community-data

---

Das Korpus besteht aus:

- Bibliographie zum Autor Hans Fallada (1893–1947), die alle Primärtexte, Bearbeitungen, Rezensionen und den aktuellen Stand der Forschung listet.
- Digitalisate schwer zugänglicher journalistischer und literarischer Beiträge Falladas sowie der Erstdrucke seiner Romane, die in verschiedenen Zeitungen und Zeitschriften in Fortsetzungen erschienen sind.
- Digitalisate zeitgenössischer Rezensionen zu Fallada.

Das Korpus dieser Texte ist derzeit auf viele verschiedene Archive und Bibliotheken verteilt und zum Großteil nicht digital zugänglich.