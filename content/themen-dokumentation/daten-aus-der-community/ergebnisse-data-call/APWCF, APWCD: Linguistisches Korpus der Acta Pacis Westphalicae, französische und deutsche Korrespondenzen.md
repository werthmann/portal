---
title: "APWCF, APWCD: Linguistisches Korpus der Acta Pacis Westphalicae, französische und deutsche Korrespondenzen"

institutions: 
- Universität Potsdam, Lehrstuhl für romanische Sprachwissenschaft (Französisch und Italienisch)

languages:
- Deutsch
- Französisch
- Italienisch
- teils Latein
- zahlreiche anderssprachige Einschübe

modalities:
- geschrieben

text_plus_domains: 
- Collections 

dfg_areas:
- 102 Geschichtswissenschaften
- 103 Kunst-, Musik-, Theater- und Medienwissenschaften
- 104 Sprachwissenschaften

descriptive_areas: 
- Linguistik
- Geschichte
- Rechtsgeschichte
- Geschichte internationaler Beziehungen
- Kulturgeschichte

type: community-data

---

Es handelt sich um ein linguistisches Korpus auf Basis der digitalen Edition der Acta Pacis Westphalicae (APW). Für diese Nachnutzung und die nicht-kommerzielle Datenpublikation liegen schriftliche Genehmigungen der rechteinhabenden Institutionen vor. Aus linguistischer Sicht ist diese ursprünglich für die Historiographie aufbereitete Ressource äußerst wertvoll: die deutschen, französischen, oft mehrsprachigen fachsprachlichen oder informellen Textsorten repräsentieren unterschiedliche Register einer wichtigen Sprachwandelphase. Die Editionskriterien verzeichnen minimale Eingriffe in den Originaltext. Als Korpus (Auszeichnung von Metadaten, Trennung von Textdaten) bisher verfügbar: Französische Korrespondenzen der Acta Pacta Westphalicae (1644–1647), annotiert mit TreeTagger Parameter für das klassische Französisch des Projekts PRESTO (DFG/ANR), 2.640.000 Tokens Deutsche Korrespondenzen der Acta Pacta Westphalicae (1643–1648), ca. 835.000 Tokens, noch nicht annotiert.