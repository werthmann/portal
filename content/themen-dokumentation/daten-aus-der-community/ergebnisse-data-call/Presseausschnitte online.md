---
title: Presseausschnitte online

institutions: 
- Herder-Institut für historische Ostmitteleuropaforschung – Institut der Leibniz-Gemeinschaft

languages:
- hauptsächlich Deutsch

modalities:
- geschrieben

text_plus_domains: 
- Collections 
- Software Services

dfg_areas:
- 102 Geschichtswissenschaften
- 103 Kunst-, Musik-, Theater- und Medienwissenschaften
- 111 Sozialwissenschaften

descriptive_areas: 
- Geschichte
- Zeitgeschichte
- Politik
- Medienwissenschaft

type: community-data

---

Über 5 Millionen Ausschnitte dokumentieren die Geschichte, Politik, Kultur und Wirtschaft Ostmitteleuropas von 1916 bis heute. Wir haben uns insbesondere auf eine systematische Analyse regionaler und nationaler Tages- und Wochenzeitungen aus Ostmitteleuropa und dem deutschsprachigen Raum für den Zeitraum von 1952 bis März 1999 konzentriert. Wir bieten umfassende Archive zu Personen, Orten und Themen. Sie können als einzigartige Dokumentation des sozialistischen Experiments in Osteuropa verwendet werden. Rund 10.000 Ausschnitte über Personen sind bereits digitalisiert und mit Metadaten kombiniert, 6.500 davon zusätzlich mit Schrifterkennung behandelt.

https://www.herder-institut.de/pressesammlung/
[Übersicht](https://www.herder-institut.de/holdings/?hol=HI000025)