---
title: Niklas-Luhmann-Archiv

institutions: 
- Universität Bielefeld, Fakultät für Soziologie

languages:
- Deutsch
- Englisch
- Italienisch
- Spanisch

modalities:
- geschrieben
- gesprochen

text_plus_domains: 
- Collections 
- Editionen

dfg_areas:
- 102 Geschichtswissenschaften
- 105 Literaturwissenschaft
- 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft
- 108 Philosophie
- 109 Erziehungswissenschaft und Bildungsforschung
- 111 Sozialwissenschaften
- 113 Rechtswissenschaften

descriptive_areas: 
- Soziologie
- Philosophie
- Rechtswissenschaft
- Erziehungswissenschaft
- Literaturwissenschaft
- Religionswissenschaft
- Politikwissenschaft
- Organisationswissenschaft
- Wissenschaftsgeschichte

type: community-data

---

Wissenschaftlicher Nachlass des Soziologen Niklas Luhmann (1927–1998), einem der bedeutendsten Soziologen des 20. Jahrhunderts. Erschließung, Transkription, Edition und Digitalisierung des Zettelkastens mit ca. 90.000 Notizen, der nachgelassenen Manuskripte und anderer Materialien (u. a. Audio- und Videoaufnahmen von Vorträgen und Interviews).

https://niklas-luhmann-archiv.de/