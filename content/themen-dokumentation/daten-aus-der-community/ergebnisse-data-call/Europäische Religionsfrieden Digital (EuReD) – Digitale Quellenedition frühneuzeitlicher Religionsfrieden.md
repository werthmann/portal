---
title: Europäische Religionsfrieden Digital (EuReD) – Digitale Quellenedition frühneuzeitlicher Religionsfrieden

institutions: 
- Leibniz-Institut für Europäische Geschichte (IEG)
- Universitäts- und Landesbibliothek Darmstadt

languages:
- Deutsch
- Latein
- Französisch
- Englisch
- Tschechisch
- Ungarisch
-  Polnisch
- Italienisch
- Niederländisch
- Dänisch
- Schwedisch
- evtl. Russisch

modalities:
- geschrieben

text_plus_domains: 
- Editionen

dfg_areas:
- 102 Geschichstwissenschaftem
- 103 Kunst-, Musik-, Theater- und Medienwissenschaften
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft
- 107 Theologie 

descriptive_areas: 
- Historische Friedensforschung
- Kulturgeschichte
- Rechtsgeschichte
- Kirchen- und Theologiegeschichte
- Editionswissenschaft

type: community-data

---

Mit der Quellenedition wird erstmals eine Textbasis für die vergleichende Erforschung vormoderner, religionsbezogener Friedensstiftung in Europa bereitgestellt. Die mit ausführlichen Einleitungen und Kommentaren versehene Edition umfasst die Zeitspanne von 1485 (Kuttenberger Frieden) bis 1788 (Woellnersches Religionsedikt). Grundlage der Ausgabe sind die Texte in ihrer zuerst veröffentlichten und rezipierten Form (editio princeps). Die Edition ist born-digital und verwendet die XML/TEI-p5-Standards.

http://www.religionsfrieden.de/
[Webseite der Edition](https://tueditions.ulb.tu-darmstadt.de/v/pa000001?)