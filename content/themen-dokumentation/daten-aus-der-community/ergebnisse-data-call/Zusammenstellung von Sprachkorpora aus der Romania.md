---
title: Zusammenstellung von Sprachkorpora aus der Romania

institutions: 
- Verschiedene Anbieter, zusammengestellt vom Fachinformationsdienst Romanistik

languages:
- Romanische Sprachen (v.a. Französisch, Italienisch, Portugiesisch, Rumänisch, Spanisch)
- weitere Sprachen, bspw. Übersetzungen, können enthalten sein, z. B. Englisch, Gebärdensprachen

modalities:
- geschrieben
- teilweise gesprochen als Audio- bzw. gebärdet als Video-Korpora mit oder ohne Transkription

text_plus_domains: 
- Collections 
- Lexikalische Ressourcen

dfg_areas:
- 101-02 Klassische Philologie
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft
- 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft
- 108 Philosophie

descriptive_areas: 
- Romanistik
- Sprachwissenschaft
- weitere mit Texten arbeitende Disziplinen

type: community-data

---

Die Beschreibung der einzelnen Datensätze ist dem jeweiligen Katalogisat zu entnehmen, das neben einer formalen Titelaufnahme (Dublin Core) in der Regel eine umfassende sachliche Erschließung mit GND-Schlagwörtern, DDC-Hauptklassen und Abstracts enthält. Erfasst wird auch die betroffene Sprache, was eine Filterung nach Einzelsprachen erlaubt.

[Suche](https://www.fid-romanistik.de/forschungsdaten/suche-nach-forschungsdaten/fid-internetressourcen/linguistische-korpora-und-datenbanken/)
[Übersicht](https://fid-romanistik.de//forschungsdaten/suche-nach-forschungsdaten/fid-internetressourcen/#c2312)