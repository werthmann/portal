---
title: Zusammenstellung von Volltextsammlungen aus der Romania (Sammlungen und Editionen)

institutions: 
- Verschiedene Anbieter, zusammengestellt vom Fachinformationsdienst Romanistik

languages:
- Romanische Sprachen (v.a. Französisch, Italienisch, Portugiesisch, Spanisch)
- vereinzelt können auch weitere Sprachen, bspw. Übersetzungen, enthalten sein

modalities:
- vornehmlich geschrieben

text_plus_domains: 
- Editionen

dfg_areas:
- 101-02 Klassische Philologie
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft
- 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft
- 108 Philosophie
- 111 Sozialwissenschaften

descriptive_areas: 
- Romanistik (Literaturwissenschaft, Sprachwissenschaft, Kultur- und Medienwissenschaft, Fachdidaktik)
- interdisziplinär arbeitende Philologien
- Kultur- und Medienwissenschaften
- Sozialwissenschaften
- Digital Humanities

type: community-data

---

Die Beschreibung der einzelnen Datensätze ist dem jeweiligen Katalogisat zu entnehmen, das neben einer formalen Titelaufnahme (Dublin Core) i.d.R. eine umfassende sachliche Erschließung mit GND-Schlagwörtern, DDC-Hauptklassen und Abstracts enthält. Erfasst wird auch die betroffene Sprache, was eine Filterung nach Einzelsprachen erlaubt.

[Suche](https://www.fid-romanistik.de/forschungsdaten/suche-nach-forschungsdaten/fid-internetressourcen/volltexte/)
[Übersicht](https://www.fid-romanistik.de/forschungsdaten/suche-nach-forschungsdaten/fid-internetressourcen/#c2312)