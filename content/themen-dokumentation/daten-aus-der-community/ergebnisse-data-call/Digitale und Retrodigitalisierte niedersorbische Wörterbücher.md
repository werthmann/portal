---
title: Digitale und retrodigitalisierte niedersorbische Wörterbücher

institutions: 
- Sorbisches Institut Bautzen

languages:
- Niedersorbisch
- Deutsch

modalities:
- geschrieben

text_plus_domains: 
- Lexikalische Ressourcen

dfg_areas:
- 101-02 Klassische Philologie
- 104 Sprachwissenschaften
- 105 Literaturwissenschaft
- 106 Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft
- 108 Philosophie

descriptive_areas: 
- Sorabistik
- Slawistik
- Lexikografie
- Sprachwissenschaft
- Kulturwissenschaft

type: community-data

---

Einheitliche Internetversion vierer retrodigitalisierter niedersorbisch-deutscher Wörterbücher auf Basis fein-granular semantisch-strukturell modellierter XML-Dateien. Außerdem: Digitales aktives Deutsch-niedersorbisches Wörterbuch. Diese lexikalischen Ressourcen werden zunehmend über gemeinsame Suchschnittstellen miteinander verbunden.

https://www.niedersorbisch.de/