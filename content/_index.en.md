---
title: "Home"
type: landing
---

{{<landing/call-to-action title="Utilize and Preserve Text- and Language-based Research Data">}}
{{<lead-text>}}
The Text+ consortium will preserve text- and language-based research data for the long term and enable their broad usage in the scientific community.
{{</lead-text>}}

    {{<button-row align="end">}}
        {{<button url="/en/ueber-uns/struktur-und-governance/">}}About Text+{{</button>}}
        <!-- tab: 'metadata' -->
        {{<search-button is_data_search="true">}} Search Data {{</search-button>}}
    {{</button-row>}}

{{</landing/call-to-action>}}

## Vision and Mission

Vision:
The text- and language-oriented humanities and social sciences extensively leverage the possibilities of digitization in their research, teaching, and transfer, establishing a common data culture.

Mission:
We empower text and language data and access to such data. Access to digital sources should become a standard. We enhance the digital literacy of researchers. We encompass the diversity of the research community and build on their participation. We promote inter- and transdisciplinarity as well as innovation – for instance in the field of artificial intelligence – through the integration of infrastructure and research.

## Areas of Work in Text+

The Text+ infrastructure focuses on language and text data, initially concentrating on digital collections, lexical resources, and editions. These are highly relevant for all language- and text-based disciplines, especially linguistics, literary studies, philosophy, classical philology, anthropology, non-European cultures and languages, as well as language- and text-based research in the social, economic, political, and historical sciences. Infrastructure/Operation aims for an integrated and coordinated Text+ service portfolio consisting of interoperable offerings and services.

{{<landing/data-domain-links>}}


## User Stories 2020

To integrate user needs, we invited researchers to contribute user stories from their research environments before the start of funding. The goal was to capture a wide range of disciplines, data domains, and research questions.

{{<button url="/themen-dokumentation/user-storys-2020/">}}
Explore the User Stories 2020
{{</button>}}
