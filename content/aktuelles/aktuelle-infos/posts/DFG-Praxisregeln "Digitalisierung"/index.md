---

title: DFG-Praxisregeln „Digitalisierung“ – aktualisierte Fassung 2022 auf Zenodo publiziert
featured_image: Screenshot-2023-03-13-at-12.19.08.png
type: news
date: 2023-03-13 11:13:00

---
# DFG-Praxisregeln „Digitalisierung“ – aktualisierte Fassung 2022 auf Zenodo publiziert

Seit Mitte Februar ist die auf den neuesten Stand gebrachte Fassung der DFG-Praxisregeln „Digitalisierung“ online:

https://doi.org/10.5281/zenodo.7435724

Zusammen mit NFDI4Culture war Text+ maßgeblich an der Koordinierung dieser Aktualisierung beteiligt.

Die DFG-Praxisregeln „Digitalisierung“ stellen eine zentrale Grundlage für DFG-geförderte Projekte im [Programm „Digitalisierung und Erschließung“](https://www.dfg.de/foerderung/programme/infrastruktur/lis/lis_foerderangebote/digitalisierung_erschliessung/index.html) dar: Sie formulieren Standards und enthalten Informationen zu allen organisatorischen, methodischen und technischen Fragen im Kontext der Digitalisierung und Erschließung von Objekten des materiellen Kulturerbes. Damit leisten sie einen wichtigen Beitrag zur Nachhaltigkeit, Zugänglichkeit und Anschlussfähigkeit einer Forschungsdateninfrastruktur.

Das veröffentlichte Dokument dient als Ausgangspunkt für die bevorstehende materialbezogene Ausdifferenzierung der Praxisregeln durch die Communitys. NFDI4Culture, NFDI4Memory, NFDI4Objects und Text+ stehen gemeinsam als Kontaktpunkte und Plattform für die nächsten Schritte zur Verfügung, sind die Praxisregeln „Digitalisierung“ doch ein zentraler Baustein für die Standardbildung in ihren Forschungscommunitys. Alle mit der Digitalisierung und Erschließung forschungsrelevanter Objekte befassten Communitys und Einrichtungen sind dazu aufgerufen, mit ihrer Expertise an diesem Prozess der Neugestaltung mitzuwirken.