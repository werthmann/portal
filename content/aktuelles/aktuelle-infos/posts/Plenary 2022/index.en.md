---
title: Text+ Plenary 2022
featured_image: Banner-Website-Plenary-2022.jpg
type: news
date: 2022-05-17
---

# Text+ Plenary 2022

On September 12 and 13, 2022, the Text+ Plenary took place in Mannheim. Registration is closed. For more information, visit [http://events.gwdg.de/event/269](http://events.gwdg.de/event/269).
