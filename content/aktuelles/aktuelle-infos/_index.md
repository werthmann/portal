---
title: Aktuelle Infos

aliases:
- /aktuelles-veranstaltungen/aktuelles

menu:
  main:
    weight: 10
    parent: aktuelles
type: news

---
# Aktuelle Infos

{{<post-teaser title="DFG-Praxisregeln „Digitalisierung“ – aktualisierte Fassung 2022 auf Zenodo publiziert">}}
{{<post-teaser title="Text+ bloggt!" >}}
{{<post-teaser title="Text+ Plenary 2022" >}}
{{<post-teaser title="Solidaritätserklärung" >}}
{{<post-teaser title="Text+ Einladung zum öffentlichen Tag der Frühjahrstagung am 2. März 2022" >}}