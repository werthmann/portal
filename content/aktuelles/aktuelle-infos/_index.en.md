---
title: News

aliases:
- /aktuelles-veranstaltungen/aktuelles

menu:
  main:
    weight: 10
    parent: aktuelles
type: news

---
# News

{{<post-teaser title="DFG Guidelines on Digitisation - Updated Version 2022 Published on Zenodo" >}}
{{<post-teaser title="Text+ Blogs!" >}}
{{<post-teaser title="Text+ Plenary 2022" >}}
{{<post-teaser title="Solidarity Statement" >}}
{{<post-teaser title="Text+ Plenary 2022" >}}