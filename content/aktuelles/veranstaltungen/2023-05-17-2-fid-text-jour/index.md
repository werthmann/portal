---
type: event
title: "2. FID / Text+ Jour Fixe"
start_date: 2023-05-17 09:00:00
end_date: 2023-05-17 12:30:00
all_day: false
location: online
aliases:
- /events/2-fid-text-jour-fixe
---

Text+ ist ein Konsortium der Nationalen Forschungsdateninfrastruktur ([NFDI](https://www.nfdi.de/)). Ziel des Konsortiums ist es, sprach- und textbasierte Forschungsdaten langfristig zu erhalten und ihre Nutzung in der Wissenschaft zu ermöglichen. Fachinformationsdienste (FIDe) spielen dabei als Mittler zwischen Infrastruktur und Fachcommunities eine wichtige Rolle. Innerhalb des Konsortiums Text+ hat sich daher die AG FID Koop gegründet, um die Zusammenarbeit mit FIDen zu initialisieren und zu begleiten sowie gemeinsam Kooperationsmöglichkeiten auszuloten.

Nach der [Auftaktveranstaltung](https://events.gwdg.de/event/345/), die am 18.10.2022 stattfand und sich dem Schwerpunkt "Erwartungsmanagement" befasste, wird beim 2. Termin der Reihe "FID/Text+ Jour Fixe" das Thema "Consulting" im Fokus stehen.

Die Veranstaltung richtet sich an Kolleginnen und Kollegen aus den Fachinformationsdiensten und Text+.

Anmelde- und Programmseite: https://events.gwdg.de/event/378/