---
type: event
title: "Basics-Workshops mit de.hypotheses"
start_date: 2024-03-14 10:00:00
end_date: 2024-03-14 12:30:00
all_day: false
location: online
---
Neues Jahr, neues Wissenschaftsblog? Zum Anfang des Jahres bieten wir für alle, die im vergangenen Jahr ein Blog bei de.hypotheses eröffnet haben oder die aktuell in den Startlöchern stehen, eine zweiteilige Einsteigerschulung an. Die Schulung wird in zwei Teile aufgeteilt sein, die auch unabhängig voneinander besucht werden können.

Der praxisorientierte Workshop richtet sich an alle, die bislang keine oder nur begrenzt Erfahrung mit WordPress gesammelt haben. Natürlich sind auch alle willkommen, die schon länger dabei sind und einen kleinen Auffrischer wünschen. Inhalte werden sein:
* Wie kann ich meinen Blogbeitrag visuell ansprechend gestalten und richtig strukturieren?
* Hier geht es um die Arbeit mit Blockvorlagen, das Einbinden von Bildern, die Formatierung von Fußnoten und Zitatvorschlägen in de.hypotheses sowie die Verwendung von Kategorien und Schlagwörtern.
* Wie und wo antworte ich auf Kommentare?
* Wie entscheide ich mich für ein WordPress-Theme? Worin unterscheiden sich die verschiedenen Themes, die bei de.hypotheses verfügbar sind?

Außerdem wird es in beiden Workshops ausreichend Zeit für individuelle Fragen (gerne bei der Anmeldung mit angeben!) und zum praktischen Üben geben.

Die Teilnehmenden arbeiten je nach Wunsch entweder im eigenen oder in einem eigens eingerichteten Schulungsblog, das anschließend auch zum Üben genutzt werden kann.

Wir bitten um Anmeldung bis zum 17.01.2024 (Teil 1) bzw. bis zum 6. März 2024 (Teil 2) über blogs at maxweberstiftung.de. Bitte bei der Anmeldung das de.hypotheses-Blog angeben. Wer während der Schulung nicht im eigenen Blog arbeiten möchte (oder noch keins hat), sollte der Anmeldung den folgenden Einzeiler hinzufügen: “Ich erkläre mich damit einverstanden, dass mit meiner Mailadresse ein WordPress-Schulungsblog angelegt wird, das nach spätestens 12 Monaten gelöscht wird.”

Der Workshop wird geleitet von Dr. Ulrike Stockhausen (Max Weber Stiftung) und Leonard Dorn (Deutsches Historisches Institut Paris).

Wir werden die Schulung über Zoom abhalten. Teilnehmende können sich über den Einladungslink einwählen, es ist kein eigenes Zoom-Konto notwendig.