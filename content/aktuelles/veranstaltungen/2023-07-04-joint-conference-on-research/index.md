---
type: event
title: "Joint Conference on Research on Text Analytics"
start_date: 2023-07-04
all_day: true
location: Aula, Universität Mannheim, Schloss, 68161 Mannheim
featured_image: images/JCoRoTA_savethedate.png
aliases:
- /events/joint-conference-on-research-on-text-analytics
---

Within the NFDI, text data is an important source for analysis, for example in the consortia BERD@NFDI and Text+, which deal with humanities research (Text+) and Business, Economic and Related Data (BERD@NFDI). Although both consortia address different communities and research questions, there is overlap in methods, e.g. machine learning, aspects of language models and other natural language processing techniques. The Joint Conference on Research on Text Analytics seeks to present the different application areas and identify possible synergies when looking at similar methods.

Further information and registration: https://events.gwdg.de/event/435