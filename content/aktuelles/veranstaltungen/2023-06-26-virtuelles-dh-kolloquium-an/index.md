---
type: event
title: "Virtuelles DH-Kolloquium an der BBAW"
start_date: 2023-06-26 16:15:00
end_date: 2023-06-26 18:00:00
all_day: false
location: online
featured_image: 
aliases:
- /events/virtuelles-dh-kolloquium-an-der-bbaw-7
---

Im Rahmen des DH-Kolloquiums an der BBAW laden wir Sie herzlich zum nächsten Termin am Montag, den 22. Mai 2023, 16 Uhr c.t., ein (virtueller Raum: [https://meet.gwdg.de/b/lou-eyn-nm6-t6b](https://meet.gwdg.de/b/lou-eyn-nm6-t6b)). Holger Helbig, Ulrike Henny-Krahmer, Fabian Kaßner (alle Universität Rostock) Das digitale Buch. Perspektiven auf Uwe Johnsons Jahrestage in der digitalen Werkausgabe.

Veranstaltungsraum: https://meet.gwdg.de/b/lou-eyn-nm6-t6b