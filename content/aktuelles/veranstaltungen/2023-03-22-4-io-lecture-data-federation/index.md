---
type: event
title: "4. IO-Lecture: Data Federation Architecture (DFA) &amp; Data Modeling Environment (DME)"
start_date: 2023-03-22 10:00:00
end_date: 2023-03-22 12:00:00
all_day: false
location: online
aliases:
- /events/4-io-lecture-data-federation-architecture-dfa-data-modeling-environment-dme
---

In der 4. IO-Lecture wird Tobias Gradl (Lehrstuhl für Medieninformatik an der Universität Bamberg) über die Data Federation Architecture (DFA) und Data Modeling Environment (DME) sprechen. Bei beiden Infrastrukturkomponenten handelt es sich um mögliche Bausteine der zukünftigen Text+ Dateninfrastruktur, die Bestände aus verschiedenen Kontexten (Text+ Datenzentren sowie Kooperationsprojekte) find- und durchsuchbar machen wird.

**Über die DFA**

Konzeptionell ist die DFA ist in Schichten organisiert. Sie ordnet ihre Komponenten der Datenschicht, Föderationsschicht und Diensteschicht zu und kann in jeder dieser Schichten jederzeit erweitert werden.

- Auf Ebene der _Datenschicht_ werden Forschungsdaten gespeichert und verwaltet

- Die Komponenten der _Föderationsschicht_ haben eine nachweisende und deskriptive Funktion und bieten insbesondere Schnittstellen für die integrative Betrachtung von Forschungsdaten

- Die _Diensteschicht_ umfasst schließlich diejenigen Softwaresystem, die auf Basis untergeordneter Schichten einen weiterführenden Nutzen für ihre Anwenderinnen und Anwender generieren.

**Über die DME**

Anders als die weiteren Komponenten der [DFA](https://dfa.de.dariah.eu/doc/dme/glossary.html#term-dfa) ist das [DME](https://dfa.de.dariah.eu/doc/dme/glossary.html#term-dme) als Expertenwerkzeug konzipiert und unterstützt die tiefgreifende Beschreibung der Erstellungs- und Verwendungskontexte von Forschungsdaten. Basierend auf dem Konzept domänenspezifischer Sprachen wurden neuartige Methoden zur Spezifikation von Daten und deren Transformation entwickelt.

**Weiterführende Informationen**

[Dokumentationsseiten](https://dfa.de.dariah.eu/doc/dfa/)

**Zielgruppe der IO-Lecture**

Die Veranstaltung richtet sich in erster Linie an Mitarbeitende der drei Text+ Datendomänen. Darüber hinaus freuen wir uns aber auch über Teilnehmende aus anderen NFDI-Konsortien. Im Zweifelsfalle kontaktieren Sie gerne das Text+ Operations Office.

**Hinweise**

Die IO-Lecture wir aufgezeichnet. Wenn Sie nicht wünschen, dass Ihre persönlichen Informationen geteilt werden, schalten Sie bitte Ihre Webcam aus. Benutzernamen und Chatnachrichten werden **nicht** aufgezeichnet.

Diese Veranstaltung ist Teil der [IO-Lectures](https://events.gwdg.de/category/169/).

Info & Anmeldung: https://events.gwdg.de/event/440/