---
type: event
title: "Queens of Humanities"
start_date: 2023-03-03 10:30:00
end_date: 2023-03-03 11:30:00
all_day: false
location: online
aliases:
- /events/queens-of-humanities
---

DARIAH Friday Frontiers: Spring 2023 Series

DARIAH is pleased to announce the first session of our popular in-house webinar series Friday Frontiers: Spring 2023 Series. The Friday Frontiers webinars allow researchers, practitioners and stakeholders from across the broad DARIAH community, and now beyond, to learn about current research, best practice and social impact, and different tools and methods in digital humanities scholarly practice.

The webinar sessions are all free to attend, but registration is required.  Presentations are all recorded and published at a later date on [DARIAH-Campus](https://campus.dariah.eu/).  The full details of the first Spring session, along with information on how to register, can be found below:

Info & Registration: https://www.dariah.eu/2023/02/13/dariah-friday-frontiers-spring-2023-series/

#### Friday 3rd March, 10.30am GMT / 11.30am CET / 12.30pm EET

#### Queens of Humanities. Stories to attract and engage.

**Speakers**: Marta Swietlik (IBL-PAN, Poland), Magdalena Wnuk (IBL-PAN, Poland)

**Registration (required):** [https://dariah.zoom.us/meeting/register/tZEpf-GvqjktH90n4GN1R03HtYlKV23M5TFa](https://dariah.zoom.us/meeting/register/tZEpf-GvqjktH90n4GN1R03HtYlKV23M5TFa)

#### Abstract

How do we tell the story of humanities as the essence of understanding humankind in its all aspects and bring it back to the table as an equal partner of science? Seeking an answer to this question, we will present the scope and dissemination of the Queens of Humanities campaign run last year by OPERAS-PL. Its purpose was to promote innovative humanistic approaches and show their relevance in today’s world. 

“Born digital” monographs, data visualisations, web applications, podcasts or vlogs were presented as innovative outputs aimed at telling the story of humanities as an indispensable and ubiquitous means of expanding human knowledge about the world. The posts were also an occasion to bring up important problems of humanities research, such as the underestimation of innovative publications that do not fit with the strict academic criteria. 

The “Queens of Humanities” project is a simple, yet inspiring advocacy project which can be implemented among different audiences and through various social channels. It also reflects on Facebook, which, despite increasing competition in the area of social media, offers unsophisticated, yet sufficient solutions for storytelling targeted at specific local audiences.
