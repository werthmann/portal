---
type: event
title: "Virtuelles DH-Kolloquium an der BBAW"
start_date: 2023-05-22 16:15:00
end_date: 2023-05-22 18:15:00
all_day: false
location: online
aliases:
- /events/virtuelles-dh-kolloquium-an-der-bbaw-6
---

Im Rahmen des DH-Kolloquiums an der BBAW laden wir Sie herzlich zum nächsten Termin am Montag, den 22. Mai 2023, 16 Uhr c.t., ein (virtueller Raum: https://meet.gwdg.de/b/lou-eyn-nm6-t6b):

Ulf Hamster (Berlin-Brandenburgische Akademie der Wissenschaften)  
Auswahl von Belegen aus großen Textkorpora mit Hilfe von „Active Learning“

Anmeldung: https://meet.gwdg.de/b/lou-eyn-nm6-t6b