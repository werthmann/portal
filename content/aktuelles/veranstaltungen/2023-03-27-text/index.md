---
type: event
title: "Text+ Frühjahrstreffen"
start_date: 2023-03-27
end_date: 2023-03-28
all_day: true
location: Leipzig
featured_image: images/Text-Logo-ohne-Umschrift.png
aliases:
- /events/text-fruhjahrstreffen
---

Am 27. und 28. März 2023 trifft sich Text+ in Leipzig. Das NFDI-Konsortium kommt in Präsenz zusammen, um Arbeitsgruppentreffen und Task Area-Treffen durchzuführen sowie übergreifende Themen intern zu diskutieren.
