---
title: "Korpora in Text+: Kennenlernen und nachhaltig nutzen"
start_date: 2022-07-13
location: Deutsche Nationalbibliothek Frankfurt
type: event
all_day: true
aliases:
- /events/korpora-in-text-kennenlernen-und-nachhaltig-nutzen
---

Text+ ist Heimat zahlreicher textlicher Korpora. Einige von diesen sind bereits in weiten Wissenschaftskreisen bekannt, andere bedienen bislang nur ein Expertenpublikum. Aus diesem Grund veranstaltet die DNB am 13. Juli 2022 einen Präsenz-Workshop, um Korpora aus Text+ einer breiteren Wissenschaftsöffentlichkeit vor- und Möglichkeiten der Arbeit mit ihnen darzustellen.

In insgesamt vier 90-minütigen Workshopblöcken (Programm s.u.) zu linguistischen aufbereiteten und nicht aufbereiteten Korpora, historischen Korpora und computerphilologisch aufbereiteten Korpora werden diese vorgestellt, diskutiert, problematisiert und hands-on mit ihnen gearbeitet. Dabei wird immer ein Fokus auf die Aufbereitung sowie die unterschiedlichen Formate der Korpora gesetzt, im Mittelpunkt steht aber besonders auch die Frage, welche Forschungsfragen den Korpora zugrunde liegen. Alle Diskussionspunkte werden immer auch in gemeinsamer Perspektive in Text+ betrachtet. 

**Die Registrierung ist ab dem 24. Mai unter** [**https://events.gwdg.de/event/235/**](https://events.gwdg.de/event/235/) **möglich.** Dort finden Sie auch weitere Informationen und das genaue Programm.

Die Teilnahmegebühr für die Veranstaltung beträgt inklusive Verpflegung 35€ pro Person.

Für das NFDI-Konsortium Text+ veranstaltet die [DNB](https://www.dnb.de/DE/Home/home_node.html) in ihren Räumen (Vortragssaal, Adickesallee 1, 60322 Frankfurt am Main) diesen Präsenz-Workshop. Organisiert wird der Workshop von der AG Dissemination der Text+ Task Area Collections.
