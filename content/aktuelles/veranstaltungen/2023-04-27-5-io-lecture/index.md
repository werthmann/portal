---
type: event
title: "5. IO-Lecture: entityXML"
start_date: 2023-04-27 10:00:00
end_date: 2023-04-27 12:00:00
all_day: false
location: online
aliases:
- /events/5-io-lecture-entityxml
---

In der 5. IO-Lecture werden Uwe Sikora und Susanne Al-Eryani (beide SUB Göttingen, Gruppe Metadaten und Datenkonversion) entityXML vorstellen, einem Arbeitsergebnis aus dem GND-Task der Text+ Task Area Infrastructure/Operations.

**Programm**

1. Was ist entityXML?

2. Wie sieht entityXML aus?

3. Wie arbeite ich mit entityXML? (Hands-on)

**Was ist entityXML?**

entityXML ist (bisher) eine Konzeptstudie in der Version _0.5.1_ (ALPHA), die darauf abzielt, ein einheitliches XML basiertes Datenformat für die Text+ GND Agentur zu modellieren.

Das Format soll dabei vor allem drei Aspekte abdecken: Die Bereitstellung eines einheitlichen (1) **Austausch**\- und (2) **Speicherformats** zur Beschreibung von Entitäten, das direkt auf die GND gemappt werden kann, und der Text+ GND Agentur zudem als (3) **Workflow-Steuerungsinstrument** dient.

**Zielgruppe der IO-Lecture**

Die Veranstaltung richtet sich in erster Linie an Mitarbeitende der drei Text+ Datendomänen. Darüber hinaus freuen wir uns aber auch über Teilnehmende aus anderen NFDI-Konsortien. Im Zweifelsfalle kontaktieren Sie gerne das Text+ Operations Office.

**Hinweise**

Die IO-Lecture wird ggf. aufgezeichnet oder Screenshots erstellt. Wenn Sie nicht wünschen, dass Ihre persönlichen Informationen geteilt werden, schalten Sie bitte Ihre Webcam aus. Benutzernamen und Chatnachrichten werden **nicht** aufgezeichnet.

Diese Veranstaltung ist Teil der [IO-Lectures](https://events.gwdg.de/category/169/).

Anmeldung: https://events.gwdg.de/event/446/