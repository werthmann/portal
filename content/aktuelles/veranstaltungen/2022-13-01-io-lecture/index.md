---
type: event
title: "IO-Lecture: Git"
start_date: 2022-12-01 10:00:00
end_date: 2022-12-01 12:00:00
all_day: false
location: Zoom (Link wird am Vortag per E-Mail verschickt)
aliases:
- /events/io-lecture-git
---


![](images/Text-Logo-transp-EN-300x300.png)

Nach unserem [GitLab-Onboarding am 17.11.](https://pad.gwdg.de/3QefSWCKTZaTBX8C9wy7EQ?view) setzen wir die IO-Lectures mit einer Einführung in Git fort. Jasmin Oster, Entwicklerin bei der GWDG, steht uns dafür am Donnerstag, dem 1. Dezember von 10 bis 12 Uhr als Referentin zur Verfügung.

Es folgt eine Einführung in die verschiedenen Begrifflichkeiten (Repository, Commit, Branches) und eine allgemeine Nutzung von \`git\` auf der Kommandozeile. Die Einführung wird etwa eine Stunde in Anspruch nehmen. Die verbleibende Zeit im Anschluss kann dafür genutzt werden, um das Zusammenspiel zwischen GitLab und Git im Detail zu erklären oder auch um Fragen der Teilnehmenden zu beantworten.

Bitte meldet euch [hier im Eventmanagement](https://events.gwdg.de/event/367/) an, damit wir euch aktuelle Informationen und Materialien zur Verfügung stellen können.
