---
type: event
title: "Designworkshop NFDI & GND4C Community Empowerment"
start_date: 2022-12-06 13:00:00
end_date: 2022-12-06 15:30:00
all_day: false
location: Zoom
featured_image: images/gnd_logo.png
aliases:
- /events/designworkshop-nfdi-gnd4c-community-empowerment
---

In den Auftaktveranstaltungen zu den verschiedenen Text+ Foren wurde der Bedarf nach Schulungen und Workshops immer wieder geäußert. Dabei geht es sowohl um die Vermittlung von Kenntnissen, wie man die GND besser nutzen kann (Recherche, Abgleich, Verknüpfung mit den eigenen Daten, GND Einführung, Einführung in die GND-Erfassungsregeln) als auch um Schulungen für potenzielle Anwender\*innen, die zum Beispiel neue GND Datensätze anlegen, die Webformulare nutzen oder wissen wollen wie sie ihre Bedarfe an das Regelwerk adressieren können (Beteiligungswege, Eignungskriterien, uvm.).

Es wird jedoch ebenso um Metadaten und Normdaten gehen, denn auch hier bestehen Informationslücken. In dem Designworkshop wollen wir uns gemeinsam mit Partnern aus den NFDI-Konsortien und GND4C überlegen, wie ein modularer Baukasten von Workshops und Schulungen zum Community-Empowerment aussehen sollte und wie man evtl. Teile davon auch zum Selbststudium anbieten kann.

Eine Teilnahme am Workshop ist nur auf Einladung möglich. Kontakt: Barbara Fischer
