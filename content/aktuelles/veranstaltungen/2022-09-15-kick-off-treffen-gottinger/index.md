---
type: event
title: "Kick-off Treffen „Göttinger Digitale Akademie“"
start_date: 2023-09-15 14:00:00
end_date: 2023-09-15 18:00:00
all_day: false
location: hybrid (Göttingen/Zoom)
aliases:
- /events/kick-off-treffen-gottinger-digitale-akademie-am-donnerstag-15-09-22-1400-1800-uhr-hybrid-gottingen-zoom
---


Am Donnerstag dem 15.09.22 wird ein Kick-off Treffen des Projekts „Göttinger Digitale Akademie“ hybrid in Göttingen (Theaterstr. 7, Besprechungsraum im Erdgeschoss, ggf. auch Geiststraße 10) stattfinden, zu dem wir herzlich einladen möchten. Das Projekt wird seit Januar dieses Jahres vom Niedersächsischen Ministerium für Wissenschaft und Kultur über das Programm „Niedersachsen Vorab“ (Sprung) gefördert. Im Mittelpunkt des Treffens wird ein Gespräch am Runden Tisch zur Nationalen Forschungsdateninfrastruktur (NFDI) und den geisteswissenschaftlichen Konsortien (insb. Text+ und NFDI4Memory) stehen. Es werden Hoffnungen, Perspektiven und Unsicherheiten diskutiert sowie eine Zwischenbilanz gezogen. Es ist eine Runde mit vier TeilnehmerInnen vorgesehen.

- Fabian Cremer (IEG Mainz, NFDI4Memory)
- Bärbel Kröger (Germania Sacra, WIAG, NFDI4Memory)
- Regine Stein (Co-Sprecherin von Text+, NFDI4Culture, SUB Göttingen)
- Cord Wiljes (Direktorat der NFDI)  
    Jörg Wettlaufer, Leiter des Projekts „Göttinger Digitalen Akademie“, wird den runden Tisch moderieren. Nach einer Pause werden dann anschliessend digitale Projekte mit einem Fokus auf Forschungsdaten aus der Digitalen Akademie und den Kooperationsprojekten mit der SUB und der VZG kurz vorgestellt. Nach dem Ende der Veranstaltung gegen 18:15 Uhr wird es die Möglichkeit geben, gemeinsam zu essen und bei einem Bier oder Wein den Abend ausklingen zu lassen.

Programmübersicht:

14:00 Uhr – Beginn der Veranstaltung und Begrüßung

14:15 Uhr – 15:45 Uhr – Round Table NFDI und die geisteswiss. Konsortien

15:45 Uhr – 16:15 Uhr Kaffee-Pause

16:15 Uhr – 18:00 Uhr – Projektvorstellungen aus der Digitalen Akademie und den digitalen Kooperationsprojekten der AdWG

Moderation: Thomas Bode

16:15 – C. Beckers: Prize Papers  
16:30 – G. Bei der Wieden / J. Wettlaufer: KuVis / Visualisierung Kulturzeitschriften der Jahrhundertwende  
16:45 – T. Bode: Digitale Bibliothek der AdWG  
17:00 – B. Kröger: GS Klosterdatenbank  
17:15 – N.N.: Digitales Handbuch Residenzstädte  
17:30 – J.C. Schaffert: FWB Visualisierungen  
17:45 – N.N. Ein Projekt aus dem Friedländer Weg (TBC)

ca. 18:15 Uhr Möglichkeit gemeinsames Abendessen

Zur besseren Organisation bitten wir um Anmeldung bis spätestens zum 9.9.22 unter diesem [Link](https://digitale-akademie.adw-goe.de/ninja-forms/74hje).
