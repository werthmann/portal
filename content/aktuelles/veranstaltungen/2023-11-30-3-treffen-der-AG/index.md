---
type: event
title: "3. Treffen der AG GND Community Empowerment"
start_date: 2023-11-30 13:00:00
end_date: 2023-11-30 15:30:00
all_day: false
location: virtuell
---

Mit der AG GND Community Empowerment hat sich ein Format etabliert, bei dem gemeinsam an der Kenntnisvermittlung zur besseren Nutzung der GND und an der Schulung potenzieller Anwender*innen gearbeitet wird.

Im dritten Treffen der Arbeitsgruppe stellen wir weitere Bausteine zur Nachnutzung und Bearbeitung der GND vor und widmen uns den offenen Themen des letzten Treffens, ob wir die Communities im Blick haben und der Bedeutung des Curriculums für unsere Arbeit.

https://events.gwdg.de/event/563/