---
type: event
title: "InFoDiTexT+ Workshop: Mehr als nur Buchstabensuppe - Die Bibelstellen des Augsburger Interims erfasst mit BeautifulSoup"
start_date: 2023-01-25 17:15:00
all_day: true
location: online
featured_image: images/InFoDiText_Workshop_Wunsch-768x543-1.png
aliases:
- /events/infoditext-workshop-mehr-als-nur-buchstabensuppe-die-bibelstellen-des-augsburger-interims-erfasst-mit-beautifulsoup
---


Im Augsburger Interim tauchen eine Vielzahl von Bibelstellen auf, die Kevin Wunsch in unserem ersten InFoDiText+ Workshop mit Hilfe von beautifulSoup gemeinsam mit den Teilnehmer:innen erfassen wird.

Weiter Informationen: https://infoditex.hypotheses.org/veranstaltungen
