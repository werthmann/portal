---
type: event
title: "Nationale Forschungsdateninfrastruktur: Info-Veranstaltung der DGEKW & DGSKA"
start_date: 2022-06-30 10:00:00
end_date: 2022-06-30 11:45:00
aliases:
- /events/nationale-forschungsdateninfrastruktur-info-veranstaltung-der-dgekw-dgska
---

Die Fachgesellschaften für die Empirische Kulturwissenschaft/Europäische Ethnologie (DGEKW) und die Sozial- und Kulturanthropologie (DGSKA) laden ihre Mitglieder und Interessierte herzlich zu einer gemeinsamen digitalen Gesprächsrunde mit den Konsortien der Nationalen Forschungsdateninfrastruktur (NFDI) am Donnerstag, 30. Juni 2022 von 10:00 und 11:45 Uhr ein.