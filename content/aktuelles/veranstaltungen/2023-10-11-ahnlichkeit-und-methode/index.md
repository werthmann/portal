---
type: event
title: "Ähnlichkeit und Methode: Digitale Perspektiven für die Arbeit mit historischem Bildmaterial"
start_date: 2023-10-11
end_date: 2023-10-13
all_day: true
location: Wolfenbüttel
aliases:
- /events/ahnlichkeit-und-methode-digitale-perspektiven-fur-die-arbeit-mit-historischem-bildmaterial
---

_Tagung des Projekts „Automatische Bilderkennung frühneuzeitlicher Porträtgrafik als App“ (PortApp)  
Herzog August Bibliothek Wolfenbüttel  
Universität Hildesheim, Institut für Informationswissenschaft & Sprachtechnologie  
Wolfenbüttel, 11.–13.10.2023_

**Die aktuelle Entwicklung KI-gestützter Bilderkennung verändert den Umgang mit historischem Bildmaterial. Der Begriff der Ähnlichkeit erhält neue Relevanz und neue Bedeutungen. Anhand aktueller Projekte der bildorientierten digitalen Geisteswissenschaften geht die Tagung diesem Wandel in theoretischer und praktischer Perspektive nach.**

Die Suche nach Ähnlichkeiten kann als fundamentale Methode der Digital Humanities im Bereich der Bildanalyse gelten. Damit verweist die automatische Bildverarbeitung auf den Ähnlichkeitsbegriff, dessen produktive Neufassung ein generelles Desiderat der Bildwissenschaften ist. Seine Rückbindung an den Wahrnehmungsprozess und der hierüber einfließende Relativismus sorgen neben den in künstlerischen Werkprozessen bewusst erzeugten Ähnlichkeiten für eine Vervielfachung der Beschreibungsmöglichkeiten für Ähnlichkeit.

Die Entwicklung KI-gestützter Bilderkennung und die kontinuierliche Erweiterung digitaler Bildsammlungen im kunsthistorischen Bereich regt die Entwicklung von Ansätzen für deren Adressierbarkeit via Kriterien visueller Ähnlichkeit an. Neben Bildähnlichkeitssuchen für betont heterogene Datenbanken entstehen auf spezielle Sammlungen zugeschnittene Tools, die näher eingegrenzte Forschungsinteressen bedienen. So entwickelt das Projekt „Automatische Bilderkennung frühneuzeitlicher Porträtgrafik als App“ (PortApp) eine Bildähnlichkeitssuche für frühneuzeitliche druckgrafischer Porträts, ausgehend von der Sammlung der Herzog August Bibliothek Wolfenbüttel.

Die Kategorie der Ähnlichkeit erhält durch die aktuelle technische Entwicklung völlig neue Relevanz, insofern die maschinelle Analyse eine Vielzahl an Ähnlichkeiten zutage fördert, deren Einordnung nur durch umfassende Berücksichtigung der möglichen Beziehungen zwischen Bildern möglich ist. KI-gestützt festgestellte Ähnlichkeiten können eine Vielzahl an Ursachen haben, darunter Material und Technik, Darstellungskonventionen, Imitation und Reproduktion sowie die Identität des dargestellten Gegenstandes. In die Entwicklung automatischer Bilderkennungssysteme fließt daher eine Fülle von Vorannahmen ein, und die Suchergebnisse weisen eine kaum zu vermeidende Bias in Hinblick auf die ausgewählten Bildelemente und Trainingsmethoden auf. Herausforderung ist es daher, automatische Bilderkennung und Interpretation in eine produktive Wechselbeziehung zu bringen: Sind tradierte Ähnlichkeiten zwischen Bildern mit informationstechnischen Methoden nachvollziehbar? Wie sind maschinell zu ermittelnde Ähnlichkeiten bildwissenschaftlich zu erklären? Welche Visualisierungsverfahren stützen die bildwissenschaftliche Auswertung? Wie kann mit Transfers zwischen Bildmedien und künstlerischen Techniken umgegangen werden? Wie können ähnliche Bildwerke trotz Ergänzungen, Überarbeitungen und Variationen erkannt werden? Außerdem ist zu fragen, für welche kunsthistorischen Interessen KI-gestützte Ansätze, wie zum Beispiel die automatische Objekterkennung oder die digitale Form-/Stilanalyse, eingesetzt werden können – zur Klärung von Werkstattzusammenhängen, zum Aufspüren migrierender Bildmotive, zur Rekonstruktion von rezeptionsgeschichtlichen Konstellationen, oder schlicht zur Schaffung neuer Ordnungssysteme für digitale Bildsammlungen?

Die Tagung lädt sowohl Vertreter\*innen der Kunstgeschichte und Bildwissenschaften als auch Expert\*innen für KI und bildbezogene Digital Humanities (Digital Visual Studies) zur Diskussion ihrer Forschungen ein. In einem angeschlossenen Workshop sind die Teilnehmenden eingeladen, das Bildmaterial ihrer eigenen Projekte einer angeleiteten digitalen Analyse zu unterziehen. Hinzu kommen Präsentationen von Projekten und Neuentwicklungen auf diesem Feld.

Willkommen sind insbesondere Beiträge zu folgenden Themen:

- Erkenntnispotenzial von Ähnlichkeit in der digital gestützten Bildwissenschaft

- Aktuelle Methoden der bildorientierten digitalen Geisteswissenschaften

- Digitale Bildforschung in Bibliotheken, Archiven und Museen

- Forschungsdaten und Infrastrukturen für die Arbeit mit historischem Bildmaterial

Die Konferenzsprachen sind Englisch und Deutsch. Eine Veröffentlichung der Beiträge in einem gemeinsamen Band wird angestrebt.

Wir bitten um die Einreichung von **Vortragstitel mit einem kurzen Abstract bis zum 18.06.2023**. Für das Einbringen von Datencorpora in den Workshop ist eine rechtzeitige Anmeldung (bis Ende Juli 2023) und nach Möglichkeit Vorabübermittlung der Daten erforderlich. Für die einfache Teilnahme bitten wir um eine formlose Anmeldung vor der Tagung.

Kontakt:

**Dr. Hartmut Beyer**, Herzog August Bibliothek Wolfenbüttel. E-Mail: [beyer@hab.de](mailto:beyer@hab.de,)

**Prof. Dr. Thomas Mandl**, Universität Hildesheim, Institut für Informationswissenschaft & Sprachtechnologie. E-Mail: [mandl@uni-hildesheim.de](mailto:mandl@uni-hildesheim.de)

DHd-Blog: https://dhd-blog.org/?p=19262