---
type: event
title: "Workshop: Digitale Edition – Grundlagen und Perspektiven"
start_date: 2023-09-01
end_date: 2023-10-06
all_day: true
location: online
featured_image: 
aliases:
- /events/workshop-digitale-edition-grundlagen-und-perspektiven
---


Der Workshop im Zoom-Format bietet eine sechsteilige, praktische Einführung in die digitale Edition mit TEI für Editionsphilolog\*innen. Der Workshop wird die Grundlagen digitaler Edition, TEI’s und strukturierten Markups mit dem Oxygen Editor sowie die Darstellung der Edition im Browser behandeln. Hinzu kommen Grundlagen in relevanten XML-Datenbankstrukturen.

Die Workshop-Sitzungen werden praktisch orientiert, hands-on, sein und sind für Anfänger\*innen ohne Vorkenntnisse in digitaler Edition geeignet, etwa für interessierte erfahrene Editionswissenschaftler\*innen mit wenig digitalen Vorkenntnissen oder angehende wissenschaftliche Mitarbeiter\*innen. Im Laufe des Workshops werden die Teilnehmer\*innen Gelegenheit haben, mit eigenen Editionsmaterialien zu arbeiten und ihre eigenen Editionskonzepte zu entwickeln.

Im Veranstaltungskonzept sind Präsentationsanteile, eigenständige Übungen in Gruppen und individuelle Übungen vorgesehen. Die Zahl der Teilnehmer\*innen ist aus pragmatischen Gründen auf 25 Plätze begrenzt.

Verantwortliche Veranstalter\*innen des Workshops ist die Kommission AgE-DH der Arbeitsgemeinschaft für germanistische Edition, mit freundlicher Unterstützung des NFDI-Konsortiums Text+. Federführende Veranstalter, und Ansprechpartner für weitere Fragen zu diesem Workshop, sind (in alphabetischer Reihenfolge):

_Philipp Hegel (Technische Universität Darmstadt), philipp.hegel@tu-darmstadt.de  
Thorsten Ries (University of Texas at Austin), thorsten.ries@austin.utexas.edu  
Gabriel Viehhauser (Universität Stuttgart), viehhauser@ilw.uni-stuttgart.de_

Anmeldung: https://events.gwdg.de/event/531/