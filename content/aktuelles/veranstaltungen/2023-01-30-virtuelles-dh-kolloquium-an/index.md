---
type: event
title: "Virtuelles DH-Kolloquium an der BBAW"
start_date: 2023-01-30 16:15:00
all_day: false
location: https://meet.gwdg.de/b/lou-eyn-nm6-t6b
aliases:
- /events/virtuelles-dh-kolloquium-an-der-bbaw-3
---


Chris Biemann (Universität Hamburg) spricht über „Text Mining für die Digital Humanities im Spannungsfeld zwischen Forschung und Service am Beispiel von Begriffsgeschichte“.

Weitere Informationen: https://dhd-blog.org/?p=18936