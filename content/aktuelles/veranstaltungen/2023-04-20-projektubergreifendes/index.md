---
type: event
title: "Projektübergreifendes Arbeitstreffen nicht-bibliothekarischer Agenturen"
start_date: 2023-04-20 10:00:00
end_date: 2023-04-20 13:00:00
all_day: false
location: online
aliases:
- /events/projektubergreifendes-arbeitstreffen-nicht-bibliothekarischer-agenturen
---

Am 20. April von 10:00 bis 13:00 Uhr treffen sich projektübergreifend GND-Akteur\*innen, die jede/r für sich in unterschiedlichen Settings und Entwicklungsstufen am Aufbau von nachhaltigen Strukturen zur aktiven Mitarbeit in der GND als Datenhub und Organisation beteiligt sind. Alle haben bereits eigene Erfahrungen im Aufbau oder auch Nutzung der Strukturen des GND-Netzwerks gesammelt. Im Arbeitstreffen wollen wir uns zu gemeinsamen Themen oder Fragestellungen austauschen, die uns alle gleichermaßen beschäftigen.

- **10:00** **Begrüßung**

- **10:15** **Entity XML:** Präsentation und Diskussion des Tools mit dem Lieferdaten für den GND Import aufbereitet werden können **(**Stefanie Rühle, Susanne Al Eryani)

- **10:45 GND4C Toolbox**: Präsentation und Diskussion des Tools mit dem bestimmte Lieferdaten für den GND Import mit der GND abgeglichen werden können (Erdal Ayan, Michael Markert)

- **11:15** **Pause**

- **11:30** **Bulkupload in die GND**: Präsentation und Diskussion zu bestehenden Lieferwegen und Schnittstellen für Daten von der Agentur an die GND - semiautomatische Einspielung der Daten in die GND

- **12:30** **Offene Fragen & Wrap up**

Anmeldung: https://events.gwdg.de/event/466/