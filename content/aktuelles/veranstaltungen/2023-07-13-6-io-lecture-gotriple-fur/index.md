---
type: event
title: "6. IO-Lecture: GoTriple für Text+"
start_date: 2023-07-13 10:00:00
end_date: 2023-07-13 12:00:00
all_day: false
location: online
aliases:
- /events/6-io-lecture-gotriple-fur-text
---

In der 6. IO-Lecture werden Pattrick Piel und Marlen Töpfer (beide Max Weber Stiftung, National Node OPERAS-GER) GoTriple vorstellen, eine Discovery-Plattform für die Sozial- und Geisteswissenschaften.

**Programm**

1.      Was ist GoTriple im Rahmen von OPERAS?

2.      Wie sieht GoTriple aus?

3.      Wie arbeite ich mit GoTriple für meine eigene Forschung und/oder in der Beratung? (Hands-on)

**Was ist GoTriple?**

GoTriple ist eine Single-Access-Point für Dokumente, Projekte und Forscher:innenprofile aus den Sozial- und Geisteswissenschaften. Auf der Plattform ist es dabei möglich, auf 11 europäischen Sprachen zu suchen und Ergebnisse zu finden. Innovative Services wie ein visuelles Discovery-System, ein Annotationsservice, ein Trust Building System, ein Recommender-System und eine Crowdfunding-Plattform sind in die Plattform integriert.

**Zielgruppe der IO-Lecture**

Die Veranstaltung richtet sich in erster Linie an Mitarbeitende der drei Datendomänen von Text+, die Forschung innerhalb der Sozial- und Geisteswissenschaften betreiben und/oder in der Beratung tätig sind. Darüber hinaus freuen wir uns aber auch über Teilnehmende aus anderen NFDI-Konsortien. Im Zweifelsfalle kontaktieren Sie gerne das Text+ Operations Office.

Anmeldung: https://events.gwdg.de/e/gotriple