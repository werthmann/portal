---
type: event
title: "Edithaton zu den Guidelines for Quality Assessment and Assurance for Digital Editions"
start_date: 2024-01-29 13:00:00
end_date: 2024-01-30 14:00:00
all_day: false
location: Berlin-Brandenburgische Akademie der Wissenschaften
---

Die Kolleg:innen des Text+-Measures M3 Standardisierung der Datendomäne Editionen veranstaltet einen Editathon an der Berlin-Brandenburgische Akademie der Wissenschaften. Ziel ist es, der ersten Version der Guidelines for Quality Assessment and Assurance for Digital Editions auf Grundlage des im laufenden Jahr erarbeiteten Konzepts eine konkrete Gestalt zu geben, indem so viele Textbausteine einzeln oder in Kleingruppen erarbeitet werden, wie möglich.
Die Schreibwerkstatt soll im Rahmen eines Lunch-to-Lunchs den direkten und unkomplizierten Gedankenaustausch zwischen den Vertreter:innen der editionswissenschaftlichen Community ermöglichen und die Vorüberlegungen bündeln, die im vergangenen Jahr in Text+ gemacht wurden.

Datum und Uhrzeit
29. Januar 2024 / 13.00-18.00 Uhr mit anschließendem Abendessen vor Ort
30. Januar 2024/ 9.00-14.00 Uhr

Veranstaltungsort
Berlin-Brandenburgische Akademie der Wissenschaften
Tag 1 Konferenzraum 2 und 3 (1. OG)
Tag 2 Konferenzraum 1 (1. OG)
Jägerstr. 22/23, 10117 Berlin

Für das leibliche Wohl während der gesamten Arbeitszeit ist gesorgt. Die Kosten für Anreise und Unterbringung können leider nicht übernommen werden. Im Anhang der E-Mail finden Sie eine Übersicht über empfehlenswerte Hotels in der näheren Umgebung der Akademie.

Da die Anzahl der Plätze begrenzt ist, ist eine Anmeldung bis zum 20. Dezember 2023 bei karoline.lemke@bbaw.de erforderlich.
