---
type: event
title: "Virtuelles DH-Kolloquium an der BBAW"
start_date: 2022-11-28
location: virtuell https://meet.gwdg.de/b/lou-eyn-nm6-t6b 
aliases:
- /events/virtuelles-dh-kolloquium-an-der-bbaw
---


Heike Zinsmeister (Universität Hamburg) spricht über „Modellierung von Forschungsdaten durch Annotation: Konzepte, Prozesse und Formate“.