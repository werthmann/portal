---
type: event
title: "Praxis-Workshop OCR: Mit Optical-Character-Recognition-Verfahren Texte aus Bildern und Scans extrahieren"
start_date: 2023-05-08 13:00:00
end_date: 2023-05-09 13:15:00
all_day: false
location: Herzog August Bibliothek Wolfenbüttel
aliases:
- /events/praxis-workshop-ocr-mit-optical-character-recognition-verfahren-texte-aus-bildern-und-scans-extrahieren
---

In einem Workshop am 8. und 9. Mai 2023 können Forschende am praktischen Beispiel lernen, wie sie mit OCR-Verfahren Texte in Bildern und Scans gemeinfreier Werke identifizieren und herausfiltern. Die Veranstaltung findet in Präsenz an der Herzog August Bibliothek in Wolfenbüttel statt.

#### Was ist OCR?

Optical-Character-Recognition-Verfahren (OCR) ermöglichen es, Texte in Bildern oder Scans zu "erkennen" und diese durchsuchbar zu machen. Daher sind sie für die Disziplinen, die mit großen Textmengen arbeiten, ein wichtiges Tool und können die Forschungsarbeit maßgeblich erleichtern. Sie müssen die Texte nicht mehr selbst manuell abtippen und können sie professionell bearbeiten, edieren und dokumentieren. Für die Buch-, Bibliotheks- und Informationswissenschaft sind OCR-Verfahren nicht nur ein Tool, sondern ein Forschungsgegenstand per se.

Sie werden erfahren, wie OCR-Verfahren die Forschung schon jetzt beeinflussen und voranbringen  

Wie das Projekt OCR-D dazu beiträgt, dass OCR-Verfahren direkt durch die Bibliotheken eingesetzt werden, wenn diese gemeinfreie Werke digitalisieren und bereitstellen was bei der Anwendung von OCR-Verfahren zu beachten ist.

Wie man mit OCR4all "eigene" Texte aus Scans und Bildern extrahiert.

Wie ein gutes Forschungsdatenmanagement für OCR-Texte aussehen kann (Gastbeitrag von text+).

#### Wer kann teilnehmen?

Der Workshop richtet sich an Forschende aller Fächer, Vertreter:innen der Buch-, Bibliotheks- und Informationswissenschaft werden bei der Anmeldung bevorzugt. Die Teilnahme ist kostenlos. Anreise- und Übernachtungskosten werden nicht übernommen.

Der Workshop wird organisiert und gestaltet vom Fachinformationsdienst für Buch-, Bibliotheks- und Informationswissenschaft, OCR-D, OCR4all und Text+.

Anmeldung: https://events.gwdg.de/event/444/