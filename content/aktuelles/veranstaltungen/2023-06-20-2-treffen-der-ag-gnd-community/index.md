---
type: event
title: "2.Treffen der AG GND Community Empowerment"
start_date: 2023-06-20 10:00:00
end_date: 2023-06-20 12:30:00
all_day: false
location: online
aliases:
- /events/2-treffen-der-ag-gnd-community-empowerment
---

Die Öffnung und Modernisierung der GND setzen sowohl in den neuen Redaktionen als auch für die einfache Nachnutzung einiges an Wissen und Kompetenzen voraus. Daher haben sich eine Vielzahl von Einrichtungen in einer Arbeitsgruppe zusammengefunden, um für beide Kreise im Sinne des Community Empowerments zu wirken. Im ersten Treffen haben wir einen gemeinsamen Aktionsrahmen für die kollaborative Wissensvermittlung für das GND-Netzwerk erarbeitet, über den man in der [Dokumentation zum Treffen](https://wiki.dnb.de/pages/viewpage.action?pageId=263855986) mehr erfährt. Wir stellen im zweiten Arbeitstreffen erste Bausteine und geplante Vorhaben für Schulungen und Workshops vor, die Kenntnisse zur Nachnutzung und Bearbeitung der GND vermitteln.

Generell ist das Anliegen der Arbeitsgruppe sowohl die Vermittlung von Kenntnissen, wie man die GND besser nutzen kann (Recherche, Abgleich, Verknüpfung mit den eigenen Daten, GND Einführung, Einführung in die GND-Erfassungsregeln) als auch die Schulung für potentielle Anwender\*innen, die zum Beispiel neue GND Datensätze anlegen, die Webformulare nutzen oder wissen wollen, wie sie ihre Bedarfe an das Regelwerk adressieren können (Beteiligungswege, Eignungskriterien ...). Jedoch brauchen wir auch Angebote, die allgemein zu Meta- und Normdaten informieren, denn auch hier bestehen Informationslücken.

Anmeldung: https://events.gwdg.de/event/504/