---
title: "Home"
type: landing
---

{{<landing/call-to-action title="Text- und sprachbasierte Forschungsdaten nutzen und erhalten">}}
{{<lead-text>}}
Der Verbund Text+ wird text- und sprachbasierte Forschungsdaten langfristig erhalten und ihre breite Nutzung in der Wissenschaft ermöglichen.
{{</lead-text>}}

    {{<button-row align="end">}}
        {{<button url="/ueber-uns/struktur-und-governance/">}}Über Text+{{</button>}}
        <!-- tab: 'metadata' -->
        {{<search-button is_data_search="true">}} Daten suchen {{</search-button>}}
    {{</button-row>}}

{{</landing/call-to-action>}}

## Vision und Mission

Vision:
Die text- und sprachorientierten Geistes- und Sozialwissenschaften nutzen die Möglichkeiten der Digitalisierung in ihrer Forschung und Lehre sowie im Transfer umfassend und haben eine gemeinsame Datenkultur.

Mission:
Wir ertüchtigen Text- und Sprachdaten und den Zugang zu solchen Daten. Der Zugriff auf digitale Quellen soll zum Standard werden. Wir stärken die Digital Literacy der Forschenden. Wir decken die Diversität der Forschungsgemeinde ab und bauen auf ihre Partizipation. Wir befördern Inter- und Transdisziplinarität sowie Innovation – etwa im Bereich der künstlichen Intelligenz – durch Integration von Infrastruktur und Forschung.

## Arbeitsbereiche in Text+

Die Text+ Infrastruktur ist auf Sprach- und Textdaten ausgerichtet und konzentriert sich zunächst auf digitale Sammlungen, lexikalische Ressourcen und Editionen. Diese sind von hoher Relevanz für alle sprach- und textbasierten Disziplinen, speziell für Sprachwissenschaften, Literaturwissenschaften, Philosophie, Klassische Philologie, Anthropologie, außereuropäische Kulturen und Sprachen sowie sprach- und textbasierte Forschung der Sozial‑, Wirtschafts-, Politik‑, und Geschichtswissenschaften. Infrastruktur/Betrieb verfolgt das Ziel eines integrierten und aufeinander abgestimmten Text+-Angebotsportfolios, das aus interoperablen Angeboten und Diensten besteht.

{{<landing/data-domain-links>}}


## User-Stories 2020

Zur Integration des Nutzendenbedarfs haben wir vor Förderbeginn Forschende eingeladen, sich mit User Stories aus ihrem Forschungsumfeld zu beteiligen. Ziel war es, eine große Bandbreite der beteiligten Disziplinen, Datendomänen und Forschungsfragen zu erfassen.

{{<button url="/themen-dokumentation/user-storys-2020/">}}
Zu den User-Stories 2020
{{</button>}}
