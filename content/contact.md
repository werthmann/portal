---
title: "Kontakt"

aliases:
- /kontakt

---

# Kontakt und Beratung

Individuelle Unterstützung und Beratung zu sämtlichen über Text+ angebotenen Diensten und Werkzeugen sowie weiteren Angeboten: Hierfür steht Ihnen unser Helpdesk zur Verfügung. Er bietet Beratung in allen Phasen des Datenlebenszyklus‘: Zur Unterstützung bei der Entwicklung von spezifischen Datenmanagementplänen für neue Forschungsprojekte in den Text+-Datendomänen ebenso wie bei der „FAIRification“ und „CAREification“ vorhandener Datenbestände.

Unser Helpdesk besteht aus einem vielfältigen Netzwerk von Expertinnen und Experten der antragstellenden und beteiligten Institutionen von Text+. Sie stehen Ihnen mit ihrem Fachwissen aus den drei Datendomänen sowie dem Bereich Infrastruktur/Betrieb für das Beratungsangebot zur Seite. Je nach Anliegen stellen wir darüber hinaus gern für Sie den Kontakt zu nationalen und internationalen Verbänden und Verbünden her.

Zur Kontaktaufnahme steht Ihnen unser Webformular zur Verfügung. Ihre Anfrage wird umgehend an die Mitarbeiterinnen und Mitarbeiter des Helpdesks weitergeleitet. Sollten Sie keine Bestätigung zum Eingang Ihrer Anfrage erhalten, schreiben Sie bitte eine E-Mail an: `textplus-support[at]gwdg.de` 

(Bitte beachten Sie, dass aus Sicherheitsgründen das Formular erst nach zehn Sekunden abgeschickt werden kann und dass dies maximal zweimal innerhalb von fünf Minuten möglich ist. Andernfalls lösen Sie eine Fehlermeldung aus.)

## Kontakt aufnehmen
{{<support-form>}}

### Der Text+-Helpdesk ist vielfältig
#### Allgemeines
Beantwortet allgemeine und projektübergreifende Anfragen und informiert über Partizipationsmöglichkeiten in Text+.

#### Datendomäne Collections
Bietet Beratung zu sprach- und textbasierten Sammlungsdaten, z.B. Beratung zur Digitalisierung von analogen Text- und Sprachobjekten sowie Text- und Datamining. Beratung zu sammlungsspezifischen Angeboten und Datenmanagementplänen.

#### Datendomäne Lexikalische Ressourcen
Bietet Beratung zu lexikalischen Ressourcen, z.B. der standardkonformen Digitalisierung und Datenkuratierung von Altbeständen sowie der empirischen Ressourcennutzung. Beratung zu spezifischen Angeboten und Datenmanagementplänen für lexikalische Ressourcen.

#### Datendomäne Editionen
Bietet Unterstützung für alle Arten von Editionen aus verschiedenen Forschungsfeldern und alle Phasen des Editionsprozesses. Beratung zu editionsspezifischen Angeboten und Datenmanagementplänen.

#### Infrastruktur/Betrieb
Beratung zu Infrastrukturangeboten und Querschnittsthemen wie z.B. Datenqualität, Interoperabilität, Langzeitarchivierung, Nachnutzbarkeit, Registries und Linked Open Data.

#### Technischer Support
Bietet technische Hilfestellung zu den Angeboten des Text+-Portfolios.

#### Rechtliches und Ethisches
Informiert zu rechtlichen und ethischen Belangen in der text- und sprachbasierten Forschung, wie z.B. dem Urheberrecht (keine Rechtsberatung).
