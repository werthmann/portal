---
title: Collaborative Projects

aliases:
- /research-data/collaborative-projects

menu:
  main:
    weight: 40
    parent: vernetzung
---

# Collaborative Projects

## Call for Proposals 2023: Processing and Securing Research Data in Collaboration with Text+

Text+ is part of the National Research Data Infrastructure (NFDI) with the aim of establishing a distributed research data infrastructure for humanities research data. The focus is on the data areas of digital collections, lexical resources, and editions. As part of community engagement, Text+ conducts an annual call for proposals for collaborative projects.

The goal of this call is to continuously expand the offerings of data and services from Text+ and make them available to the research community in the long run. To this purpose, projects with a maximum duration of one calendar year are funded, and their results are integrated into the Text+ infrastructure. This can be achieved, for example, by processing existing data in a way that makes them (permanently) available to the community through a [data center](/en/vernetzung/daten-kompetenzzentren/) already connected to the Text+ infrastructure. Alternatively, established and sustainable data centers that interface to the technical infrastructure of Text+ make the relevant data available through the Text+ infrastructure. For software and other services, the expectation is that the result will be provided under an open license, aligned with Text+ (providing coherence) and can be operated long-term for the infrastructure. Provision according to the [FAIR](https://doi.org/10.1038/sdata.2016.18) and [CARE](https://www.gida-global.org/care) principles is essential. According to the guidelines of the NFDI, only works that aim to be integrated into the Text+ service portfolio are eligible for funding. The establishment and expansion of resources themselves are to be financed using own funds.

Funding proposals can be submitted for one of the data domains: Collections, Lexical Resources, Editions, and the Task Area Infrastructure/Operation. To prepare the proposals, a consultation with the respective area is recommended. A list of contacts can be found at the end of this call.

In these calls for proposals, multiple projects can typically be funded with a volume ranging from 35,000 to 60,000 EUR. In addition, an overhead of 22% is granted on the project sum. The project duration is tied to the calendar year 2024, with a maximum duration of 12 months; the transfer of unspent funds to the following year is not possible. After the completion of the funding, a project report is expected to be published on the Text+ website.

Any questions can be addressed to office@text-plus.org. Further information on the application process and the projects that have been approved so far will be provided on the Text+ website.

### Timeline
- Application deadline: April 30, 2023
- Announcement during the Text+ Plenary 2023: September 27 – 29, 2023
- Earliest start of funding: January 01, 2024
- End of funding at the latest: December 31, 2024

The next call for proposals will take place in 2024 for the project year 2025.

### Eligibility
Eligible to apply are departments and working groups that are not already financially supported within the framework of Text+, but comply with the DFG funding guidelines for the NFDI (https://www.dfg.de/formulare/nfdi100/nfdi100_en.pdf, Section III (1)).

The evaluation process will be led by the coordination committees of the respective working areas. If members of the coordination committees are involved in applications, these members will be excluded from the evaluation process in the respective year.

Funding requires the conclusion of a subcontracting agreement within the framework of the fund transfer and cooperation agreement in the NFDI project with the Leibniz Institute for the German Language Mannheim as the main applying institution of Text+. If the necessary conditions are met, especially a long-term and lasting contribution to the Text+ consortium beyond the collaboration project, an alternative is to join the fund transfer and cooperation agreement (and thus the Text+ consortium). However, repeated funding of the same project as a collaborative project is not possible.

### Selection Criteria
The evaluation of the proposals will be based on the following criteria:
- Linkage and integration into the Text+ infrastructure according to FAIR and CARE principles and the expansion of Text+'s portfolio regarding data or services within the funding period.
- Relevance and innovation.
- Potential scientific contribution of the data or services.
- Clear problem description and reference to the work programs of the Text+ consortium.
- Reusability and accessibility of the data or services.
- Scientific quality of the project.
- Contribution to diversity and representativity of the Text+ data offerings.
- Effort estimation.
- Appropriateness of the requested funding amount for implementation.
- Preliminary work and contributions of the applicants.

### Contacts
Listed are the Task Area Leads and the Task Area Coordinators. For initial contact, it is recommended to approach the coordinators.
- Collections: Head: [Dr. Peter Leinen](mailto:p.leinen@dnb.de), Coordination: [Philippe Genêt](mailto:P.Genet@dnb.de)
- Lexical Resources: Head: [PD Dr. Alexander Geyken](mailto:geyken@bbaw.de), Coordination: [Axel Herold](mailto:herold@bbaw.de)
- Editions: Head: [Prof. Dr. Andreas Speer](mailto:andreas.speer@uni-koeln.de), Coordination: [Kilian Hensen](mailto:Kilian.Hensen@uni-koeln.de)
- Infrastructure/Operation: Head: [Prof. Dr. Philipp Wieder](mailto:philipp.wieder@gwdg.de), Coordination: [Stefan Buddenbohm](mailto:stefan.buddenbohm@sub.uni-goettingen.de)
- Also reachable are [Lukas Weimer](mailto:lukas.weimer@sub.uni-goettingen.de) and [Thorsten Trippel](mailto:trippel@ids-mannheim.de), who work in the Text+ Office.

### Proposal Template
Please use the [Word template for your proposal](Template-Collaborative-Projects-Text.docx).

Any questions can be addressed to office@text-plus.org.

---
---
---

## Collaborative Projects Funded in the 1st Call (2022)

Each collaborative project is associated with the assigned Text+ Task Area, i.e. Collections, Editions, and Lexical Resources. Assignments to the Task Area Infrastructure/Operation were only possible from the 2nd call onward.

- **Unlocking Disk Magazines from the 1980s and 1990s: Re-Digitizing Digital Heritage from the Pioneer Era of Home Computers for Text-Based Diskmag Research (Diskmags)**
  - **Task Area:** Collections
  - **Institution:** University of Würzburg
  - **Project Leader:** Torsten Roeder

- **Open Access to DLA Marbach Data Based on Research Project-Related Questions. Developed Using the Example of Data from the Exile Library Source Repertory (Alfred Döblin and Siegfried Kracauer) (DLA Data+)**
  - **Task Area:** Collections
  - **Institution:** German Literature Archive Marbach
  - **Project Leader:** Karin Schmidgall

- **Integration of the Digital Program Archive of German Adult Education Centers into the NFDI Consortium Text+ (DiPA+)**
  - **Task Area:** Collections
  - **Institution:** German Institute for Adult Education
  - **Project Leader:** Kerstin Hoenig

- **KOLIMO+. Optimization and Open Publication of the Corpus of Literary Modernism (KOLIMO+)**
  - **Task Area:** Collections
  - **Institution:** University of Bielefeld
  - **Project Leaders:** Berenike Herrmann, Alan Lena van Beek

- **Modeling of Text Editions as Linked Data (edition2LD)**
  - **Task Area:** Editions
  - **Institution:** Heidelberg Academy of Sciences
  - **Project Leaders:** Dieta Svoboda-Baas and Sabine Tittel

- **Pessoa Digital**
  - **Task Area:** Editions
  - **Institution:** University of Rostock
  - **Project Leader:** Ulrike Henny-Krahmer

- **European Peace Treaties of the Premodern Era in Data (FriVer+)**
  - **Task Area:** Editions
  - **Institution:** Leibniz Institute for European History (IEG)
  - **Project Leader:** Thorsten Wübbena

- **Text+-Interface for the Middle High German Dictionary (MWB-APIplus)**
  - **Task Area:** Lexical Ressources
  - **Institution:** University of Trier / Göttingen Academy
  - **Project Leaders:** Ute Recker-Hamm, Ralf Plate, Jonas Richter

- **Integration of Lower Sorbian-German Dictionaries as Lexical Resources in Text+ (INSERT)**
  - **Task Area:** Lexical Ressources
  - **Institution:** Sorbian Institute
  - **Project Leader:** Hauke Bartels

- **Corpus Glossariorum Latinorum Online Lemmatization (CGLO)**
  - **Task Area:** Lexical Ressources
  - **Institution:** Bavarian Academy of Sciences
  - **Project Leader:** Adam Gitner

---
---
---

## FAQ

{{<accordion>}}
{{<accordion-item title="Objective of Collaborative Projects">}}
**What is the objective of collaborative projects?**

Collaborative projects within the Text+ framework aim to expand Text+'s portfolio of offerings. Text+ is open to new offerings and participants, not limiting itself to the offerings of initial partners. It can integrate additional data, services, and resources as infrastructure. Those offering a collaborative project can become part of the Text+ network, participate in further infrastructure development, and benefit from these advancements.
{{</accordion-item>}}

{{<accordion-item title="Eligibility">}}
**If a proposal for a collaborative project is not funded in one year, can the proposal be submitted again in a later round? Can a proposal be submitted in one of the subsequent rounds if funding was not possible previously?**

Absolutely. During the project evaluation, a priority list of proposals is created, and projects can be approved until the budget for that funding round is exhausted. This does not necessarily mean that a project not funded in one year is ineligible for funding. Of course, a proposal can be strengthened and updated with additional preparatory work before resubmission. In specific cases, discussions should be held with those coordinating the respective data domain (see the question "Who can be approached in advance of the proposal to align the planning and conception of collaborative projects with the requirements of Text+?" under the section "Proposal Submission").

**Who can submit a proposal? Can students/Ph.D. candidates/initiatives submit a proposal?**

Eligible to apply are departments and working groups not already financially supported within the Text+ framework but complying with the DFG FUNDING GUIDELINES FOR THE NFDI (SECTION III (1) (https://www.dfg.de/formulare/nfdi100/nfdi100_en.pdf)). Individual researchers and consortia of researchers (e.g., in collaborative projects) can submit a proposal through their university or research institution if they belong to the institutions mentioned in the DFG guidelines. Many universities and research institutions have their own departments dealing with research funding and third-party projects. Researchers should contact the relevant department at their home institution to clarify the conditions for submitting a proposal.

**Can individuals also submit proposals, or must they always be teams? Can two researchers submit proposals?**

There are no requirements regarding the size of a team conducting a collaborative project. However, only departments and working groups not already financially supported within the Text+ framework but complying with the DFG FUNDING GUIDELINES FOR THE NFDI (SECTION III (1), see https://www.dfg.de/formulare/nfdi100/nfdi100_en.pdf) are eligible to apply. Individual researchers and consortia of researchers (e.g., in collaborative projects) can submit a proposal through their university or research institution if they belong to the institutions mentioned in the DFG guidelines. In specific cases, discussions should be held with those coordinating the respective data domain (see the question "Who can be approached in advance of the proposal to align the planning and conception of collaborative projects with the requirements of Text+?" under the section "Proposal Submission").

**Can researchers from other countries participate in a proposal?**

In some projects, researchers from abroad contribute significantly, providing expertise beyond the national perspective. They can express their contributions through letters of support. Grant recipients are bound by the DFG funding guidelines (see http://www.dfg.de/formulare/nfdi300/nfdi300_de.pdf), and grant recipients must be eligible to apply (see DFG funding guidelines for the NFDI Section III (1) https://www.dfg.de/formulare/nfdi100/nfdi100_en.pdf).
{{</accordion-item>}}

{{<accordion-item title="Application Process">}}
**Are there further application rounds planned?**

Yes, there are additional application rounds scheduled for the years 2024 (funding for 2025) and 2025 (funding for 2026).

**What role do specialist associations play in the application process?**

Specialist associations and networks play a crucial role in collaborative projects. As part of the plenary assembly, they participate in building and composing committees that prioritize applications and accompany the call. Additionally, associations and networks can support project proposals by providing their own assessments of the project applications in the form of support letters.

**How extensive should the application be?**

The application template includes information on page numbers for individual sections of the application. This template serves to provide a quick overview of the submitted projects in the evaluation process. The page numbers also serve as indications of the extent of each section. Applicants should orient themselves based on the specified extents. In specific cases, inquiries can be clarified with those who coordinate the respective data domain (see the question "Who can be contacted in advance of the application to align the planning and conception of collaborative projects with the requirements of Text+?" under the "Application Process").

**Who can be contacted in advance of the application to align the planning and conception of collaborative projects with the requirements of Text+?**
- Collections: Leadership: [Dr. Peter Leinen](mailto:p.leinen@dnb.de), Coordination: [Philippe Genêt](mailto:P.Genet@dnb.de)
- Lexical Resources: Leadership: [PD Dr. Alexander Geyken](mailto:geyken@bbaw.de), Coordination: [Axel Herold](mailto:herold@bbaw.de)
- Editions: Leadership: [Prof. Dr. Andreas Speer](mailto:andreas.speer@uni-koeln.de), Coordination: [Kilian Hensen](mailto:Kilian.Hensen@uni-koeln.de)
- Infrastructure/Operation: Leadership: [Prof. Dr. Philipp Wieder](mailto:philipp.wieder@gwdg.de), Coordination: [Stefan Buddenbohm](mailto:stefan.buddenbohm@sub.uni-goettingen.de)
- Also available for contact are [Lukas Weimer](mailto:lukas.weimer@sub.uni-goettingen.de) and [Thorsten Trippel](mailto:trippel@ids-mannheim.de), who work in the Text+ Office.

{{</accordion-item>}}

{{<accordion-item title="Evaluation and Assessment Process">}}
**Will the subsequent application rounds be similar to those in 2022, or is there a plan to potentially make the application criteria or criteria for potential projects more specific, narrower, or different?**

Based on the experiences of the first application round, the criteria and their suitability will be reviewed and possibly revised for subsequent rounds.
{{</accordion-item>}}

{{<accordion-item title="Data">}}
**How extensive should the data be that can be contributed within the framework of a collaborative project?**

There is no completed list of recommended and acceptable data formats. It is recommended to align with formats already used (and "approved") by partners. There are also standard formats, such as the Text Encoding Initiative (TEI), W3C, and ISO. A rule of thumb is that a format is easier to integrate the closer it aligns with the standard and can be easily converted into formats already used within Text+. In specific cases, discussions should be held in advance with those who coordinate the respective data domain (see the question "Who can be contacted in advance of the application to align the planning and conception of collaborative projects with the requirements of Text+?" under the "Application Process").

**Which data formats are accepted for data in collaborative projects by Text+ partners?**

There is no completed list of recommended and acceptable data formats. It is recommended to align with formats already used (and "approved") by partners. There are also standard formats, such as the Text Encoding Initiative (TEI), W3C, and ISO. A rule of thumb is that a format is easier to integrate the closer it aligns with the standard and can be easily converted into formats already used within Text+. In specific cases, discussions should be held in advance with those who coordinate the respective data domain (see the question "Who can be contacted in advance of the application to align the planning and conception of collaborative projects with the requirements of Text+?" under the "Application Process").

**What does "Integration of Data and Services in Text+" mean?**

Integration refers to measures through which data and services are made accessible in the long term for users of the Text+ infrastructure. This can be achieved, for example, by hosting data from other Text+ partners. Another possibility is to operate its own sustainable (certified) repository and connect it to Text+ via interfaces. The metadata is provided via corresponding interfaces. Integration also includes that no post-digitization is necessary.

**If interesting data collections are discovered, how can they be made accessible through Text+? Can Text+ be proposed to take care of the preparation and integration of data?**

Text+ focuses on research data available in digital formats. Digitization by Text+ is currently not part of the service portfolio. If resource types are to be contributed that do not fall within the focus of a data and competence center of Text+, there is an integration possibility to establish its own repository for these data types. The adoption of offers requires clarification of rights and long-term responsibilities in any case.
{{</accordion-item>}}

{{<accordion-item title="Formalities">}}
**What does the conclusion of a cooperation agreement with the Leibniz Institute for the German Language specifically include?**

Collaborative projects within the framework of Text+ are based on the usage guidelines of the DFG for NFDI consortia (see http://www.dfg.de/formulare/nfdi300/nfdi300_de.pdf). The partners carrying out collaborative projects become part of the Text+ consortium. Collaboration and fund allocation are regulated in Text+ through a fund allocation and cooperation agreement, which is bilaterally concluded between the Leibniz Institute for the German Language and the respective partners. For questions about the agreement, the Text-Plus-Office (office@text-plus.org) is available as a contact.

**To which Text+ domain (Collections, Lexical Resources, Editions, Infrastructure/Operations) is a project proposal assigned?**

The structure of Text+ is based on the data domains Collections, Lexical Resources, and Editions. Additionally, there is the overarching category of "Infrastructure/Operations."
{{</accordion-item>}}

{{<accordion-item title="Funds">}}
**Which types of costs can be applied for?**

According to the funding guidelines of the DFG, personnel costs and material resources (travel expenses) can be applied for. In total, they should not exceed the amount of 60,000 euros. Capital goods (servers, computer equipment, workspace, devices, etc.) are not eligible for funding.
{{</accordion-item>}}



{{</accordion>}}
