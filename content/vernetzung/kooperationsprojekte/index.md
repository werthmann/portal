---
title: Kooperationsprojekte

aliases:
- /forschungsdaten/kooperationsprojekte

menu:
  main:
    weight: 40
    parent: vernetzung
---

# Kooperationsprojekte

## Ausschreibungsrunde 2023: Forschungsdaten aufbereiten und sichern gemeinsam mit Text+

Text+ ist Teil der Nationalen Forschungsdateninfrastruktur (NFDI) mit dem Ziel, eine ortsverteilte Forschungsdateninfrastruktur für geisteswissenschaftliche Forschungsdaten aufzubauen. Der Fokus liegt dabei auf den Datenbereichen digitale Sammlungen, lexikalische Ressourcen und Editionen. Als Teil der Communityanbindung führt Text+ jedes Jahr eine Ausschreibungsrunde für Kooperationsprojekte durch.

Ziel dieser Ausschreibung ist es, die Angebote an Daten und Services von Text+ kontinuierlich zu erweitern und für die Community der Forschenden langfristig verfügbar zu machen. Dazu werden auf maximal ein Kalenderjahr befristete Projekte gefördert, deren Ergebnisse in die Text+ Infrastruktur integriert werden. Dies kann z. B. dadurch geschehen, dass im Rahmen dieser Projekte vorhandene Daten so aufbereitet werden, dass sie durch ein bereits an die Text+ Infrastruktur angebundenes [Datenzentrum](/vernetzung/daten-kompetenzzentren/) für die Community (dauerhaft) verfügbar werden. Alternativ können eigene institutionalisierte und nachhaltige Datenzentren mit Schnittstellen in die technische Infrastruktur von Text+ eingebunden werden, um die betreffenden Daten in der Text+ Infrastruktur verfügbar zu machen. Bei Software und anderen Services wird erwartet, dass das Ergebnis unter einer offenen Lizenz bereitgestellt wird, mit Text+ abgestimmt ist (Leistung eines Kohärenzbeitrags) und langfristig für die Infrastruktur betrieben werden kann. Die Bereitstellung entsprechend den [FAIR](https://doi.org/10.1038/sdata.2016.18)- und [CARE](https://www.gida-global.org/care)-Prinzipien ist dabei wesentlich. Nach den Richtlinien der NFDI sind nur Arbeiten förderfähig, die auf die Integration in das Angebotsportfolio von Text+ abzielen. Aufbau und Ausbau von Ressourcen selbst sind hingegen aus Eigenmitteln zu tragen.

Förderanträge können für eine der Datendomänen Collections, lexikalische Ressourcen, Editionen sowie für die Task Area Infrastruktur/Betrieb gestellt werden. Zur Vorbereitung der Anträge sollte ein Beratungsgespräch mit dem zuständigen Bereich erfolgen. Die Liste der Ansprechpersonen finden Sie am Ende dieser Ausschreibung.

Im Rahmen dieser Ausschreibungen können mehrere Projekte in der Regel mit einem Volumen zwischen 35.000 und 60.000 EUR gefördert werden. Zusätzlich wird auf die Projektsumme ein Overhead von 22% gewährt. Die Laufzeit der Projekte ist an das Kalenderjahr 2024 gebunden und beträgt dadurch maximal 12 Monate; eine Übertragung von nicht ausgegebenen Mitteln auf das Folgejahr ist nicht möglich. Nach Abschluss der Förderung wird ein Projektbericht erwartet, der auf der Text+ Webseite veröffentlicht werden soll.

Eventuelle Rückfragen können Sie gerne an office@text-plus.org adressieren. Weitere Informationen zum Antragsverfahren und zu den bisher bewilligten Projekten werden auf der Text+ Webseite bereitgestellt.

### Zeitplanung
- Bewerbungsende: 30. April 2023
- Bekanntgabe im Rahmen des Text+ Plenary 2023: 27. – 29. September 2023
- Frühestmöglicher Förderbeginn: 01. Januar 2024
- Ende der Förderung spätestens: 31. Dezember 2024

Die nächste Ausschreibungsrunde wird 2024 für das Projektjahr 2025 erfolgen.

### Antragsberechtigung
Antragsberechtigt sind Abteilungen und Arbeitsgruppen, die nicht bereits im Rahmen von Text+ finanziell gefördert werden, die aber den DFG-Förderrichtlinien für die NFDI (https://www.dfg.de/formulare/nfdi100/nfdi100_en.pdf,  Abschnitt III (1)) entsprechen.

Das Begutachtungsverfahren wird durch die Koordinationskomitees der jeweiligen Arbeitsbereiche geleitet. Falls Mitglieder der Koordinationskomitees an Anträgen beteiligt sind, werden diese Mitglieder vom Begutachtungsprozess im jeweiligen Jahr ausgeschlossen.

Die Förderung setzt den Abschluss eines Nachunternehmervertrags im Rahmen des Mittelweiterleitungs- und Kooperationsvertrags im NFDI-Projekt mit dem Leibniz-Institut für Deutsche Sprache Mannheim als hauptantragsstellender Einrichtung von Text+ voraus. Bei Vorliegen der notwendigen Voraussetzungen, insbesondere eines langfristigen und dauerhaften Beitrags zum Konsortium Text+ über das Kooperationsprojekt hinaus, ist alternativ der Beitritt zum Mittelweiterleitungs- und Kooperationsvertrag (und damit zum Konsortium Text+) möglich. Wiederholte Förderungen desselben Projektes als Kooperationsprojekt sind dagegen nicht möglich.

### Auswahlkriterien
Die Begutachtung der Anträge wird nach den folgenden Kriterien erfolgen:
- Verknüpfung und Integration in die Text+ Infrastruktur nach den FAIR- und CARE-Prinzipien und die Erweiterung des Angebotsportfolios von Text+ bezüglich Daten bzw. Services innerhalb des Förderzeitraums
- Relevanz und Innovation
- potenzieller wissenschaftlicher Beitrag der Daten bzw. Services
- klare Problembeschreibung und Bezug zu den Arbeitsprogrammen des Konsortiums Text+
- Nachnutzbarkeit und Zugänglichkeit der Daten bzw. Services
- wissenschaftliche Qualität des Projekts
- Beitrag zu Diversität und Repräsentativität des Text+ Datenangebots
- Aufwandsabschätzung
- Angemessenheit der beantragten Fördersumme für die Realisierung
- Vorarbeiten und eigene Beiträge der Antragsstellenden


### Ansprechpersonen
Aufgeführt sind die Task Area-Leads sowie die Task Area-Koordinatoren. Für eine erste Kontaktaufnahme empfiehlt es sich, die Koordinatoren anzusprechen.
- Collections: Leitung: [Dr. Peter Leinen](mailto:p.leinen@dnb.de), Koordination: [Philippe Genêt](mailto:P.Genet@dnb.de)
- Lexikalische Ressourcen: Leitung: [PD Dr. Alexander Geyken](mailto:geyken@bbaw.de), Koordination: [Axel Herold](mailto:herold@bbaw.de)
- Editionen: Leitung: [Prof. Dr. Andreas Speer](mailto:andreas.speer@uni-koeln.de), Koordination: [Kilian Hensen](mailto:Kilian.Hensen@uni-koeln.de)
- Infrastruktur/Betrieb: Leitung: [Philipp Wieder](mailto:philipp.wieder@gwdg.de), Koordination: [Stefan Buddenbohm](mailto:stefan.buddenbohm@sub.uni-goettingen.de)
- Ebenfalls ansprechbar sind [Lukas Weimer](mailto:lukas.weimer@sub.uni-goettingen.de) sowie [Thorsten Trippel](mailto:trippel@ids-mannheim.de), die im Text+ Office mitarbeiten.


### Bewerbungsvorlage
Bitte nutzen Sie die hier hinterlegte [Word-Vorlage für Ihre Bewerbung](Template-Kooperationsprojekte-Text.docx).

Eventuelle Rückfragen können Sie gerne an office@text-plus.org adressieren.

---

## In der 1. Ausschreibungsrunde (2022) geförderte Kooperationsprojekte
Jedes Kooperationsprojekt ist mit der zugeordneten Text+ Task Area versehen, d.h. Col = Collections, Ed = Editions sowie LR = Lexical Resources. Zuordnungen zur Task Area Infrastuktur/Betrieb sind erst ab der 2. Ausschreibungsrunde möglich.

- Erschließung von Diskettenmagazinen der 1980er und 1990er: Digitales Kulturerbe aus der Pionierzeit der Heimcomputer für textbasierte Diskmags Forschung re-digitalisieren (Diskmags) Col Universität Würzburg/Torsten Roeder
- Offener Zugang zu den Daten des DLA Marbach auf Basis forschungsprojektbezogener Fragestellungen. Entwickelt am Beispiel der Daten des Quellenrepertorium der Exilbibliotheken (Alfred Döblin und Siegfried Kracauer) (DLA Data+) Col Deutsches Literaturachiv Marbach/Karin Schmidgall
- Integration des Digitalen Programmarchivs deutscher Volkshochschulen in das NFDI-Konsortium Text+ (DiPA+) Col Deutsches Institut für Erwachsenenbildung /Kerstin Hoenig
- KOLIMO+. Optimierung und Offene Publikation des Korpus der literarischen Moderne (KOLIMO+) Col Universität Bielefeld/Berenike Herrmann, Alan Lena van Beek
- Modellierung von Texteditionen als Linked Data (edition2LD) Ed Heidelberger Akademie der Wissenschaften/ Dieta Svoboda-Baas und Sabine Tittel
- Pessoa digital Ed Uni Rostock/Ulrike Henny-Krahmer
- Europäische Friedensverträge der Vormoderne in Daten (FriVer+) Ed Leibniz-Institut für Europäische Geschichte (IEG)/Thorsten Wübbena
- Text+-Schnittstelle für das Mittelhochdeutsche Wörterbuch (MWB-APIplus) LR Uni Trier/Akademie Göttingen/ Ute Recker-Hamm, Ralf Plate, Jonas Richter
- Integration NiederSorbisch-deutscher Wörterbücher als lexikalischE Ressourcen in Text+ (INSERT) LR Sorbisches Institut/Hauke Bartels
- Corpus glossariorum Latinorum Online Lemmatization (CGLO) LR Bayerische Akademie der Wissenschaften/ Adam Gitner

---

## FAQ
{{<accordion>}}
{{<accordion-item title="Ziel der Kooperationsprojekte">}}
**Was ist das Ziel der Kooperationsprojekte?**

Kooperationsprojekte im Rahmen von Text+ dienen dazu, das Angebotsportfolio von Text+ zu erweitern. Text+ ist offen für neue Angebote und Beteiligte und beschränkt sich nicht nur auf die Angebote der initialen Partner, sondern kann als Infrastruktur auch weitere Daten, Dienste und Services integrieren. Wer ein Kooperationsprojekt anbietet, kann auch Teil des Text+-Netzwerks werden, sich am weiteren Aufbau der Infrastruktur beteiligen und von den Entwicklungen profitieren.
{{</accordion-item>}}

{{<accordion-item title="Antragsberechtigungen">}}
**Wenn ein Vorschlag für ein Kooperationsprojekt in einem Jahr nicht gefördert wird, darf der Antrag in einer späteren Runde nochmals gestellt werden? Kann ein Antrag in einer der nächsten Runden auch eingereicht werden, falls zuvor keine Finanzierung möglich war?**

Auf jeden Fall. Im Rahmen der Begutachtung der Projekte wird eine Prioritätenliste der Vorschläge erstellt, die bis zur Ausschöpfung des jeweiligen Budgets einer Finanzierungsrunde bewilligt werden können. Das bedeutet also nicht notwendigerweise, dass ein Projekt, das in einem Jahr nicht gefördert wurde, nicht förderfähig wäre. Natürlich kann ein Antrag vor der Neueinreichung weiter gestärkt und auch durch weitere eigene Vorarbeiten aktualisiert werden. Im konkreten Fall sollte zuvor das Gespräch mit denjenigen gesucht werden, die die jeweilige Datendomäne koordinieren (vgl. hierzu unter dem Punkt „Antragstellung“ die Frage „Wer kann im Vorfeld der Antragstellung angesprochen werden, um die Vorgaben, Erwartungen etc. bei der Planung und Konzeption der Kooperationsprojekte auf die Anforderungen von Text+ abzustimmen?“).

**Wer kann einen Antrag stellen? Können Studierende/Promovierende/Initiativen einen Antrag stellen?**

Antragsberechtigt sind Abteilungen und Arbeitsgruppen, die nicht bereits im Rahmen von Text+ finanziell gefördert werden, die aber den DFG-FÖRDERRICHTLINIEN FÜR DIE NFDI (ABSCHNITT III (1) (https://www.dfg.de/formulare/nfdi100/nfdi100_en.pdf)) entsprechen. Einzelforschende und Verbünde von Forschenden (z. B. in Verbundprojekten) können z. B. über ihre Hochschule oder Forschungseinrichtung einen Antrag stellen, wenn sie zu den in den DFG-Richtlinien genannten Einrichtungen zählen. Viele Universitäten und Forschungseinrichtungen haben dazu eigene Abteilungen, die sich mit Forschungsförderung und Drittmittelprojekten beschäftigen. Forschende sollten mit der entsprechenden Abteilung ihrer Heimatinstitution Kontakt aufnehmen, um die Voraussetzungen einer Antragstellung durch die Einrichtung zu klären.

**Können auch Einzelpersonen Anträge stellen oder müssen es immer Teams sein? Können auch zwei Wissenschaftler/Wissenschaftlerinnen Anträge stellen?**

Es gibt keine Vorgaben zur Größe eines Teams, das ein Kooperationsprojekt durchführt. Antragsberechtigt sind aber nur Abteilungen und Arbeitsgruppen, die nicht bereits im Rahmen von Text+ finanziell gefördert werden, die aber den DFG-FÖRDERRICHTLINIEN FÜR DIE NFDI (ABSCHNITT III (1), siehe https://www.dfg.de/formulare/nfdi100/nfdi100_en.pdf ) entsprechen. Einzelforschende und Verbünde von Forschenden (z. B. in Verbundprojekten) können z. B. über ihre Hochschule oder Forschungseinrichtung einen Antrag stellen, wenn sie zu den in den DFG-Richtlinien genannten Einrichtungen zählen. Im konkreten Fall sollte zuvor das Gespräch mit denjenigen gesucht werden, die die jeweilige Datendomäne koordinieren (vgl. hierzu unter dem Punkt „Antragstellung“ die Frage „Wer kann im Vorfeld der Antragstellung angesprochen werden, um die Vorgaben, Erwartungen etc. bei der Planung und Konzeption der Kooperationsprojekte auf die Anforderungen von Text+ abzustimmen?“).

**Können sich auch Forschende aus anderen Ländern an einem Antrag beteiligen?**

In einigen Projekten haben Forschende aus dem Ausland einen wichtigen Beitrag zu leisten und sorgen für die fachliche Anbindung auch über die nationale Perspektive hinaus. Sie können ihre Beiträge auch im Rahmen von Unterstützungsschreiben ausdrücken. Zuwendungsempfänger sind an die Zuwendungsbestimmungen der DFG gebunden (siehe http://www.dfg.de/formulare/nfdi300/nfdi300_de.pdf), außerdem müssen die Zuwendungsempfänger antragsberechtigt sein (siehe DFG-Förderrichtlinien für die NFDI Abschnitt III (1) https://www.dfg.de/formulare/nfdi100/nfdi100_en.pdf).
{{</accordion-item>}}

{{<accordion-item title="Antragsstellung">}}
**Sind weitere Ausschreibungsrunden geplant?**

Ja, es sind weitere Ausschreibungsrunden in den Jahren 2024 (Mittelvergabe für 2025) und 2025 (Mittelvergabe für 2026) geplant.

**Welche Rolle haben Fachverbände bei der Antragstellung?**

Den Fachverbänden und -verbünden kommt eine wichtige Scharnierfunktion bei Kooperationsprojekten zu: Als Teil der Plenarversammlung sind sie am Aufbau und an der Zusammensetzung der Komitees beteiligt, die die Priorisierung der Anträge vornehmen und die Ausschreibung begleiten. Daneben können Verbände und Verbünde auch Projektanträge unterstützen, z. B. durch die Abgabe von eigenen Einschätzungen zu den Projektanträgen im Rahmen eines Unterstützungsbriefes.

**Wie umfangreich soll der Antrag sein?**

Im Antragstemplate sind Angaben zu Seitenzahlen einzelner Abschnitte des Antrags hinterlegt. Dieses Template dient dazu, im Begutachtungsprozess einen schnellen Überblick über die eingereichten Projekte zu schaffen. Außerdem sollen die Seitenzahlen auch als Hinweise zum Umfang der jeweiligen Abschnitte verstanden werden. Antragstellende sollten sich an den dort angegebenen Umfängen orientieren. Im konkreten Fall können Rückfragen mit denjenigen geklärt werden, die die jeweilige Datendomäne koordinieren (vgl. hierzu unter dem Punkt „Antragstellung“ die Frage „Wer kann im Vorfeld der Antragstellung angesprochen werden, um die Vorgaben, Erwartungen, etc. bei der Planung und Konzeption der Kooperationsprojekte auf die Anforderungen von Text+ abzustimmen?“).

**Wer kann im Vorfeld der Antragstellung angesprochen werden, um die Vorgaben, Erwartungen, etc. bei der Planung und Konzeption der Kooperationsprojekte auf die Anforderungen von Text+ abzustimmen?**
- Collections: Leitung: [Dr. Peter Leinen](mailto:p.leinen@dnb.de), Koordination: [Philippe Genêt](mailto:P.Genet@dnb.de)
- Lexikalische Ressourcen: Leitung: [PD Dr. Alexander Geyken](mailto:geyken@bbaw.de), Koordination: [Axel Herold](mailto:herold@bbaw.de)
- Editionen: Leitung: [Prof. Dr. Andreas Speer](mailto:andreas.speer@uni-koeln.de), Koordination: [Kilian Hensen](mailto:Kilian.Hensen@uni-koeln.de)
- Infrastruktur/Betrieb: Leitung: [Philipp Wieder](mailto:philipp.wieder@gwdg.de), Koordination: [Stefan Buddenbohm](mailto:stefan.buddenbohm@sub.uni-goettingen.de)
- Ebenfalls ansprechbar sind [Lukas Weimer](mailto:lukas.weimer@sub.uni-goettingen.de) sowie [Thorsten Trippel](mailto:trippel@ids-mannheim.de), die im Text+ Office mitarbeiten.

{{</accordion-item>}}

{{<accordion-item title="Begutachtungs- und Bewertungsprozess">}}
**Werden die weiteren Bewerbungsrunden ähnlich ausfallen wie die in 2022 oder ist geplant, die Bewerbungskriterien bzw. Kriterien für potenzielle Projekte dann möglicherweise spezifischer, enger oder anders zu fassen?**

Nach den Erfahrungen der ersten Bewerbungsrunde werden die Kriterien und deren Passgenauigkeit überprüft und möglicherweise für nachfolgende Runden überarbeitet werden.
{{</accordion-item>}}

{{<accordion-item title="Daten">}}
**Wie umfangreich sollen die Daten sein, die im Rahmen eines Kooperationsprojekts eingebracht werden können?**

Es gibt keine abgeschlossene Liste von empfohlenen und akzeptablen Datenformaten. Es wird empfohlen, sich an Formaten zu orientieren, die bereits von Partnern eingesetzt (und „für gut befunden“) wurden. Auch gibt es Standard-Formate, etwa der Text Encoding Initiative (TEI), des W3C und der ISO. Eine Faustregel ist, dass ein Format dann leichter zu integrieren ist, je näher es sich am Standard orientiert und es sich leicht in bereits innerhalb von Text+ verwendeten Formate konvertieren lässt. Im konkreten Fall sollte zuvor das Gespräch mit denjenigen gesucht werden, die die jeweilige Datendomäne koordinieren (vgl. hierzu unter dem Punkt „Antragstellung“ die Frage „Wer kann im Vorfeld der Antragstellung angesprochen werden, um die Vorgaben, Erwartungen, etc. bei der Planung und Konzeption der Kooperationsprojekte auf die Anforderungen von Text+ abzustimmen?“).

**Welche Datenformate werden für Daten in Kooperationsprojekten von Text+-Partnern angenommen?**

Es gibt keine abgeschlossene Liste von empfohlenen und akzeptablen Datenformaten. Es wird empfohlen, sich an Formaten zu orientieren, die bereits von Partnern eingesetzt (und „für gut befunden“) wurden. Auch gibt es Standard-Formate, etwa der Text Encoding Initiative (TEI), des W3C und der ISO. Eine Faustregel ist, dass ein Format dann leichter zu integrieren ist, je näher es sich am Standard orientiert und es sich leicht in bereits innerhalb von Text+ verwendeten Formate konvertieren lässt. Im konkreten Fall sollte zuvor das Gespräch mit denjenigen gesucht werden, die die jeweilige Datendomäne koordinieren (vgl. hierzu unter dem Punkt „Antragstellung“ die Frage „Wer kann im Vorfeld der Antragstellung angesprochen werden, um die Vorgaben, Erwartungen, etc. bei der Planung und Konzeption der Kooperationsprojekte auf die Anforderungen von Text+ abzustimmen?“).

**Was bedeutet „Integration von Daten und Diensten in Text+“?**

Unter Integration werden die Maßnahmen verstanden, über die Daten und Dienste langfristig für Nutzende der Text+-Infrastruktur zugänglich gemacht werden. Dies kann zum Beispiel dadurch erfolgen, dass Daten von anderen Text+-Partnern gehostet werden. Eine andere Möglichkeit besteht darin, dass ein eigenes, nachhaltiges (zertifiziertes) Repositorium betrieben und über Schnittstellen an Text+ angebunden wird. Die Metadaten werden jeweils über entsprechende Schnittstellen bereitgestellt. Zur Integration gehört auch, dass keine Nachdigitalisierung nötig ist.

**Wenn interessante Datensammlungen entdeckt werden, wie können sie über Text+ zugänglich gemacht werden? Kann man Text+ vorschlagen, sich um die Aufbereitung und Integration von Daten zu kümmern?**

Text+ legt den Schwerpunkt auf Forschungsdaten, die in digitalen Formaten vorliegen. Eine Digitalisierung durch Text+ ist derzeit nicht Teil des Angebotsportfolios. Wenn Ressourcentypen beigetragen werden sollen, die nicht im Schwerpunkt eines Daten- und Kompetenzzentrums von Text+ liegen, besteht eine Integrationsmöglichkeit darin, ein eigenes Repositorium für diese Datentypen aufzubauen. Eine Übernahme von Angeboten erfordert auf jeden Fall die Abklärung von Rechten und langfristigen Verantwortungen.
{{</accordion-item>}}

{{<accordion-item title="Formalia">}}
**Was konkret beinhaltet der Abschluss eines Kooperationsvertrages mit dem Leibniz-Institut für Deutsche Sprache?**

Kooperationsprojekte im Rahmen von Text+ beruhen auf den Verwendungsrichtlinien der DFG für NFDI-Konsortien (siehe http://www.dfg.de/formulare/nfdi300/nfdi300_de.pdf). Die Partner, die Kooperationsprojekte durchführen, werden Teil des Text+-Konsortiums. Die Zusammenarbeit und Mittelweiterleitung ist in Text+ durch einen Mittelweiterleitungs- und Kooperationsvertrag geregelt, der zwischen dem Leibniz-Institut für Deutsche Sprache und den jeweiligen Partnern inhaltsgleich bilateral geschlossen wird. Als Kontaktmöglichkeit für Fragen zum Vertrag steht das Text-Plus-Office (office@text-plus.org) zur Verfügung.

**Welcher Text+-Domäne (Collections, lexikalische Ressourcen, Editionen, Infrastructure/Operations) wird ein Projektvorschlag zugeordnet?**

Die Struktur von Text+ richtet sich nach den Datendomänen Collections, lexikalische Ressourcen und Editionen. Daneben gibt es übergreifend den Bereich „Infrastructure/Operations“.
{{</accordion-item>}}

{{<accordion-item title="Mittel">}}
**Welche Kostenarten können beantragt werden?**

Gemäß den Förderrichtlinien der DFG können Personalkosten und Sachmittel (Reisemittel) beantragt werden. In der Summe sollten sie den Betrag von 60.000,- Euro nicht überschreiten. Investitionsgüter (Server, Rechnerausstattung, Arbeitsplatz, Geräte,…) sind nicht förderfähig.
{{</accordion-item>}}



{{</accordion>}}
