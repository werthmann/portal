---
title: Nationale Akademie der Wissenschaften Leopoldina (Leopoldina)
short_name: Leopoldina
text_plus_domains:
- Editionen
text_plus_clusters:
  Editionen:
  - Early Modern, Modern, and Contemporary Texts

external_url: 'https://www.leopoldina.org/en/leopoldina-home/'

type: competence-center
---

# [Nationale Akademie der Wissenschaften Leopoldina (Leopoldina)](https://www.leopoldina.org/en/leopoldina-home/)

Datenkompetenzzentrum in folgenden Clustern der Datendomäne Editionen: Early Modern, Modern, and Contemporary Texts

Die 1652 gegründete Deutsche Akademie der Naturforscher Leopoldina ist mit ihren rund 1.600 Mitgliedern aus nahezu allen Wissenschaftsbereichen eine klassische Gelehrtengemeinschaft. Sie wurde 2008 zur Nationalen Akademie der Wissenschaften Deutschlands ernannt. In dieser Funktion hat sie zwei besondere Aufgaben: die Vertretung der deutschen Wissenschaft im Ausland sowie die Beratung von Politik und Öffentlichkeit. Sie unterstützt diesen Prozess mit einer kontinuierlichen Reflexion über Voraussetzungen, Normen und Folgen wissenschaftlichen Handelns. 

Das Leopoldina-Zentrum für Wissenschaftsforschung (ZfW) koordiniert seit 2012 diesen Reflexionsprozess und verantwortet wissenschaftshistorische, wissenschaftstheoretische und wissenschaftsphilosophische Projekte. Schwerpunkte der Arbeiten des Zentrums liegen dabei zum einen im Bereich historischer (spezifisch wissenschafts- und wissenshistorischer) Forschung, zum anderen im Bereich der _science studies_, der reflektierten wissenschaftlichen Beschäftigung mit Institutionen, Formen und Inhalten der Wissenschaft an sich. 
Neben der Bereitstellung der dafür notwendigen Infrastruktur, initiiert das Zentrum selbst reflektierende Forschung zu Wissenschaft im Allgemeinen. Hierzu gehören neben Drittmittelprojekten aus dem Bereich der Digital Humanities auch die Planung und Durchführung von Veranstaltungen, die die digitale Transformation der Wissenschaft zum Gegenstand haben. In bereits abgeschlossenen Projekten aus dem Bereich der digitalen Edition sowie Sammlung, die historische, wissenschaftliche Texte zum Gegenstand haben, konnten Erfahrungen in Planung, Management und nachhaltiger Verfügbarmachung von Textdaten gesammelt werden. Diese Projekte sind:

- das Leopoldina-Projekt Goethe. Die Schriften zur Naturwissenschaft mit vollständigen Online-Indexen für die Printausgabe (gefördert durch die Fritz Thyssen Stiftung)

- das Projekt Objektsprache und Ästhetik: Das Beispiel Konchylien mit einer digitalen Manuskriptausgabe, erschienen 2021 (Bundesministerium für Bildung und Forschung, BMBF)

Aktuell wird am ZfW das Langzeitprojekt _Ernst-Haeckel-Briefedition_ betreut, dass eine vollständige Online-Ausgabe der Korrespondenz sowie kommentierte Druckausgabe ausgewählter Briefe im Rahmen des Akademienprogramms der Akademienunion erarbeitet. 

Das ZfW steht für eine stärkere Vernetzung in die Digital Humanities-Community mit der Wissenschaftsforschung und wird sich durch Veranstaltungen zu reflektierenden Themen aus dem Bereich der (digitalen) Editorik sowie der Bedeutung und dem Umgang mit Forschungsdaten engagieren. Über das ZfW bringt sich die Leopoldina innerhalb von Text+ besonders in den Aufbau eines Kompetenzzentrums für Digitale Editionen ein und engagiert sich im Bereich der Community Services. 
