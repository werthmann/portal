---
title: Akademie der Wissenschaften in Hamburg, AdWHH, Zentrum für Interdisziplinarität und linguistische Diversität in Sprachdaten
short_name: AdWHH
text_plus_domains:
- Collections
text_plus_clusters:
  Collections:
  - Contemporary Language
  - Historical Texts

external_url: "https://www.awhamburg.de"

type: competence-center
---

# [Akademie der Wissenschaften in Hamburg (AdWHH)](https://www.awhamburg.de/), Zentrum für Interdisziplinarität und linguistische Diversität in Sprachdaten

Datenzentrum in folgenden Clustern der Datendomäne Collections: Contemporary Language; Historical Texts

Seit ihrer Gründung im Jahr 2004 fördert die AdWHH die interdisziplinäre Forschung zu gesellschaftlich bedeutsamen Zukunftsfragen und grundlegenden wissenschaftlichen Problemen. Darüber hinaus koordiniert die AdWHH derzeit fünf langfristig angelegte Forschungsprojekte im Rahmen des Akademienprogramms (das wiederum von der [Union der deutschen Akademien der Wissenschaften](https://www.akademienunion.de/) koordiniert wird), die jeweils einen starken Fokus auf die digitale Erschließung und Analyse einzigartigen und vielfältigen Sprachmaterials legen. Als prominentes Beispiel ist das Projekt [DGS-Korpus](http://www.dgs-korpus.de/) zu nennen, das die umfassende Sammlung von Gebärdensprachdaten und deren Zusammenstellung in Form des Öffentlichen DGS-Korpus zum Ziel hat.

Um eine solide Grundlage für die langfristige Verfügbarkeit vielfältiger sprachlicher Ressourcen für weltweite Forschungsgemeinschaften und die interessierte Öffentlichkeit zu schaffen, bereitet die AdWHH derzeit eine gemeinsame Initiative mit dem [Zentrum für nachhaltiges Forschungsdatenmanagement](https://www.fdm.uni-hamburg.de/) (FDM) vor.

Als zentrale Betriebseinheit an der Universität Hamburg stellt das FDM unter anderem eine lokale technische Infrastruktur (einschließlich eines Datenrepositoriums) für nachhaltiges Forschungsdatenmanagement zur Verfügung.

Folgende Expertisen/Ressourcen sollen mit der Text+ Infrastruktur zur Verfügung gestellt werden (mit den Leitern des [HH Langzeitvorhaben](https://www.awhamburg.de/forschung/langzeitvorhaben.html) zu spezifizieren/diskutieren):

- [Beta maṣāḥǝft](http://www.betamasaheft.uni-hamburg.de/)
  Eine systematische Studie der christlichen Manuskripttradition Äthiopiens und Eritreas.
- [DGS-Korpus](http://www.dgs-korpus.de/)
  Erfasst und dokumentiert systematisch die Deutsche Gebärdensprache (DGS) in ihrer ganzen Vielfalt und erstellt auf der Grundlage der Korpusdaten ein elektronisches Wörterbuch.
- [Etymologika](https://www.etymologika.uni-hamburg.de/)
  Kritische Ausgabe, Übersetzung und Kommentierung der griechischen Enzyklopädie „Etymologicum Gudianum“. Erforschung der reichen Manuskriptproduktion griechisch-byzantinischer etymologischer Enzyklopädien und Präsentation der Ergebnisse in einer gedruckten und umfangreichen digitalen Version.
- [INEL Corpus](https://inel.corpora.uni-hamburg.de/)
  Indigene nordeurasische Sprachen (INEL): Bereitstellung von Sprachressourcen für indigene Sprachen und Schaffung einer digitalen Forschungsinfrastruktur für die Nutzung dieser Ressourcen. Ausführlich kommentierte, beschönigte und zum größten Teil audio-alignierte Korpora der Sprachen Dolgan, Kamas und Selkup. Während der vorgesehenen Förderperiode werden Korpora weiterer Sprachen (z.B. Evenkisch, Nenzisch, etc.) folgen.
- [Formulae – Litterae – Chartae](https://www.formulae.uni-hamburg.de/)
  Erforschung und kritische Edition der frühmittelalterlichen Formulae sowie Zugriffsmöglichkeiten zu diesen über eine digitale Forschungsinfrastruktur, die eine Erforschung der Formulae-Schrift in Westeuropa vor der Entwicklung der ars dictaminis auf der Grundlage von Briefen und Urkunden ermöglicht.
