---
title: Akademie der Wissenschaften und der Literatur | Mainz (AdWL)
short_name: AdWL Mainz
text_plus_domains:
- Editionen
text_plus_clusters:
  Editionen:
  - Ancient and Medieval Texts

external_url: "http://www.adwmainz.de/"

type: competence-center
---

# [Akademie der Wissenschaften und der Literatur, Mainz (AdWMZ)](http://www.adwmainz.de/)

Datenzentrum in folgenden Clustern der Datendomäne Editionen: Ancient and Medieval Texts

Mit einem starken Fokus auf digitale Methoden und Infrastrukturen in den Geistes- und Kulturwissenschaften verbindet die AdWL Mainz langfristige Grundlagenforschung in Sprache und Literatur mit Forschung in Musikwissenschaft, Kunstgeschichte und Archäologie.

Die [Digitale Akademie (DA)](http://www.adwmainz.de/en/digitalitaet/digitale-akademie.html), die Forschungsabteilung Digital Humanities (DH) der AdWL Mainz, ist an einem breiten Spektrum digitaler Editionen beteiligt, die sich mit Textquellen und -materialien von der Antike bis zur Avantgarde beschäftigen. Die Forschungsaktivitäten der DA konzentrieren sich auf die Datenmodellierung und die Erstellung von Webportalen, nachhaltiges Research Software Engineering, aktuelle Webtechnologien sowie die Anwendung von Linked Open Data (LOD) und Graphentechnologien zur Erschließung neuer Analyse- und Nachnutzungsszenarien in den Text- und Kulturwissenschaften. Die AdWL Mainz ist die Institution, an der [NFDI4Culture](https://nfdi4culture.de/) angesiedelt ist, und fungiert als Brücke zwischen Forschungsgemeinschaften, die sich mit text- und objektbezogenen Editionen und Datenpublikationen befassen. Zudem ist die AdWL Mainz eine der mitbegründenden Institutionen des DH-Masterstudiengangs in Mainz und trägt mit einer DH-Professur, regelmäßigen DH-Vorlesungen und einer internationalen DH-Sommerschule zur Aus- und Weiterbildung des wissenschaftlichen Nachwuchses bei.

Darüber hinaus trägt die AdWL Mainz mit zahlreichen [digitalen Editionen](https://www.text-plus.org/en/research-data/editions-list-en/), Textsammlungen und Softwareanwendungen zu Text+ bei. Thematisch reichen diese von großen mittelalterlichen und frühneuzeitlichen Textkorpora wie der Regesta Imperii, den Augsburger Baumeisterbüchern und den Deutschen Inschriften bis hin zu Editionen mit Schwerpunkt auf Quellen des 19. und 20. Jahrhunderts wie [DER STURM](https://sturm-edition.de/) und der [Hans Kelsen Werke](https://kelsen.online/). Darüber hinaus hostet und kuratiert die AdWL Mainz übergreifende Forschungsinformationssysteme wie [AGATE](https://agate.academy/) (ein europäisches Portal für die Akademien der Wissenschaften) oder das [Portal Kleine Fächer](https://www.kleinefaecher.de/) (zusammen mit der Johannes Gutenberg-Universität). Technisch gesehen wird die AdWL Mainz Text+ Lösungen für die Erstellung integrierter Editions- und Webportale, Annotationssoftware für graphbasierte digitale Editionen und LOD-Anwendungen zur Verfügung stellen, die eine semantische Anreicherung und Verknüpfung digitaler wissenschaftlicher Editionen ermöglichen.

Werkzeuge und Ressourcen (Auswahl):

Software:
- [Cultural Heritage Framework](https://digicademy.github.io/2017-editionsportale-jena/)
- [SPEEDy](https://digicademy.github.io/2019-06-04_GraphsDE_Lausanne_SPEEDy)
- [XTriples](https://xtriples.lod.academy/)
- [STURM Digital Edition Environment](https://github.com/digicademy/sturm-exist-app)
- [DLight Microframework](https://github.com/digicademy/dlight)
- Epigraf (Virtual Research Environment for Inscriptions)

Ressourcen (Digitale Editionen und Portale):
- [Augsburger Baumeisterbücher](https://www.augsburger-baumeisterbuecher.de/) (mit JGU)
- [Das Buch der Briefe der Hildegard von Bingen](https://liberepistolarum.mni.thm.de/home) (mit TUDa und JGU)
- Die [Deutschen Inschriften](http://www.inschriften.net/) (mit NAdWG, BAdW, HAW, NRWAW, SAW)
- [DER STURM](https://sturm-edition.de/)
- [Die Sozinianischen Briefwechsel](https://sozinianer.mni.thm.de/home) (mit JALB)
- [Hans Kelsen Werke](https://kelsen.online/)
- [Leichenpredigten der Frühen Neuzeit](http://www.personalschriften.de/leichenpredigten/digitale-editionen.html)
- [Quellensammlung zur Geschichte der Deutschen Sozialpolitik](https://quellen-sozialpolitik-kaiserreich.de/)
- [Regesta Imperii](http://www.regesta-imperii.de/)
