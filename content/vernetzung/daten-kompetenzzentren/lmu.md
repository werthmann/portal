---
title: Ludwig-Maximilians-Universität (LMU) München, Bayerisches Archiv für Sprachsignale, BAS
short_name: LMU
text_plus_domains:
- Collections
text_plus_clusters:
  Collections:
  - Contemporary Language

external_url: 'https://www.uni-muenchen.de/index.html'

type: competence-center
---

# [Ludwig-Maximilians-Universität (LMU) München](https://www.uni-muenchen.de/index.html), [Bayerisches Archiv für Sprachsignale, BAS](https://www.phonetik.uni-muenchen.de/Bas/BasHomedeu.html)

Datenzentrum in folgenden Clustern der Datendomäne Collections: Contemporary Language

Sitz des BAS ist das Institut für Phonetik und Sprachverarbeitung der LMU München. Es wurde 1995 mit dem Ziel gegründet, den Zugang zu Sprachdaten und Sprachverarbeitungsdiensten sowohl für die Sprachtechnologieentwicklung als auch für die Forschung zu ermöglichen. Seitdem hat es sich zu einem Forschungszentrum für Sprachsammlungen und die entsprechende Forschungsinfrastruktur entwickelt.

Das BAS verfügt über eine eigene technische Infrastruktur innerhalb des Instituts. Es unterhält enge Beziehungen zum [Linguistic Data Consortium (LDC)](https://www.ldc.upenn.edu/), das an der University of Pennsylvania angesiedelt ist, und zur [European Language Resources Association (ELRA)](http://www.elra.info/en/). Seit 2010 ist es Mitglied von [CLARIN-D](https://www.clarin-d.net/de/), wo es auf dem Wissensgebiet der zeitgenössischen Sprachdaten tätig ist. Darüber hinaus ist das BAS ein [CoreTrustSeal](https://www.coretrustseal.org/)-zertifiziertes CLARIN-B-Zentrum, das aktiv Dienstleistungen mit dem Schwerpunkt Sprache in Forschungsinfrastrukturen anbietet.

Die vom BAS bereitgestellten Ressourcen lassen sich in drei Hauptkategorien einteilen:

- ein Repository für Sprachdatenbanken.
- eine Reihe von webbasierten Diensten zur Sprachverarbeitung
- verschiedene eigenständige Tools zur Datensammlung und -analyse.

Das Repository des BAS enthält derzeit mehr als 40 Sammlungen von Sprachdaten in mehreren Sprachen (Deutsch, Englisch, Japanisch, Italienisch usw.). Diese Sammlungen wurden entweder intern oder durch industrielle oder akademische Projekte erstellt, z.B. Verbmobil, SmartKom. In den letzten Jahren wurde eine Reihe von Ressourcen, die von Dritten erstellt wurden, dem Repositorium hinzugefügt, z.B. das [Gesprochene Wortkorpus für Untersuchungen zur auditiven Verarbeitung von Sprache und emotionaler Prosodie (WaSeP)](http://hdl.handle.net/11022/1009-0000-0007-3D30-F) und das [Karl-Eberhard-Korpus](http://hdl.handle.net/11022/1009-0000-0007-DADB-D) aus Tübingen. Die vom BAS bereitgestellten Ressourcen sind einzigartig und wichtig für jede Forschung zur gesprochenen Sprache im In- und Ausland.

Der bekannteste Webdienst des BAS ist zweifelsohne [WebMAUS](https://clarin.phonetik.uni-muenchen.de/BASWebServices/interface), ein mehrsprachiger Aligner von Text und Sprache. Zu den weiteren Diensten gehören die Graphem-Phonem-Konvertierung, Aussprachewörterbücher, Audio-Anreicherung und Pipeline-Dienste, die vordefinierte Verarbeitungsketten für Sprachdaten bereitstellen. Zu den vom BAS entwickelten Werkzeugen gehören [SpeechRecorder](https://www.bas.uni-muenchen.de/Bas/software/speechrecorder/) für skriptgesteuerte Audioaufnahmen und das [EMU Speech Database Management System](http://ips-lmu.github.io/EMU.html).
