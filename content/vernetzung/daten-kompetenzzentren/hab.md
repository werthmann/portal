---
title: Herzog August Bibliothek Wolfenbüttel (HAB)
short_name: HAB
text_plus_domains:
- Editionen
text_plus_clusters:
  Editionen:
  - Ancient and Medieval Texts
  - Early Modern, Modern, and Contemporary Texts

external_url: "http://www.hab.de/"

type: competence-center
---

# [Herzog August Bibliothek Wolfenbüttel (HAB)](http://www.hab.de/)

Datenkompetenzzentrum in folgenden Clustern der Datendomäne Editionen: Ancient and Medieval Texts und Early Modern, Modern, and Contemporary Texts

Die HAB ist eine außeruniversitäre Studien- und Forschungseinrichtung für die europäische Kulturgeschichte des Mittelalters und der frühen Neuzeit. Als Bibliothek verwahrt sie die seit dem 16. Jahrhundert von den Herzögen aus der Wolfenbütteler Linie des Hauses Braunschweig-Lüneburg zusammengetragenen Sammlungen. Sie umfassen etwa 11.800 Handschriften, davon 2700 aus dem Mittelalter, etwa 400.000 vor 1830 gedruckte Bücher, mehr als 20.000 Druckgrafiken und andere Spezialsammlungen. Die HAB ist ein von der [Deutschen Forschungsgemeinschaft (DFG)](https://www.dfg.de/) gefördertes Zentrum für Handschriftenkatalogisierung und ist Teil des Projekts für eine retrospektive Deutsche Nationalbibliothek (AG Sammlung Deutscher Drucke), in welchem sie das 17. Jahrhundert abdeckt. Die HAB digitalisiert seit vielen Jahren ihre Bestände in großem Umfang. Sie engagiert sich insbesondere für die Digitalisierung mittelalterlicher Handschriften, für die DFG-geförderte Massendigitalisierung gedruckter Bücher und für die Digitalisierung von Grafiken ([Virtuelles Kupferstichkabinett](http://www.virtuelles-kupferstichkabinett.de/)). Die HAB ist Teil wichtiger Informationsinfrastrukturprojekte in Deutschland. Zusammen mit der Universitätsbibliothek Leipzig betreibt sie den [Fachinformationsdienst Buch-, Bibliotheks- und Informationswissenschaft (FID BBI)](https://katalog.fid-bbi.de/Content/about). Außerdem ist sie Partner in der von der DFG eingerichteten koordinierten [Förderinitiative zur Weiterentwicklung von Verfahren der Optical Character Recognition (OCR-D)](https://ocr-d.de/).

Die Bibliotheksbestände ermöglichen jede Art von Forschung zur europäischen Kulturgeschichte des Mittelalters und der frühen Neuzeit. Die HAB unterstützt und organisiert diese Forschung in vielfältiger Weise: durch aktive Förderung des Austauschs und der Vernetzung internationaler Wissenschaftlerinnen und Wissenschaftler, durch Schaffung der bestmögliche Bedingungen und Unterstützung ihrer Forschung, durch Stipendien für (Post-)Doktorandinnen und Doktoranden, durch Lehrtätigkeit an Universitäten und durch eigene Forschungsprojekte auf innovativen Gebieten der Mittelalter- und Frühneuzeitforschung, die vielfach auf Drittmittel angewiesen sind. Forschungsschwerpunkte sind kulturelle Übersetzung, Wissenskulturen, Religions- und Frömmigkeitsgeschichte und Bildpolitik.

Die Forschung an der HAB ist eng mit den Entwicklungen in den Digital Humanities verbunden. Die HAB verfügt über langjährige Erfahrung in digitalem Edieren. Sie stellt eine selbst entwickelte Editionsinfrastruktur in Form der [Wolfenbütteler Digitalen Bibliothek (WDB)](http://www.hab.de/en/home/library/wolfenbuettel-digital-library.html) bereit, die für zahlreiche Projekte genutzt wird. Herausragende Beispiele sind die von der DFG geförderten Langzeitprojekte zur [Edition der Werke und Briefe des Reformators Andreas Bodenstein von Karlstadt](https://karlstadt-edition.org/), der [Tagebücher von Herzog Christian II. von Anhalt-Bernburg](http://www.tagebuch-christian-ii-anhalt.de/) und der [Reise- und Sammlungsberichte des Kunstunternehmers Philipp Hainhofer](https://hainhofer.hab.de/).

Die HAB ist auch Teil des [Forschungsverbunds Marbach Weimar Wolfenbüttel](https://www.mww-forschung.de/), der die literarische Überlieferung durch gemeinsame Forschungsprojekte und die Entwicklung eines virtuellen Forschungsraums zur Erschließung und Auswertung der digitalen Sammlungen der HAB, des [Deutschen Literaturarchivs Marbach](https://www.dla-marbach.de/) und der [Klassik Stiftung Weimar](https://www.klassik-stiftung.de/) erforscht. Mit der Beteiligung an der [Nationalen Forschungsdateninfrastruktur (NFDI)](https://www.dfg.de/en/research_funding/programmes/nfdi/index.html) will die HAB ihre eigene Expertise in den Digitalen Geisteswissenschaften, insbesondere in digitalen Editionen, sichtbarer machen und Teil einer persistenten digitalen Infrastruktur werden, die ihren Wissenschaftlerinnen und Wissenschaftlern wichtige forschungsnahe Dienste anbietet.
