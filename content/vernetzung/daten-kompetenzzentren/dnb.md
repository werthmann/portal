---
title: Deutsche Nationalbibliothek (DNB)
short_name: DNB
text_plus_domains:
- Collections
- Infrastruktur/Betrieb
text_plus_clusters:
  Collections:
  - Unstructured Text (Coord.)

external_url: "https://www.dnb.de/"

type: competence-center
---

# [Deutsche Nationalbibliothek (DNB)](https://www.dnb.de/)

Die DNB ist die zentrale Archivbibliothek Deutschlands. Sie sammelt, dokumentiert und archiviert alle seit 1913 in Deutschland erschienenen Publikationen und Tonträger sowie Werke, die in deutscher Sprache erstellt wurden oder einen Bezug zu Deutschland haben. Entsprechend ihrem gesetzlichen Auftrag baut die DNB eine große, ständig wachsende digitale Sammlung auf und wird diese unter Beachtung der rechtlichen Rahmenbedingungen in Text+ integrieren. Diese Sammlung ist bereits in sich inhomogen und reicht von zeitgenössischer deutschsprachiger Literatur über alle Tageszeitungen, wissenschaftliche Artikel aus deutschen Verlagen bis hin zu Kioskliteratur. Sie umfasst auch eine Reihe von Sondersammlungen, wie z.B. das Archiv und die Bibliothek des [Börsenvereins des Deutschen Buchhandels e.V.](https://www.boersenverein.de/boersenverein/) oder die Sammlung des Deutschen Exilarchivs 1933-1945 mit Exilpresse digital. Die DNB erleichtert Forschungsprojekte in den verschiedensten Disziplinen, indem sie die digitale Sammlung von Texten des 21. Jahrhunderts so flexibel wie möglich bereitstellt und Projekte zur Korpusbildung unterstützt.

## Collections

Datenzentrum in folgenden Clustern der Datendomäne Collections: Unstructured Text (Coord.)

Der Zugang zu den meisten Objekten in den Beständen der DNB ist aus urheberrechtlichen Gründen beschränkt. Abseits der Nutzung von Volltexten müssen rechtskonforme, flexiblere Zugangsmöglichkeiten entwickelt werden. Zusammen mit dem [Scientific Coordination Committee](../../ueber-uns/textplus-governance/) wird sich die DNB an der Entwicklung eines Satzes abgeleiteter Textformate, wie N-Gramme und andere, beteiligen.

Die DNB wird eine aktive Rolle bei der Weiterentwicklung von Techniken zur Verknüpfung von Sammlungen mit anderen lokal und thematisch getrennten Datensätzen aus Text+ über Linked Open Data (LOD) und insbesondere über Normdateien wie die Gemeinsame Normdatei (GND) oder über lexikalische Ressourcen spielen. Sie wird die GND auch im Hinblick auf die Bedürfnisse der wissenschaftlichen Gemeinschaften weiterentwickeln. Zusammen mit dem [Leibniz-Institut für Deutsche Sprache (IDS)](https://www.ids-mannheim.de/) wird die DNB ein zentraler Anlaufpunkt für die Vielzahl rechtlicher Themen sein, die sich aus der Nutzung und Veröffentlichung textbasierter Daten ergeben.

## Infrastruktur/Betrieb

Die DNB wird eine aktive Rolle bei der Weiterentwicklung von Techniken zur Verknüpfung von Sammlungen mit anderen lokal und thematisch getrennten Datensätzen aus Text+ über Linked Open Data (LOD) und insbesondere über Normdateien wie die Gemeinsame Normdatei (GND) oder über lexikalische Ressourcen spielen. Sie wird die GND auch im Hinblick auf die Bedürfnisse der wissenschaftlichen Gemeinschaften weiterentwickeln.
