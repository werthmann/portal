---
title: Universität Trier (UniTR), Trier Center for Digital Humanities (TCDH)
short_name: UniTR
text_plus_domains:
- Lexikalische Ressourcen
text_plus_clusters:
  Lexikalische Ressourcen:
  - German Dictionaries in a European Context

external_url: 'https://www.uni-trier.de/'

type: competence-center
---

# [Universität Trier (UniTR)](https://www.uni-trier.de/), [Trier Center for Digital Humanities (TCDH)](https://kompetenzzentrum.uni-trier.de/de/)

Datenzentrum in folgenden Clustern der Datendomäne Lexikalische Ressourcen: German Dictionaries in a European Context

Das TCDH verfügt über eine mehr als zwanzigjährige Erfahrung in der Planung, Koordination und Durchführung von Projekten in den Bereichen Volltextdigitalisierung, standardisierte Datenkodierung und digitale Publikation von Lexika und Referenzwerken. Ein besonderer Schwerpunkt liegt auf der Modellierung, Erschließung und Bereitstellung wichtiger historischer Wörterbücher. Zahlreiche Projekte am TCDH haben digitale Datenbestände u.a. für die Erstausgabe und Überarbeitung des [Deutschen Wörterbuchs von Jacob und Wilhelm Grimm](https://dwb.uni-trier.de/de/), die Mittelhochdeutschen Zentralwörterbücher, das [Althochdeutsche Wörterbuch](https://kompetenzzentrum.uni-trier.de/de/projekte/projekte/althochdeutsche-woeterbuch/), die Wörterbücher der westmitteldeutschen Regionalsprachen und das [Goethe-Wörterbuch](https://gwb.uni-trier.de/de/) erstellt. Die metasprachliche, [TEI-konforme](https://tei-c.org/) Kodierung der Daten ermöglicht eine hochspezifische, sogar schlüsselwortunabhängige Recherche. Sie sind im Rahmen der Plattform des Trierer Wörterbuchnetzes ([www.woerterbuchnetz.de](https://www.woerterbuchnetz.de/)) über offene Anwendungsschnittstellen miteinander verknüpft. Das TCDH hat auf dem Gebiet der digitalen Lexikographie ein dichtes nationales und internationales Netzwerk aufgebaut und kooperiert mit allen deutschen Akademien der Wissenschaften. Insbesondere ist das TCDH der einzige deutsche Partner im europäischen Verbundprojekt [ELEXIS](https://elex.is/) (European Lexographic Infrastructure), in dessen Rahmen ein offener, auf Standards basierender Rahmen für die Online-Publikation von Wörterbüchern und Nachschlagewerken entwickelt wird.
