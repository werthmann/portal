---
title: Technische Universität Dresden, Zentrum für Informationsdienste und Hochleistungsrechnen (TUDD)
short_name: TUDD
text_plus_domains:
- Infrastruktur/Betrieb

external_url: 'https://tu-dresden.de/zih/hochleistungsrechnen'

type: competence-center
---

# [Technische Universität Dresden, Zentrum für Informationsdienste und Hochleistungsrechnen (TUDD)](https://tu-dresden.de/zih/hochleistungsrechnen)

Das Zentrum für Informationsdienste und Hochleistungsrechnen stellt Fachwissen und Ressourcen im Aufgabenbereich Infrastruktur/Betrieb von Text+ zur Verfügung. In diesem Rahmen bietet es Zugang zur Datenanalyse-Infrastruktur des HRSK-II/HPC-DA.
