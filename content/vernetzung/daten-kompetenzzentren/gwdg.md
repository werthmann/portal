---
title: Gesellschaft für wissenschaftliche Datenverarbeitung mbH Göttingen (GWDG)
short_name: GWDG
text_plus_domains:
- Infrastruktur/Betrieb

external_url: "https://www.gwdg.de/"

type: competence-center
---

# [Gesellschaft für wissenschaftliche Datenverarbeitung mbH Göttingen (GWDG)](https://www.gwdg.de/)

Die GWDG ist das Daten- und IT-Dienstleistungszentrum für die [Universität Göttingen](https://www.uni-goettingen.de/en/1.html) und die [Max-Planck-Gesellschaft](https://www.mpg.de/) und bietet eine breite Palette hochverfügbarer Dienste für Lehre und Forschung. Darüber hinaus ist die GWDG maßgeblich an geisteswissenschaftlichen Forschungsinfrastrukturprojekten wie [DARIAH-DE](https://de.dariah.eu/), [CLARIAH-DE](https://clariah.de/) und der [European Open Science Cloud (EOSC)](https://www.eosc-portal.eu/) beteiligt. In DARIAH-DE ist die GWDG als technischer Koordinator nicht nur für die deutsche Nutzendengemeinschaft, sondern auch für die europäischen Nutzenden zuständig. Dazu gehört die Bereitstellung von Diensten wie Authentifizierungs- und Autorisierungsinfrastruktur (AAI) und Persistent Identifiers (PID). Darüber hinaus bietet die GWDG den Forschenden am [Göttinger Campus](https://goettingen-campus.de/de/) Beratung an, zum Beispiel im Hinblick auf Informationssicherheit oder Forschungsdatenmanagement.
