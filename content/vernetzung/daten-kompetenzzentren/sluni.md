---
title: Universität des Saarlandes, SLUni, Fachrichtung Sprachwissenschaft und Sprachtechnologie
short_name: SLUni
text_plus_domains:
- Collections
text_plus_clusters:
  Collections:
  - Contemporary Language
  - Historical Texts

external_url: 'https://www.uni-saarland.de/fachrichtung/lst.html'

type: competence-center
---

# [Universität des Saarlandes (SLUni)](https://www.uni-saarland.de/), [Fachrichtung Sprachwissenschaft und Sprachtechnologie](https://www.uni-saarland.de/fachrichtung/lst.html)

Datenzentrum in folgenden Clustern der Datendomäne Collections: Contemporary Language; Historical Texts

Als Kompetenzzentrum ist die SLUni spezialisiert auf Registerkorpora, multilinguale Korpora und Übersetzungskorpora. Außerdem unterhält die SLUni ein [CLARIN-D-Datenzentrum](https://fedora.clarin-d.uni-saarland.de/index.de.html) mit CoreTrustSeal-Zertifizierung.

Der Schwerpunkt des Datenzentrums liegt auf multilingualen Korpora sowie Korpuswerkzeugen und mehr als 100 Datenressourcen wurden bereits im Repositorium der SLUni archiviert. Die Ressourcen sind über das [Virtual Language Observatory](https://vlo.clarin.eu/search?fq=collection:Universit%C3%A4t+des+Saarlandes+CLARIN-D-Zentrum,+Saarbr%C3%BCcken) auffindbar und eine Auswahl der archivierten Korpora ist zudem über die [Federated Content Search](https://contentsearch.clarin.eu/) durchsuchbar.

Hiervon sind im Zusammenhang mit Text+ zwei diachrone Korpora für das Englische hervorzuheben:

- **Royal Society Corpus (RSC)** Das RSC beinhaltet wissenschaftliche Publikationen aus den Jahren 1665 bis 1920, die in den Proceedings der Royal Society of London veröffentlicht wurden. Das Korpus wurde umfangreich auf Text-, Satz- und Tokenenbene annotiert und umfasst 78,6 Millionen Token.
- **Old Bailey Corpus (OBC)** Das Korpus dokumentiert gesprochenes Englisch aus zwei Jahrhunderten (1720 bis 1913) und basiert auf Verhandlungsprotokollen des zentralen Strafgerichtshofs in London. Die Texte des OBC umfassen 24,4 Millionen Token und wurden mit soziobiografischen und pragmatischen Annotationen versehen.

Aufgrund ihrer freien Lizenz, Größe und breiten Nutzung in der Forschung sind diese Datenressourcen für eine Übernahme in Text+ besonders relevant. Weiterhin beinhaltet das Repositorium der SLUni Übersetzungskorpora, darunter EuroParl-UdS und EPIC-UdS, sowie eine Reihe slawischer Ressourcen.
