---
title: Jülich Supercomputing Centre (JSC)
short_name: JSC
text_plus_domains:
- Infrastruktur/Betrieb

external_url: 'https://www.fz-juelich.de/en/ias/jsc'

type: competence-center
---

# [Jülich Supercomputing Centre (JSC)](https://www.fz-juelich.de/ias/jsc/EN/Home/home_node.html;jsessionid=64EE2D42CAB34F36E87AFAFB9A9DB68F)

Das JSC am [Forschungszentrum Jülich](https://www.fz-juelich.de/) betreibt seit 1987 das erste deutsche Höchstleistungsrechenzentrum und setzt im Jülicher Institute for Advanced Simulation die lange Tradition des wissenschaftlichen Rechnens in Jülich fort. Es stellt den Forschern in Deutschland und Europa über ein unabhängiges Peer-Review-Verfahren Rechenzeit der höchsten Leistungsebene zur Verfügung. Im JSC arbeiten rund 200 Experten und Ansprechpartner für alle Aspekte rund um Supercomputing und Simulationswissenschaften. Ein Schwerpunkt des JSC liegt auf dem Gebiet der föderierten Systeme und Daten. Hier werden neben der europäischen Open-Source-Software UNICORE gemeinsam mit Anwendern Anwendungsumgebungen und communityspezifische Dienste für verteilte Daten- und Recheninfrastrukturen entwickelt. Der föderierte Entwicklungsansatz respektiert die Autonomie der Nutzergruppen und Zentren.
