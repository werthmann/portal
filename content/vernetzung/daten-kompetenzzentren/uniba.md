---
title: Otto-Friedrich-Universität Bamberg (UniBA)
short_name: UniBA
text_plus_domains:
- Infrastruktur/Betrieb

external_url: 'https://www.uni-bamberg.de/'

type: competence-center
---

# [Otto-Friedrich-Universität Bamberg (UniBA)](https://www.uni-bamberg.de/)

Die Schwerpunkte des [Lehrstuhls für Medieninformatik](https://www.uni-bamberg.de/minf/) an der Universität Bamberg sind Information Retrieval, Datenmanagement und Forschungsinfrastrukturen der Digital Humanities. Der Lehrstuhl beteiligt sich seit 2011 an [DARIAH-DE](https://de.dariah.eu/) und ist Partner in [CLARIAH-DE](https://clariah.de/). Auf der Grundlage von DARIAH-DE und CLARIAH-DE und der Umsetzung von geförderten und nicht geförderten Anwendungsszenarien (z.B. mit dem [Forschungsverbund Marbach Weimar Wolfenbüttel](https://www.mww-forschung.de/), [Germanisches Nationalmuseum](https://www.gnm.de/en/)) hat die Gruppe die [DARIAH-DE Data Federation Architecture (DFA)](https://de.dariah.eu/en/data-federation-architecture) implementiert, die als wichtiger Enabler für die Interoperabilität und Auffindbarkeit von Forschungsdaten dient. Als primäre DFA-Komponente zur Herstellung der Interoperabilität zwischen heterogenen Datenquellen werden die [Data Modeling Environment (DME)](https://de.dariah.eu/en/web/guest/dme) und die darauf aufbauende [Generic Search](https://de.dariah.eu/en/web/guest/generische-suche) der Ausgangspunkt für entsprechende Anwendungen, Anpassungen und Weiterentwicklungen im Rahmen von Text+ sein.
