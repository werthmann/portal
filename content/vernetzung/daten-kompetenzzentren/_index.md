---
title: Daten- und Kompetenzzentren

aliases:
- /forschungsdaten/daten-und-kompetenzzentren

menu:
  main:
    weight: 20
    parent: vernetzung
---

# Daten- und Kompetenzzentren

{{<lead-text>}}
Die Text+ Datendomänen sind in thematischen Clustern organisiert, wodurch eine umfassende Erfassung von Forschungsdaten gewährleistet wird. Die Cluster bündeln alle Aktivitäten im Zusammenhang mit bestimmten Subtypen von Daten und Forschungsmethoden in einer Datendomäne entsprechend der Bedürfnisse und Forschungsprioritäten der jeweiligen Interessensgemeinschaft.
{{</lead-text>}}

Die folgenden acht Cluster werden sich zunächst auf die Bereiche Alte Kulturen, Anthropologie, Klassische Philologie, Komparative Literaturwissenschaft, Computerlinguistik, Sprach- und Literaturwissenschaft für europäische und außereuropäische Philologien, Mediävistik, Philosophie und Religionswissenschaften konzentrieren.

{{<image img="Data-Domains-Thematic-Clusters_2023.png">}}
Zuordnung von thematischen Clustern zu ihren Datendomänen.
{{</image>}}

Ein Cluster besteht in der Regel aus mindestens einem, oft mehreren Datenzentren und weiteren Kompetenzzentren. Diese sind in der folgenden Übersicht dargestellt.

{{<competence-center-list>}}
