---
title: Data and Competence Centers

aliases:
- /research-data/data-and-competence-centers

menu:
  main:
    weight: 20
    parent: vernetzung
---

# Data and Competence Centers

{{<lead-text>}}
The Text+ data domains are organized into thematic clusters, ensuring comprehensive coverage of research data. These clusters consolidate all activities related to specific subtypes of data and research methods in a data domain according to the needs and research priorities of the respective interest groups.
{{</lead-text>}}

The following eight clusters will initially focus on the areas of Ancient Cultures, Anthropology, Classical Philology, Comparative Literature, Computational Linguistics, Language and Literature Studies for European and non-European Philologies, Medieval Studies, Philosophy, and Religious Studies.

{{<image img="Data-Domains-Thematic-Clusters_2023.png">}}
Mapping of thematic clusters to their data domains.
{{</image>}}

A cluster typically consists of at least one, often multiple data centers, and additional competence centers. These are presented in the following overview.

{{<competence-center-list>}}
