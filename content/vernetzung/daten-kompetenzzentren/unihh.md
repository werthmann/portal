---
title: Universität Hamburg (UniHH), Hamburger Zentrum für Sprachkorpora (HZSK)
short_name: UniHH
text_plus_domains:
- Collections
text_plus_clusters:
  Collections:
  - Contemporary Language

external_url: 'https://www.uni-hamburg.de/'

type: competence-center
---

# [Universität Hamburg (UniHH)](https://www.uni-hamburg.de/), [Hamburger Zentrum für Sprachkorpora (HZSK)](https://corpora.uni-hamburg.de/hzsk/)

Datenzentrum in folgenden Clustern der Datendomäne Collections: Contemporary Language

Das HZSK ist am Institut für Germanistik im Fachbereich Sprache, Literatur und Medien der Universität Hamburg angesiedelt. Es bietet eine institutionelle Basis, um die nachhaltige Nutzbarkeit sprachwissenschaftlicher Primärforschungsdaten über zeitlich befristete Forschungsprojekte hinaus zu gewährleisten. Als ein Zusammenschluss von Mitgliedern verschiedener Fakultäten und Institutionen der Universität Hamburg unterstützt das HZSK die Konsistenz und Koordination computergestützter empirischer Forschung und Lehre der Sprachwissenschaft sowie der an die Universität Hamburg angegliederten Nachbardisziplinen über die Projektlaufzeiten hinaus.

Das HZSK hat in den letzten zehn Jahren zahlreiche Projekte aus verschiedenen Fachbereichen der Universität Hamburg zusammengeführt und koordiniert. Die Forschungsdaten aus diesen Projekten wurden kuratiert und im [HZSK Repository](https://corpora.uni-hamburg.de/hzsk/en/repository-search) einer breiten Nutzergemeinschaft zur Verfügung gestellt. Diese digitale Forschungsinfrastruktur wurde unter Berücksichtigung von Standards und Best Practices der digitalen Forschung entwickelt und mit einem [CoreTrustSeal](https://www.coretrustseal.org/) zertifiziert.

Darüber hinaus arbeitet das HZSK eng mit dem neu gegründeten [Zentrum für nachhaltiges Forschungsdatenmanagement (FDM)](https://www.fdm.uni-hamburg.de/) an der Universität Hamburg zusammen. Das FDM hat sein Repositorium 2019 in Betrieb genommen und wird in Zukunft auch Daten aus dem HZSK-Repositorium sammeln können. In Zusammenarbeit mit dem FDM wird das HZSK weiterhin als Kompetenzzentrum fungieren, das bei der Koordination und Kuratierung von Forschungsdaten berät und Schulungen, z.B. zu digitalen Nutzertools, anbietet. Das Repositorium des HZSK beherbergt mehr als 50 Korpora, die mehrheitlich dem thematischen Bereich der mehrsprachigen mündlichen und schriftlichen Daten sowie Daten aus weniger verbreiteten oder gefährdeten Sprachen angehören. Neben einer Vielzahl von (Kinder-)Spracherwerbskorpora und anderen Korpora, die sich auf einzelne Aspekte der Mehrsprachigkeit konzentrieren, werden weitere hochrelevante Themen zu den gesellschaftlichen Aspekten der Mehrsprachigkeit abgedeckt, z.B. durch die Korpora [Dolmetschen im Krankenhaus (DiK)](https://corpora.uni-hamburg.de/hzsk/de/islandora/object/spoken-corpus:dik) und die [Community Interpreting Database (ComInDat)](https://corpora.uni-hamburg.de/hzsk/de/islandora/object/spoken-corpus:comindat). Durch Datendepots aus abgeschlossenen externen Projekten in Zusammenarbeit mit dem HZSK wächst die Sammlung stetig an, mit anstehenden Depots wie Korpora der mehrsprachigen Kommunikation in Institutionen (z.B. Schulen, Unternehmen, NGOs).
