---
title: Berlin-Brandenburgische Akademie der Wissenschaften (BBAW)
short_name: BBAW
text_plus_domains:
- Collections
- Lexikalische Ressourcen
- Editionen
text_plus_clusters:
  Collections:
  - Historical Texts (Coord.)
  - Contemporary Language
  Lexikalische Ressourcen:
  - German Dictionaries in a European Context
  - Non-Latin Scripts
  Editionen:
  - Ancient and Medieval Texts
  - Early Modern, Modern, and Contemporary Texts

external_url: "https://www.bbaw.de/"

type: competence-center
---

# [Berlin-Brandenburgische Akademie der Wissenschaften (BBAW)](https://www.bbaw.de/)

## Collections - [Zentrum Sprache](https://www.bbaw.de/forschung/zentren/zentrum-sprache)

Datenzentrum in folgenden Clustern der Datendomäne Collections: Historical Texts (Coord.); Contemporary Language

Das [Zentrum Sprache](https://www.bbaw.de/forschung/zentren/zentrum-sprache) an der BBAW verfügt über verschiedene Textsammlungen und Spezialkorpora, die vor allem die (historische) deutsche Sprache dokumentieren. Dazu gehört das [Deutsche Textarchiv (DTA)](http://www.deutschestextarchiv.de/), das mit mehr als 350 Millionen Tokens auf 1,34 Millionen digitalisierten Seiten das größte zusammenhängende (Referenz-)Korpus des Neuhochdeutschen vom 16. bis zum frühen 20.Jahrhundert darstellt. In der Community der Korpus- und ComputerlinguistInnen, LiteraturwissenschaftlerInnen, HistorikerInnen, KulturwissenschaftlerInnen und anderen Forschungsbereichen ist das DTA etabliert und weit verbreitet. Das DTA umfasst umfassend annotierte Volltexttranskriptionen von Drucken, Zeitungen und Zeitschriften sowie handgeschriebenen Dokumenten verschiedener Gattungen und Textarten. Die Transkriptionen sind gemäß der Empfehlungen der [Text Encoding Initiative (TEI)](https://tei-c.org/) in TEI-XML kodiert. Externe Beitragende können weitere Textressourcen als DTA-Erweiterungen (DTAE) in die DTA-Infrastruktur integrieren. Der Workflow deckt alle Schritte der Datenkuration von der Erfassung und Annotation, dem Rendering in verschieden Ausgabeformaten (HTML, plaintext, …) bis hin zur  Publikation und Archivierung der Text- und Metadaten ab. Im Rahmen des Projekts [CLARIAH-DE](https://clariah.de/) wird die gesamte „[Digitale Bibliothek](https://textgrid.de/en/web/guest/digitale-bibliothek)“ des [TextGrid Repository](https://textgridrep.org/) nach sorgfältiger Kuratierung, Ergänzung, notwendigen Korrekturen und Anreicherung der Bestandsdaten aus dem TextGridRep in die BBAW-Infrastruktur integriert und damit die beiden größten wissenschaftlich annotierten Literaturkorpora zusammengeführt.

Alle Texte werden nach dem DTA-Basisformat ([DTABf](http://www.deutschestextarchiv.de/doku/basisformat/)) kodiert, einem Subset der TEI-Richtlinien, was zu vollständig standardisierten und interoperablen Dokumenten führt. Das DTABf wird von der [Deutschen Forschungsgemeinschaft (DFG)](https://www.dfg.de/) und [CLARIN-D](https://www.clarin-d.net/de/) empfohlen und wurde bereits von mehr als 30 Projekten im In- und Ausland verwendet. Ein Set aus Werkzeugen und Services hilft im Vorfeld bei der Vorbereitung, Verarbeitung und Analyse der Daten, während die webbasierte Plattform [DTAQ](http://www.deutschestextarchiv.de/dtaq/about) die kollaborative Qualitätssicherung unterstützt. DTAQ stellt verschiedene Such- und Abrufmöglichkeiten, Datenanalyse- und Visualisierungswerkzeuge zur Verfügung. Es werden verschiedene Ausgabeformate zum Herunterladen und zur Wiederverwendung in anderen Kontexten generiert.

Das DTA ist eng mit dem [Digitalen Wörterbuch der deutschen Sprache (DWDS)](https://www.dwds.de/) verbunden und innerhalb einer gemeinsamen Infrastruktur zugänglich. Daraus ergibt sich ein Korpusbestand, der mehr als 500 Jahre umfasst, vom 16. Jahrhundert bis in die Gegenwart. Integrierte Spezialkorpora, die ebenfalls am Zentrum Sprache gehostet werden, decken darüber hinaus noch frühere Epochen ab, z.B. das [mittelhochdeutsche Referenzkorpus](https://www.linguistics.rub.de/rem/). Als einer der Koordinatoren des von der DFG geförderten [OCR-D](https://www.bbaw.de/forschung/ocr-d)-Projekts, durch die Bereitstellung von Ground Truth-Daten sowie Formatempfehlungen für den OCR-Prozess hat das Zentrum Sprache an der BBAW dazu beigetragen, die Expertise und Infrastruktur für die Volltextdigitalisierung der umfangreichen Collections des VD 16, 17, 18 (Verzeichnisse der im deutschen Sprachbereich erschienenen Drucke) und des 19. Jahrhunderts aufzubauen und zu gestalten.

Die an der BBAW aufgebaute Korpus-Infrastruktur stellt die langfristige Verfügbarkeit, die dauerhafte Adressierbarkeit und die Versionierung der Daten über das [CoreTrustSeal](https://www.coretrustseal.org/)-zertifizierte CLARIN-Repository sicher. Im Rahmen dieser Aktivitäten wird das Zentrum Sprache als Kompetenzzentrum für historische Texte und Daten sowie für Formatspezifikationen und Standardisierungsaktivitäten, Werkzeuge und Dienstleistungen in diesem Bereich eingerichtet. Darüber hinaus hat die BBAW im Rahmen von CLARIN-D mehr als 50 Kooperationsprojekten Beratung und Schulung zu den entsprechenden Werkzeugen, Arbeitsabläufen und Verfahren angeboten.

## Lexikalische Ressourcen - [Zentrum für digitale Lexikographie der deutschen Sprache (ZDL)](https://www.bbaw.de/forschung/zentrum-fuer-digitale-lexikographie-der-deutschen-sprache) und [Zentrum Grundlagenforschung Alte Welt (RCAW)](http://altewelt.bbaw.de/)

Datenzentrum in folgenden Clustern der Datendomäne Lexikalische Ressourcen: German Dictionaries in a European Context; Non-Latin Scripts

Das ZDL an der BBAW wird umfassende lexikalische Ressourcen für die deutsche Sprache in zeitgenössischer und historischer Form zur Verfügung stellen, die einheitlich strukturiert sind, den Standards der [Text Encoding Initiative (TEI)](https://tei-c.org/) entsprechen und über eine gemeinsame Lemma-Liste miteinander verbunden sind. Diese Liste ist frei verfügbar und wird als Drehscheibe für die Integration anderer Ressourcen in die Text+ Datendomäne Lexikalische Ressourcen dienen. Das Portfolio des ZDL umfasst außerdem große zeitgenössische Textkorpora, die mit den lexikalischen Ressourcen des [Digitales Wörterbuch der deutschen Sprache (DWDS)](https://www.dwds.de/) verknüpft sind, darunter große Referenzkorpora und große Webkorpora. Auf der Grundlage dieser Korpora werden Dienste für lexikometrische Statistiken bereitgestellt, darunter Zeitreihen für lexikalische Items und Wortvorkommensstatistiken sowohl aus synchroner als auch aus diachroner Perspektive. Durch ihre Beteiligung am Projekt [e-Humanities – Zentrum für Historische Lexikographie (ZHistLex)](https://zhistlex.de/) hat die BBAW einen Prototyp für die Integration verschiedener Sprachstufenwörterbücher entwickelt, die an anderen Akademien erstellt werden (Althochdeutsch, Mittelhochdeutsch und Frühneuhochdeutsch). Das Portal und die Suchmöglichkeiten über Application Programming Interfaces (APIs) werden dazu beitragen, diese Ressourcen für die Untersuchung von langwierigen Sprachänderungen verfügbar zu machen. Der Software-Dienst Cascaded Analysis Broker (CAB) zur Rechtschreibnormalisierung wird die Vernetzung historischer Wörterbücher sowie die Verknüpfung von Wörterbüchern mit historischen Textressourcen verbessern. Soweit es die urheberrechtlichen Beschränkungen erlauben, sind die im BBAW [CLARIN-Zentrum](https://centres.clarin.eu/all_centres) gehosteten Ressourcen über [dwds.de](https://www.dwds.de/) und [zdl.org](https://zdl.org/) einer größeren Öffentlichkeit, einschließlich wissenschaftlicher Nutzerinnen und Nutzer, zugänglich. Mit mehr als 1 Million Aufrufen pro Monat sind sie die meistbesuchten wissenschaftlichen Websites für lexikalische Ressourcen in Deutschland.

Das Zentrum RCAW umfasst neun bedeutende Langzeitprojekte, die digitale Textdaten in verschiedenen alten Sprachen und Schriften produzieren. Einige von ihnen befassen sich mit griechischen Manuskripten, wie zum Beispiel

- die [alexandrinische und antiochenische Bibelexegese in der Spätantike](https://www.bbaw.de/en/research/die-alexandrinische-und-antiochenische-bibelexegese-in-der-spaetantike-the-late-antique-biblical-exegesis-of-alexandria-and-antioch),
- [Commentaria in Aristotelem Graeca et Byzanztina](https://cagb-db.bbaw.de/) und
- [Galen von Pergamon. Die Überlieferung, Deutung und Vollendung der antiken Medizin](http://cmg.bbaw.de/project-office)

und einige mit dokumentarischen Quellen aus der klassischen Welt, wie zum Beispiel

- [Corpus Inscriptionum Latinarum](https://cil.bbaw.de/) und
- [Inscriptiones Graecae](http://ig.bbaw.de/)

sowie mit der europäischen Rezeption antiker Objekte seit der frühen Neuzeit, wie z.B.

- [Census of Ancient Works of Art and Architecture](http://www.census.de/).

Die Datenbank [Thesaurus Linguae Aegyptiae](http://aaew.bbaw.de/tla/) ist die weltweit führende Datenquelle zu (prä-koptischen) altägyptischen Lexemen und transliterarischen Texten. Als Publikationsplattform, die vom Projekt „Struktur und Transformation im Wortschatz der ägyptischen Sprache“ im Internet zur Verfügung gestellt wird, bietet sie das weltweit größte elektronische Korpus (1,4 Millionen Token) ägyptischer Texte, die mit Übersetzungen, Kommentaren und Metadaten annotiert sind. Es ist durchgehend mit einem umfassenden Lexikon der ägyptischen Sprache in ihren diachronen Phasen lemmatisiert. Die Daten sind unter einer Open-Access-Lizenz verfügbar. Sie werden weltweit von mehr als 7500 registrierten Benutzern genutzt. Die Website sowie eine API, die implementiert werden soll, werden den Zugang zu diesen Daten sowohl für akademische Nutzer als auch für die breite Öffentlichkeit erleichtern.

## Editionen

Kompetenzzentrum in folgenden Clustern der Datendomäne Editionen: Ancient and Medieval Texts; Early Modern, Modern, and Contemporary Texts

Seit rund 20 Jahren plant, realisiert und hostet die Digital Humanities-Abteilung [TELOTA – IT/DH](https://www.bbaw.de/bbaw-digital/telota) (TELOTA: The Electronic Life Of The Academy) der BBAW zahlreiche digitale wissenschaftliche Editionen aus verschiedenen Disziplinen wie Philologie im Allgemeinen, Philosophie, Theologie und Geschichte (insbesondere Wissenschaftsgeschichte). Ein besonderer Schwerpunkt liegt dabei auf der Edition von Korrespondenzen.

Neben der Erarbeitung digitaler Editionen liegt der Fokus der TELOTA-Abteilung auf der Entwicklung von Forschungssoftware. In diesem Zusammenhang widmet sich die Abteilung vor allem dem Beitrag zu Standards für die Textkodierung, der Entwicklung benutzerfreundlicher Werkzeuge für die Erstellung digitaler Editionen, dem Design von Application Programming Interface (API) und nachhaltigen Publikationslösungen.  TELOTA beteiligt sich aktiv an der Entwicklung und Anpassung von Textkodierungsstandards auf der Grundlage der Empfehlungen der Text Encoding Initiative (TEI) und des DTA-Basisformats (DTABf). Ein zentrales Werkzeug mit dem die Geisteswissenschaftlerinnen und Geisteswissenschaftler der BBAW digitale Editionen erstellen, ist die benutzerfreundliche Redaktionssoftware ediarum, die seit 2012 von TELOTA entwickelt wird. Eine weitere zentrale Anwendung von TELOTA ist correspSearch, ein Webdienst zur Verknüpfung wissenschaftlicher Korrespondenzausgaben.

Ressourcen und Tools (Auswahl):

- Digitale Editionen: [Corpus Coranicum](https://corpuscoranicum.de/) (die ältesten textlichen Zeugnisse des Qur’an)
- [Inscriptiones Graecae](http://ig.bbaw.de/) (antike Inschriften)
- [edition humboldt digital](https://edition-humboldt.de/) (Manuskripte zur Thematik „Reisen“ im Oeuvre Alexander von Humboldts)
- [MEGAdigital](https://megadigital.bbaw.de/) (alle Textzeugnisse zu Kapital von Karl Marx und Friedrich Engels)
- und [schleiermacher digital](https://schleiermacher-digital.de/) (Dokumente zu Friedrich Schleiermachers philosophischem und theologischem Oeuvre).

Software:

- [correspSearch](https://correspsearch.net/) (Webservice zur Vernetzung von Briefeditionen)
- [ediarum](https://www.ediarum.org/) (darunter ediarum.BASIS, ediarum.WEB und ediarum.PDF)
- [rasmify](https://github.com/telota/rasmify) (Tool zum Umgang mit arabischen Schriftzeichen bzw. Diakritika)

Die BBAW engagiert sich über ihre Mitarbeiterinnen und Mitarbeiter aktiv in verschiedenen nationalen und internationalen Vereinigungen, die die Datendomäne Editionen von Text+ betreffen, darunter die Arbeitsgruppe eHumanities der Union der deutschen Akademien der Wissenschaften, die Arbeitsgruppe Research Software Engineering des Fachverbandes [Digital Humanities im deutschsprachigen Raum (DHd)](https://dig-hum.de/), die TEI-Fachgruppe „correspondence, das Digital Classicist-Seminar (Berlin) und das Institut für Dokumentologie und Editorik (IDE).
