---
title: Universität Duisburg-Essen (UniDUE)
short_name: UniDUE
text_plus_domains:
- Collections
text_plus_clusters:
  Collections:
  - Contemporary Language

external_url: 'https://www.uni-due.de/'

type: competence-center
---

# [Universität Duisburg-Essen (UniDUE)](https://www.uni-due.de/)

Datenzentrum in folgenden Clustern der Datendomäne Collections: Contemporary Language

Die Datenressourcen der UniDUE innerhalb von Text+ umfassen Sammlungen gesprochener Sprache, wie sie in Manuskripten und Protokollen des politischen Diskurses enthalten sind. Das charakteristische Korpus des [PolMine-Projekts](https://polmine.github.io/) ist eine digitale Sammlung von Parlamentsdebatten im Deutschen Bundestag ([Korpus GermaParl](https://polmine.github.io/corpora/)). Es ist eine treibende Kraft für textbasierte Forschung in der Politikwissenschaft zu Policy und Politik. Da die in Duisburg erstellten Sprachressourcen linguistisch annotiert sind und sich an die Richtlinien der [Text Encoding Initiative (TEI)](https://tei-c.org/) halten, sind sie auch für die sprachwissenschaftliche und zeitgeschichtliche Forschung sehr relevant. Derzeit werden die Daten über verschiedene Langzeitrepositorien sowie über die Webumgebung des Projekts verbreitet.

Ergänzend zu den Sammlungen bietet die UniDUE zugehörige Software-Tools an. Das [polmineR](https://cran.r-project.org/web/packages/polmineR/index.html)-Paket, das in der statistischen Programmiersprache R implementiert und über das [Comprehensive R Archive Network (CRAN)](https://cran.r-project.org/web/packages/polmineR/index.html) verfügbar ist, gewährleistet, dass eine Umgebung für die Analyse von Parlamentsdebatten funktional und vollständig interoperabel ist. Werkzeuge zur Integration der Analyse der parlamentarischen Rede, einschließlich interaktiver Visualisierungen, sind von Anfang an verfügbar und können leicht an die Anforderungen einzelner Forschungsprojekte angepasst werden. Das PolMine-Projekt ist in einer sich entwickelnden mehrsprachigen Forschungsgemeinschaft zur parlamentarischen Lautsprache sehr aktiv. Mitglieder des Teams sind an europäischen Kooperationen zur Bereitstellung parlamentarischer Daten für die Forschung in der Politik- und Sprachwissenschaft ([Parla-CLARIN](https://github.com/clarin-eric/parla-clarin)) beteiligt.
