---
title: 'Darmstadt Cooperation (DACo): Technische Universität Darmstadt, Universitäts- und Landesbibliothek Darmstadt, Hochschule Darmstadt'
short_name: DACo
text_plus_domains:
- Editionen
text_plus_clusters:
  Editionen:
  - Early Modern, Modern, and Contemporary Texts

external_url: 'https://www.tu-darmstadt.de/index.en.jsp'

type: competence-center
---

# Darmstadt Cooperation (DACo): [Technische Universität Darmstadt](https://www.tu-darmstadt.de/index.en.jsp), [Universitäts- und Landesbibliothek Darmstadt](https://www.ulb.tu-darmstadt.de/), [Hochschule Darmstadt](https://h-da.de/)

Kompetenzzentrum in folgenden Clustern der Datendomäne Editionen: Early Modern, Modern, and Contemporary Texts

Die DACo besteht aus drei Partnern mit einer langen Tradition institutioneller und persönlicher Zusammenarbeit in Forschung, Infrastrukturentwicklung, Lehre und Ausbildung auf dem Gebiet der Textwissenschaft, der digitalen Editionen und darüber hinaus: dem [Institut für Sprach- und Literaturwissenschaft](https://www.linglit.tu-darmstadt.de/institutlinglit/index.en.jsp), der [Universitäts- und Landesbibliothek Darmstadt (USLDA)](https://www.ulb.tu-darmstadt.de/), beide an der Technischen Universität Darmstadt (TUDa), und dem Lehrstuhl für Informationswissenschaft/Digitale Bibliothek an der [Hochschule Darmstadt](https://h-da.de/). Sie gehören zu den Gründern von [TextGrid](https://textgrid.de/) und sind Teil des Konsortiums [DARIAH-DE](https://de.dariah.eu/en) bzw. [CLARIAH-DE](https://www.clariah.de/). Die Darmstädter Kooperations-Hochschulen haben einen Vertrag über eine zukünftige enge Zusammenarbeit bei kooperativen Dissertationen unterzeichnet, der eine gemeinsame Betreuung des wissenschaftlichen Nachwuchses ermöglicht. Die Universitäts- und Landesbibliothek betreibt das institutionelle Repositorium für alle Forschungsdaten, die an der Technischen Universität entstehen oder mit denen an der Technischen Universität gearbeitet wird, und hat 2019 das [Zentrum für digitale Editionen in Darmstadt (ZEiD)](https://www.ulb.tu-darmstadt.de/ulb/zentrum_fuer_digitale_editionen/zentrum_fuer_digitale_editionen_zeid.de.jsp) gegründet. Das ZEiD betreibt und unterstützt eine Reihe von digitalen Editionen, darunter Langzeitprojekte, die im Rahmen des Akademienprogramms gefördert werden, Projekte, die von der [Deutschen Forschungsgemeinschaft (DFG)](https://www.dfg.de/) finanziert werden, sowie Kleinprojekte von Einzelforschenden ohne Förderung und andere Projekte mit besonderem Schwerpunkt auf Briefen. Die Mitglieder sind am Konsortium der [Text Encoding Initiative](https://tei-c.org/) und am Projekt [Hessische Forschungsdateninfrastrukturen (HeFDI)](https://www.uni-marburg.de/de/hefdi) beteiligt oder engagieren sich in der Arbeitsgruppe eHumanities der [Union der deutschen Akademien der Wissenschaften](https://www.akademienunion.de/). Seit fast 20 Jahren entwickeln sie Master- und Bachelor-Studiengänge, in denen Textwissenschaft, Forschungsdatenmanagement, Datenwissenschaft und Datenkompetenz eine zentrale Rolle spielen, und haben zahlreiche Digital Humanities-Workshops organisiert.
