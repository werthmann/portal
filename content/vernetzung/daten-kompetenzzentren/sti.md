---
title: Salomon Ludwig Steinheim-Institut für deutsch-jüdische Geschichte (STI)
short_name: STI
text_plus_domains:
- Editionen
text_plus_clusters:
  Editionen:
  - Ancient and Medieval Texts (Coord.)

external_url: 'http://www.steinheim-institut.de/wiki/index.php/Hauptseite'

type: competence-center
---

# [Salomon Ludwig Steinheim-Institut für deutsch-jüdische Geschichte (STI)](http://www.steinheim-institut.de/wiki/index.php/Hauptseite)

Datenzentrum in folgenden Clustern der Datendomäne Editionen: Ancient and Medieval Texts

Das STI ist ein assoziiertes Institut der [Universität Duisburg-Essen](https://www.uni-due.de/) und Mitglied der [Johannes-Rau-Forschungsgemeinschaft](https://jrf.nrw/) in Nordrhein-Westfalen.

Das STI erforscht die Geschichte und Kultur der Juden im deutschsprachigen Raum vom Mittelalter bis zur Gegenwart. Das dichte Beziehungsgeflecht zwischen jüdischer und allgemeiner Gesellschaft wird aus religions- und sozialgeschichtlicher, literatur-, kultur- und sprachwissenschaftlicher Perspektive untersucht, insbesondere mit Bezug auf innerjüdische und hebräische Quellen. Neben der deutsch-jüdischen Geschichte und Judaistik gibt es einen anwendungsorientierten Schwerpunkt auf Methoden der Digital Humanities (DH).

Das digitale Portfolio umfasst zahlreiche Editionen, prosopografische und bibliografische Werke, Fotosammlungen, Bildarchive, Briefe und Tagebücher. Ein Langzeitprojekt des STI ist die Edition jüdischer Epitaphien. 37.000 hebräische und deutsche Epitaphien von 218 jüdischen Friedhöfen, jeweils mit Transkriptionen, Übersetzungen, Objektbeschreibungen, Anmerkungen und Kommentaren wurden als digitale Editionen veröffentlicht (Extensible Markup Language [TEI](https://tei-c.org/)-XML, [Creative Commons Licensing](https://creativecommons.org/)).

Die digitalen Editionen und Sammlungen des STI stehen in engem Zusammenhang mit den Forschungsaktivitäten. Daher beteiligt sich das Institut an der Entwicklung von Forschungsplattformen und Infrastrukturkomponenten, die Web-Publishing, digitale Annotation, Retrieval, Visualisierung, Analyse und Verknüpfung der Daten ermöglichen. Folglich verfügt das Institut über grundlegende Expertise in der Verarbeitung von hebräischen (right-to-left, RTL) und EpiDoc- und TEI-Dateien ([Tübingen System of Text Processing tools, TUSTEP](https://www.tustep.uni-tuebingen.de/tustep_eng.html)). Darüber hinaus verfügt es über langjährige Praxis in den XML-Transformationssprachen XSLT und XQuery (u.a. Saxon), in Retrieval-Plattformen wie [Solr](https://lucene.apache.org/solr/) und in Werkzeugen und Technologien wie eXist-db und BaseX XML-Datenbanken, Apache Cocoon (XML-Web-Entwicklungs-Framework) und Mediawiki / Wikibase (Resource Description Framework RDF, SPARQL Protocol and RDF Query Language). Eigene Entwicklungen wie Epidat, der domänenspezifische STI Linked Data Service, die Judaica-Suchmaschine und ein bibliographisches System, das Georeferenzen und Normdateien unterstützt, basieren auf diesen Kompetenzen, genauso wie die Zusammenarbeit mit [Europeana](https://www.europeana.eu/) oder dem [PEACE Portal](https://peaceportal.org/). Vor diesem Hintergrund verfügt das STI über Erfahrungen mit den Anforderungen, der Implementierung und der Nutzung von Forschungsinfrastrukturen in den Geisteswissenschaften. Das STI ist ein langjähriges Mitglied von [TextGrid](https://textgrid.de/en) und der [International TUSTEP User Group (ITUG)](http://www.itug.de/). Mitglieder des Instituts werden bei verschiedenen Projekten zu digitalen Editionen konsultiert (z.B. als Mitglieder des Beirats). Die Aktivitäten umfassen wissenschaftliche Beiträge, Blog-Posts, Vorträge über Anwendungen und Methoden der DH sowie die Organisation von Schulungen und Workshops zu digitalen Editionen. Das STI nimmt an den Arbeitsgruppen der DH teil. In diesem Zusammenhang setzt es sich insbesondere für die Interoperabilität und Vernetzung von Ressourcen auf der Grundlage von Standards und Normdateien ein und beteiligt sich aktiv an der Öffnung der Integrated Authority File GND ([GND for Cultural Data](https://www.dnb.de/EN/Professionell/ProjekteKooperationen/Projekte/GND4C/gnd4c.html;jsessionid=0FE3CADB0E0F22A2B0490A03F5DB3946.internet561)) sowie an der wissenschaftlichen Nutzung von Wikibase in den Geisteswissenschaften.
