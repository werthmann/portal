---
title: Universität Tübingen (UniTÜ)
short_name: UniTÜ
text_plus_domains:
- Collections
- Lexikalische Ressourcen
text_plus_clusters:
  Collections:
  - Contemporary Language (Coord.)
  - Historical Texts
  Lexikalische Ressourcen:
  - Born-Digital Lexical Resources

external_url: "https://uni-tuebingen.de/"

type: competence-center
---

# [Universität Tübingen (UniTÜ)](https://uni-tuebingen.de/)

## Collections

Datenzentrum in folgenden Clustern der Datendomäne Collections: Contemporary Language (Coord.); Historical Texts

Die Datenressourcen der UniTÜ umfassen Korpora für gesprochene Sprache und geschriebene Texte, die auf verschiedenen Ebenen der linguistischen Annotation von Morphologie, Syntax und Semantik annotiert werden. Solche Korpora sind für die datengetriebene Forschung sowohl in der theoretischen als auch in der Computerlinguistik unverzichtbar. Die Annotationen umfassen verschiedene grammatikalische Rahmen und halten sich an die in der Gemeinschaft weit verbreiteten Kodierungsstandards sowie an die Kodierungsstandards der [International Standards Organization (ISO)](https://www.iso.org/home.html). Diese Ressourcen sind im TALAR-Datenrepository untergebracht, das vom CTS zertifiziert wurde und standardisierte Protokolle für den Daten-Ingest von externen Datenressourcen entwickelt hat.

Das Tübinger Data and Competence Centre beherbergt eine Sammlung von weit verbreiteten syntaktisch annotierten Korpora, die so genannten TüBa-Baumbanken für Deutsch, Englisch und Japanisch. Darüber hinaus enthält das [Tübinger Archiv für Sprachressourcen (TALAR)](https://talar.sfb833.uni-tuebingen.de/) eine große Anzahl extern entwickelter Treebanks im Rahmen der [Universal Dependencies](https://universaldependencies.org/). Alle sprachlich annotierten Korpora der UniTÜ können mit der Webanwendung [Tübingen Annotated Data Retrieval Application (TüNDRA)](https://weblicht.sfs.uni-tuebingen.de/Tundra/) durchsucht und visualisiert werden und sind auch über die CLARIN Federated Content Search zugänglich. Zusätzlich zu den sprachlich annotierten Korpora bietet die UniTÜ Datendienste in Form von Vektorraum-Wortdarstellungen und zugehörigen Softwaretools an. Darüber hinaus bietet sie Softwaredienste für die inkrementelle Annotation externer Textkorpora über die virtuelle Forschungsumgebung [WebLicht](https://weblicht.sfs.uni-tuebingen.de/weblichtwiki/index.php/Main_Page) an. WebLicht ermöglicht u.a. die automatische Anreicherung von Textkorpora mit einer Names-Entity-Erkennung auf der Basis von Deep-Learning-Tools und kann somit als Werkzeug für die automatische Anreicherung unstrukturierter Daten und die anschließende Verknüpfung mit Autoritätsdaten sowie verknüpften offenen Daten genutzt werden.

## Lexikalische Ressourcen

Datenzentrum in folgenden Clustern der Datendomäne Lexikalische Ressourcen: Born-Digital Lexical Resources

Die lexikalischen Ressourcen, die vom Tübinger Daten- und Kompetenzzentrum angeboten werden, sind eng mit anderen lexikalischen und textuellen Ressourcen, die in Text+ vertreten sind, verbunden und mit ihnen interoperabel. Das Valenzwörterbuch der deutschen Verben wurde aus großen Textkorpora abgeleitet und ist daher mit Korpusdaten verknüpft. [GermaNet](http://www.sfs.uni-tuebingen.de/GermaNet/) ist eine lexikalische Datenbank der Wortbedeutungen für zeitgenössiche deutsche Substantive, Verben und Adjektive, die über einen interlingualen Index direkt mit Wortnetzen von mehr als fünfzig Sprachen der Welt verbunden ist. Neben anderen Wortnetzen ist GermaNet mit anderen digital entstandenen Ressourcen wie [Wikipedia](https://www.wikipedia.org/) und [Wiktionary](https://www.wiktionary.org/) verknüpft. Zusammengenommen bieten sie eine prinzipielle Grundlage für die Beurteilung lexikalischer Ähnlichkeit und Unähnlichkeit. Diese beiden Begriffe sind sowohl für die psycho- und neurolinguistische Forschung als auch für die Themenmodellierung und die semantische Suche in einem breiten Spektrum von Disziplinen, das von Anwendungen in der Informatik bis hin zur literaturwissenschaftlichen Forschung reicht, unerlässlich. Hier sind z.B. Autorenidentifizierung und Genreklassifizierung sowie die semantische Suche nach Wörterbuchdaten oder nach großen Metadatensammlungen zu nennen. Neben dem akademischen Bereich ist GermaNet auch für industrielle Anwendungen sehr gefragt. Darüber hinaus bietet die Verknüpfung der Wortsinne über semantische Beziehungen einen idealen Ausgangspunkt für die Konvertierung von Wortnetzdaten in verknüpfte offene Datenformate und für die einfache Integration in Wissensgraphen. Diese Datenformate werden nicht nur in Text+, sondern auch in der [Nationalen Forschungsdateninfrastruktur (NFDI)](https://www.nfdi.de/) insgesamt eine zentrale Rolle spielen. Dementsprechend wird das Mapping von GermaNet auf verknüpfte offene Daten und Wissensgraphen einen erheblichen Mehrwert für Text+ bieten und eine direkte Datenbrücke zu anderen NFDI-Konsortien bilden.
