---
title: Sächsische Akademie der Wissenschaften zu Leipzig (SAW)
short_name: SAW
text_plus_domains:
- Lexikalische Ressourcen
- Infrastruktur/Betrieb
text_plus_clusters:
  Lexikalische Ressourcen:
  - German Dictionaries in a European Context
  - Born-Digital Lexical Resources
  - Non-Latin Scripts

external_url: "https://www.saw-leipzig.de/"

type: competence-center
---

# [Sächsische Akademie der Wissenschaften zu Leipzig (SAW)](https://www.saw-leipzig.de/)

## Lexikalische Ressourcen

Datenzentrum in folgenden Clustern der Datendomäne Lexikalische Ressourcen: German Dictionaries in a European Context; Born-Digital Lexical Resources; Non-Latin Scripts

Die SAW betreibt eine Vielzahl von Wörterbuchprojekten, die sich mit historischen und zeitgenössischen lexikalischen Daten befassen. Im Bereich der lexikalischen Daten in digitaler Form ist die [Leipzig Corpora Collection (LCC)](https://corpora.uni-leipzig.de/en) ein wichtiger Anbieter einsprachiger Wörterbüchern für Hunderte von Sprachen, wobei der Schwerpunkt auf statistisch fundierten Textanalysen und der Förderung von Sprachen mit weniger Ressourcen liegt. Das Projekt, das ursprünglich von der Universität Leipzig ins Leben gerufen wurde, wird von der SAW weitergeführt. Die Ressourcen der SAW umfassen historische und zeitgenössische lexikalische Daten für verschiedene Stadien der deutschen Sprache und eine große Sammlung einsprachiger Wörterbücher, die auf öffentlich zugänglichem Textmaterial basieren, das seit den 1990er Jahren gesammelt wurde. Gegenwärtig enthält die LCC mehr als 400 Korpora und Wörterbücher in mehr als 250 Sprachen. Die Daten werden über ein Webportal und RESTful-Webdienste (REST steht für Representational State Transfer) zur Verfügung gestellt, von denen viele in die [CLARIN](https://www.clarin.eu/)-Infrastruktur integriert sind. Die LCC ist zusammen mit ihrem Teilprojekt [Deutscher Wortschatz](https://wortschatz.uni-leipzig.de/en) eine der wichtigsten Online-Ressourcen im Bereich der Lexikographie moderner Sprachen und wirkt über den akademischen Bereich hinaus. Es stellt zuverlässige Text- und Lexikografiedaten für Hunderte von Sprachen zur Verfügung, die dann als Schulungsmaterial für etablierte Werkzeuge zur Verarbeitung natürlicher Sprache (Natural Language Processing, NLP) wie [Apache OpenNLP](https://opennlp.apache.org/) oder als Online-Nachschlagewerk (z.B. in Projekten wie [Wiktionary](https://www.wiktionary.org/)) dienen. Die LCC legt einen starken Schwerpunkt auf die Verbesserung der Verfügbarkeit digitaler Ressourcen für unterversorgte Sprachen. In Zusammenarbeit mit externen Sprachexperten unterstützt sie die Vorbereitung und das Hosting lexikalischer Datensätze in einer modernen Forschungsumgebung. Die LCC ist auch aktiv in der Verwendung, Standardisierung und Anpassung von Linked-Data-Formaten für lexikalische Ressourcen.

## Infrastruktur/Betrieb

Die SAW ist für mehr als 20 laufende Langzeitforschungsprojekte in den Geisteswissenschaften verantwortlich und engagiert sich in der Bereitstellung von Services und Support für die Geisteswissenschaften bei der Nutzung digitaler Ressourcen und Werkzeuge. Ab März 2021 werden die Services, die derzeit vom [CLARIN-D](https://www.clarin-d.net/)- und [CLARIAH-DE](https://www.clariah.de/)-Team am [Institut für Informatik der Universität Leipzig](https://www.informatik.uni-leipzig.de/ifi/startseite/) erbracht werden, an der SAW verstetigt. Sie verfügt damit über langjährige Erfahrungen in der Entwicklung technischer Infrastrukturen für die Geisteswissenschaften und wird  die Aufgaben des Arbeitspakets für die Koordination der technischen Entwicklung in CLARIAH-DE weiterführen. Die SAW wird insbesondere ihre Expertise im Bereich der Suche und Recherche in verteilten Umgebungen, der Metadateninfrastruktur und der semantischen Webtechnologie sowie der Qualitätssicherung von Diensten und Daten in die Text+-Infrastruktur einbringen.
