---
title: Nordrhein-Westfälische Akademie der Wissenschaften und der Künste (NRWAW)
short_name: NRWAW
text_plus_domains:
- Editionen
text_plus_clusters:
  Editionen:
  - Ancient and Medieval Texts (Coord.)
  - Early Modern, Modern, and Contemporary Texts

external_url: 'http://www.awk.nrw.de/about-us.html'

type: competence-center
---

# [Nordrhein-Westfälische Akademie der Wissenschaften und der Künste (NRWAW)](http://www.awk.nrw.de/about-us.html)

Datenzentrum in folgenden Clustern der Datendomäne Editionen: Ancient and Medieval Texts (Coord.); Early Modern, Modern, and Contemporary Texts

Die 1970 vom Land Nordrhein-Westfalen gegründete Akademie ist ein Zusammenschluss der führenden Forscherinnen und Forscher des Landes und vereint alle Formen der Erkenntnisgewinnung, sowohl wissenschaftlicher als auch künstlerischer Natur. Sie ist Mitglied der [Union der deutschen Akademien der Wissenschaften](https://www.akademienunion.de/) und arbeitet in internationalen Forschungsprojekten mit der [Union Académique Internationale (UAI)](http://www.uai-iua.org/) zusammen.

Eine zentrale Aufgabe der Akademie ist die Förderung und Betreuung von langfristiger Grundlagenforschung, die an Universitäten oder anderen Forschungseinrichtungen in dieser Form meist nicht durchgeführt werden kann. Die Akademie betreut derzeit 13 Langfristvorhaben, von denen sich viele mit dem textlichen Erbe und mit Editionen in all ihren Aspekten befassen, von der Antike bis zur Moderne und von deutscher Sprache bis zu nicht-lateinischen Schriften. Zu den Projekten gehören u.a. die [Averroes-Edition](https://averroes.uni-koeln.de/), die Edition der [Fränkischen Herrschererlasse](https://capitularia.uni-koeln.de/en/), die Rekonstruktion des [griechischen Neuen Testaments](http://www.awk.nrw.de/forschung/forschungsvorhaben-im-akademienprogramm/novum-testamentum-graecum.html), die Edition der [Kleinen und Fragmentarischen Historiker der Spätantike](http://kfhist.awk.nrw.de/), die genetische Edition der literarischen Werke von [Arthur Schnitzler](https://www.arthur-schnitzler.de/) und die Digitalisierung und Edition der [Zettelkästen von Niklas Luhmann](https://niklas-luhmann-archiv.de/). Die Koordinierungsstelle für Digital Humanities der Akademie fungiert als zentrales Kompetenzzentrum für alle Langfristvorhaben der Akademie, um den Einsatz aktueller digitaler Methoden zu gewährleisten und den gesamten Projektlebenszyklus abzudecken, mit einem Schwerpunkt auf editionsrelevanten Methoden und Technologien. Die Koordinierungsstelle ist am [Cologne Center for eHumanities (CCeH)](https://cceh.uni-koeln.de/) angesiedelt und arbeitet eng mit dem [Data Center for the Humanities (DCH)](https://dch.uni-koeln.de/) zusammen. Sie ist aktiv in Forschung und Lehre auf dem Gebiet der [Digital Humanities und der Informationsverarbeitung](https://dh.phil-fak.uni-koeln.de/en/) an der Universität zu Köln beteiligt. Derzeit hat die Akademie die Position des Sprechers der [Arbeitsgruppe eHumanities](https://www.akademienunion.de/en/working-groups/working-group-on-ehumanities/) der Union der deutschen Akademien der Wissenschaften inne. Die Beteiligung an der Nationalen Forschungsdateninfrastruktur (NFDI) ist Ausdruck der langfristigen strategischen Ausrichtung der Forschung der Akademie. Als Mitantragsteller ist die Akademie für den Aufgabenbereich Editionen in Text+ verantwortlich und wird ihre langjährige Erfahrung in der Digitalisierung sowie in der Erstellung und Pflege von Editionen unterschiedlichster Art einbringen. Die Koordinierungsstelle der Akademie bringt umfangreiche Erfahrungen in Beratung, Planung, Durchführung und Hosting digitaler Forschungsprojekte sowie im Datenmanagement, in der Archivierung und in der Umsetzung von Nachhaltigkeitsmaßnahmen ein. Besonderes Gewicht wird auf die wissenschaftsgetriebene Entwicklung von Forschungsinfrastrukturen in enger Wechselwirkung mit Forschung und Innovation in den digitalen Geistes- und Informationswissenschaften gelegt.
