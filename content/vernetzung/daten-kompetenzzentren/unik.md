---
title: Universität zu Köln (UniK), Data Center for the Humanities (DCH)
short_name: UniK
text_plus_domains:
- Collections
- Lexikalische Ressourcen
text_plus_clusters:
  Collections:
  - Contemporary Language
  Lexikalische Ressourcen:
  - Born-Digital Lexical Resources
  - Non-Latin Scripts

external_url: "https://www.uni-koeln.de/"

type: competence-center
---

# [Universität zu Köln (UniK)](https://www.uni-koeln.de/), [Data Center for the Humanities (DCH)](https://dch.phil-fak.uni-koeln.de/)

Datenzentrum in folgenden Clustern der Datendomäne Collections: Contemporary Language

## Collections

Das DCH ist ein Kompetenzzentrum für nachhaltiges Forschungsdatenmanagement (FDM) in den Geisteswissenschaften. Die UniK ist als Forschungsstandort im Bereich Digital Humanities/eHumanities international sichtbar. Als Fakultätseinrichtung zeichnet sich das DCH durch eine große Nähe zur Forschung aus und ist aktiv in die Lehre in den einschlägigen Studiengänge der Digital Humanities und der (Sprachlichen-)Informationsverarbeitung an der Universität zu Köln eingebunden. Das DCH ist ein [CLARIN-Zentrum](https://www.clarin.eu/content/overview-clarin-centres) und Teil des akkreditierten verteilten CLARIN Knowledge-Centre für sprachliche Vielfalt und Sprachdokumentation. Als Forschungsdatenzentrum der Philosophischen Fakultät übernimmt das DCH die Verantwortung für die institutionelle Sicherung, Bereitstellung und Langzeitarchivierung aller ihr anvertrauten digitalen Ressourcen.

Das Zentrum bietet Datenarchivierungs- und Publikationsdienste an, insbesondere für audiovisuelle Daten und lexikalische Ressourcen. Das DCH arbeitet eng mit dem Rechenzentrum der Universität zu Köln zusammen und nutzt dessen Infrastruktur. Als Kompetenzzentrum für Forschungsdatenmanagement sind FDM-Beratung und Metadaten besondere Kompetenzfelder des DCH. Darüber hinaus legt das DCH einen besonderen Schwerpunkt auf Sprachdaten aus dem globalen Süden und auf die Zusammenarbeit mit Institutionen und Wissenschaftlern aus dem globalen Süden. Das [Language Archive Cologne (LAC)](https://lac.uni-koeln.de/) ist ein Repositorium für audiovisuelle Daten mit Schwerpunkt auf Sprachaufnahmen. Das LAC ist Mitglied des [Digital Endangered Languages and Musics Archives Network (DELAMAN)](https://www.delaman.org/) und verfügt über besondere Expertise bei Daten aus gefährdeten und außereuropäischen Sprachen sowie bei Aufnahmen außereuropäischer mündlicher Literatur.

Das LAC ist in die [CLARIN](https://www.clarin.eu/)-Infrastruktur integriert und entspricht den technischen Standards dieser europäischen Forschungsdateninfrastruktur. Das DCH verfügt über umfangreiche Erfahrung und Expertise in lexikalischen Ressourcen für außereuropäische Sprachen. Mit [Kosh](https://cceh.github.io/kosh/) bietet das DCH eine generische Infrastruktur zur Veröffentlichung beliebiger XML-basierter lexikalischer Ressourcen über standardisierte Application Programming Interfaces (APIs) an. Die [Sanskrit-Wörterbücher](https://sanskrit-lexicon.uni-koeln.de/) [Cologne South Asian Languages and Texts (C-SALT)](http://c-salt.uni-koeln.de/) sind die größte Ressource für die klassische südasiatische Sprache Sanskrit und das [Kritische Pāli Wörterbuch](https://cpd.uni-koeln.de/) ist eine der größten lexikalischen Ressourcen für diese buddhistische liturgische Sprache. Die vom DCH zur Verfügung gestellten Representational State Transfer (REST) und GraphQL APIs ermöglichen die Verbindung der Ressourcen mit Texteditionen oder Sprachkorpora.

## Lexikalische Ressourcen

Datenzentrum in folgenden Clustern der Datendomäne Lexikalische Ressourcen: Born-Digital Lexical Resources; Non-Latin Scripts

Das DCH in Köln ist ein Kompetenzzentrum für nachhaltiges Forschungsdatenmanagement (FDM) in den Geisteswissenschaften. Es ist ein zertifiziertes [CLARIN-Zentrum](https://centres.clarin.eu/all_centres) und Teil des akkreditierten verteilten CLARIN Knowledge-Centre für sprachliche Vielfalt und Sprachdokumentation. Das DCH verfügt über umfangreiche Erfahrung und Expertise im Bereich lexikalischer Ressourcen für außereuropäische und alte Sprachen. Die [Sanskrit-Wörterbücher](https://sanskrit-lexicon.uni-koeln.de/) [Cologne South Asian Languages and Texts (C-SALT)](http://c-salt.uni-koeln.de/) sind beispielsweise die größte Ressource für die klassische südasiatische Sprache Sanskrit und das [Kritische Pāli Wörterbuch](https://cpd.uni-koeln.de/) ist eine der größten lexikalischen Ressourcen für diese buddhistische liturgische Sprache. Die vom DCH zur Verfügung gestellten Representational State Transfer (REST) und GraphQL APIs ermöglichen die Verbindung der Ressourcen mit Texteditionen oder Sprachkorpora. Die Kosh-Wörterbuchserver-Infrastruktur bietet eine generische Infrastruktur zur Veröffentlichung beliebiger XML-basierter lexikalischer Ressourcen über standardisierte APIs. Im Laufe von Text+ wird die DCH diese Ressourcen zur Verfügung stellen und die oben genannten APIs weiterentwickeln. Das Hauptaugenmerk der DCH in Text+ liegt auf nicht-lateinischen Skripten. Die DCH trägt jedoch zum Cluster Born-Digital Lexical Resources mit einer (multimodalen) lexikalischen Datenbank für Daten aus Feldforschungsstudien bei (Spracharchiv Köln).
