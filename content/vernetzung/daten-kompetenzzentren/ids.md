---
title: Leibniz-Institut für Deutsche Sprache (IDS)
short_name: IDS
text_plus_domains:
- Collections
- Lexikalische Ressourcen
- Infrastruktur/Betrieb
text_plus_clusters:
  Collections:
  - Contemporary Language (Coord.)
  - Historical Texts
  Lexikalische Ressourcen:
  - German Dictionaries in a European Context
  - Non-Latin Scripts

external_url: 'https://www.ids-mannheim.de/'

type: competence-center
---

# [Leibniz-Institut für Deutsche Sprache (IDS)](https://www.ids-mannheim.de/)

## Collections

Datenzentrum in folgenden Clustern der Datendomäne Collections:  Contemporary Language (Coord.); Historical Texts

Das IDS ist Deutschlands zentrale wissenschaftliche Einrichtung zur Dokumentation und Erforschung der deutschen Sprache in ihrem gegenwärtigen Gebrauch und der neueren Geschichte. Für seinen Auftrag, die sprachliche Vielfalt, Struktur und Verwendung der deutschen Sprache zu dokumentieren, zu archivieren und zu erforschen, hat das IDS die wichtigsten Sammlungen des Gegenwartsdeutschen aufgebaut. Im Bereich der Schriftsprache enthält das [Deutsche Referenzkorpus (DeReKo)](https://www1.ids-mannheim.de/kl/projekte/korpora/) 46,9 Milliarden Wörter aus vielen verschiedenen Gattungen, darunter Zeitungen, wissenschaftliche Texte und Werke der Belletristik, aber auch aus der computervermittelten Kommunikation aus Chat und Usenet sowie Wikipedia. Im Bereich der gesprochenen Sprache bietet das [Archiv für Gesprochenes Deutsch (AGD)](http://agd.ids-mannheim.de/index_en.shtml) 46 Korpora mit mehr als 4000 Stunden Audio- und audiovisuellen Aufnahmen an, die z.B. Ressourcen zu Dialekten oder „umgangssprachlichen“ Variationen sowie zur Sprache von Auswanderern nach Israel und deutschsprachigen Minderheiten in Namibia oder Russland sowie z.B. das Wendekorpus zur deutschen Wiedervereinigung oder das GeWiss-Korpus der akademischen Rede enthalten. Das [FOLK-Korpus](http://agd.ids-mannheim.de/folk.shtml) (Forschungs- und Lehrkorpus Gesprochenes Deutsch) bietet eine stratifizierte Auswahl einer großen Vielfalt an gesprochenem Deutsch in natürlichen Interaktionen.

Das IDS entwickelt ständig Werkzeuge und Schnittstellen zur Abfrage und Analyse der Korpora: Für gesprochene Korpora ist die [Datenbank für Gesprochenes Deutsch (DGD)](https://dgd.ids-mannheim.de/dgd/pragdb.dgd_extern.welcome) die zentrale Schnittstelle mit rund 12000 registrierten Nutzern. Für schriftliche Korpora wird [COSMAS II](https://www.ids-mannheim.de/cosmas2/) (Corpus Search, Management, and Analysis System, entwickelt seit den 1990er Jahren) zugunsten der Korpusanalyseplattform [KorAP](https://korap.ids-mannheim.de/) abgelöst. KorAp ist für große, mehrfach annotierte Korpora und komplexe Suchmechanismen optimiert und unterstützt mehrere Abfragesprachen. Letztere teilen sich die gleiche Benutzerbasis von über 54000 registrierten Benutzern.Das IDS ist seit Projektbeginn an [CLARIN/CLARIN-D](https://www.clarin.eu/) beteiligt und hat wesentlich zu [CLARINs Federated Content Search](https://www.clarin.eu/content/federated-content-search-clarin-fcs) und zur Entwicklung der [Virtual Collection Registry](https://www.clarin.eu/content/virtual-collections) beigetragen. Darüber hinaus war es aktiv an der Entwicklung von Standards für Sammlungen beteiligt, wobei es in der Arbeitsgruppe für linguistische Annotation der International Standards Organization ([ISO/TC 37/SC 4/WG 6](https://www.iso.org/committee/297592.html)) und in der Special Interest Group on [TEI for Linguists](https://tei-c.org/activities/sig/tei-for-linguists/) der [Text Encoding Initiative (TEI)](https://tei-c.org/) mitwirkte. Das IDS beherbergt auch den [juristischen Helpdesk von CLARIN](https://www.clarin-d.net/en/training-and-helpdesk/legal-helpdesk), der rechtliche und ethische Standards für Textsammlungen entwickelt. Darüber hinaus hat es die [CLARIN-Arbeitsgruppe für Deutsche Philologie](https://www.clarin-d.net/en/disciplines/german-philology) betreut.

## Lexikalische Ressourcen - Abteilung für Lexikalische Studien (IDS-Lexik)

Datenzentrum in folgenden Clustern der Datendomäne Lexikalische Ressourcen: German Dictionaries in a European Context; Non-Latin Scripts

Die Wörterbücher des IDS sind eine einzigartige Quelle für die akademische Lexikographie des Deutschen, die international auf Interesse stößt. Mit ihren unterschiedlichen thematischen und inhaltlichen Schwerpunkten (Neologismen, Diskurswortschatz, Fremdwörter, Lehnwörter, Kollokationen, Verbvalenz, grammatische Wörter etc.) sind sie eine notwendige wissenschaftliche Ergänzung zu weniger spezialisierten Angeboten ([Duden-Wörterbücher](https://www.duden.de/), [Zentrum für Digitale Lexikographie der deutschen Sprache](https://www.bbaw.de/forschung/zentrum-fuer-digitale-lexikographie-der-deutschen-sprache)). Darüber hinaus sind die lexikographischen Portale des IDS beispielhaft für neue Wege in der Visualisierung und Verarbeitung lexikographischer Daten und für experimentellere Formate, wie beispielsweise lexikalische Daten in Verbindung mit statistischen Korpusanalysen zu spezifisch eingeschränkten Sachgebieten (z.B. Neologismen). Sie befassen sich auch mit aktuellen Themen wie lexikalischen Veränderungen in der Coronakrise. Die lexikalischen Ressourcen des IDS sind alle online über die Portale [Online-Wortschatz-Informationssystem Deutsch (OWID, OWIDplus)](https://www1.ids-mannheim.de/lexik/owid.html), Lehnwortportal Deutsch und Grammatisches Informationssystem (grammis) zugänglich. Zusätzlich zu diesen lexikalischen Ressourcen trägt das IDS zu Text+ mit international anerkannter Expertise in der Erforschung der Wörterbuchnutzung und in der neuartigen (z.B. graphbasierten) Speicherung und Visualisierung lexikalischer Daten bei. Im Jahr 2019 wurden die IDS-Wörterbuchplattformen von mehr als 25.000 verschiedenen Benutzern genutzt.

## Infrastruktur/Betrieb

Das 1964 gegründete IDS in Mannheim, Deutschland, ist das führende nationale Zentrum, das die deutsche Sprache in ihrem zeitgenössischen Gebrauch und in der jüngeren Geschichte erforscht und dokumentiert. Aufgabe des IDS ist es, die sprachliche Vielfalt, Struktur und Verwendung der deutschen Sprache zu dokumentieren, zu archivieren und zu erforschen. In jüngster Zeit wurde das [Forum deutsche Sprache](https://www.forumdeutschesprache.de/) durch das IDS und seine Partner initiiert. Das IDS gilt auch als zentraler Knotenpunkt der internationalen deutschen Sprachwissenschaft und ist als führendes Zentrum der Grundlagenforschung anerkannt. Im Jahr 2019 wurde die [Abteilung Digitale Linguistik](https://www1.ids-mannheim.de/digspra/) gegründet, deren konstituierende Programmbereiche beide von der Leibniz-Gemeinschaft als exzellent bewertet wurden. Diese neue Abteilung wird Text+ beherbergen.

Darüber hinaus entwickelt das IDS praktische Werkzeuge und betreibt eine rechnergestützte Infrastruktur zur Unterstützung der empirischen Forschung und erstellt in engem Kontakt mit der ihm zugehörigen Gemeinschaft von Linguistinnen und Linguisten des Deutschen Nachschlagewerke (z.B. Grammatiken und Wörterbücher) und digitale Sprachressourcen (insbesondere große Korpora und Analysesoftware). Das IDS verfolgt überwiegend langfristig angelegte Projekte und entwickelt neue Forschungsschwerpunkte durch kompetitiv eingeworbene Drittmittel. Als Bindeglied zwischen Universitäten und anderen akademischen Partnern dient das IDS als Koordinator und Förderer langfristiger gemeinsamer Forschungsprojekte, wie [CLARIN-D](https://www.clarin-d.net/en/), und wirkt im Vorstand des [European Research Infrastructure Consortium CLARIN](https://www.clarin.eu/) sowie in internationalen Gremien im Hinblick auf Technologie und Organisation mit, wie z.B. der [Text Encoding Initiative](https://tei-c.org/) und der International Standards Organization.

Das IDS bringt seine Erfahrung sowohl in der Grundlagenforschung als auch in der Ressourcen- und Werkzeugentwicklung ein, seine Tradition, diese beiden Bereiche im Hinblick auf spezifische Forschungsprojekte und Forschungsfragen zu verbinden, und darüber hinaus seine Beiträge zu verteilten Forschungsinfrastrukturen. Als antragstellende Institution wird das IDS das Text+-Budget verwalten und sein Konsortium von Interessenvertretern integrieren. Unter Federführung des Aufgabenbereichs Administration wird das IDS für die Auszahlung der Projektmittel an die mitantragstellenden und beteiligten Institutionen verantwortlich sein und das Scientific Office von Text+ betreiben. Das IDS ist auch einer der zentralen Knotenpunkte in den Text+ Clustern mit zwei Spezialgebieten.
