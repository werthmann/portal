---
title: Niedersächsische Staats- und Universitätsbibliothek Göttingen (SUB)
short_name: SUB
text_plus_domains:
- Collections
- Editionen
- Infrastruktur/Betrieb
text_plus_clusters:
  Collections:
  - Unstructured Text (Coord.)
  Editionen:
  - Early Modern, Modern, and Contemporary Texts (Coord.)
  - Ancient and Medieval Texts

external_url: 'https://www.sub.uni-goettingen.de/'

type: competence-center
---

# [Niedersächsische Staats- und Universitätsbibliothek Göttingen (SUB)](https://www.sub.uni-goettingen.de/)

## Collections

Datenzentrum in folgenden Clustern der Datendomäne Collections: Unstructured Text (Coord.)

Mit ihrem derzeit rund 9 Millionen Medieneinheiten umfassenden Bestand zählt die SUB zu den größten Bibliotheken in Deutschland. Von besonderem Interesse für die geisteswissenschaftliche Forschung sind mehrere digitale Textsammlungen des [Göttinger Digitalisierungszentrums](https://gdz.sub.uni-goettingen.de/). Die SUB koordiniert das Digitalisierungsprojekt [VD18](https://gdz.sub.uni-goettingen.de/collection/vd18.digital) (Verzeichnis der im deutschen Sprachraum erschienenen Drucke des 18. Jahrhunderts) und ist Partner im [VD17](http://www.vd17.de/), das nicht nur digitalisierte Drucke in deutscher Sprache, sondern auch in vielen anderen europäischen Sprachen und darüber hinaus enthält. Die Sammlungen des VD17 und VD18 enthalten seltene Druckwerke wie literarische Anthologien, Reisezeitschriften, Chroniken, religiöse oder wissenschaftliche Dokumente. Der Schwerpunkt auf Reisezeitschriften, wissenschaftliche Dokumente und Druckerzeugnisse in den Textsammlungen der SUB bildet eine Brücke zu anderen disziplinär relevanten Sammlungen wie [Americana](https://gdz.sub.uni-goettingen.de/collection/nordamericana) (Literatur über Nordamerika), [Itineraria](https://gdz.sub.uni-goettingen.de/collection/itineraria) (Reisezeitschriften aus dem 16. bis 20. Jahrhundert), [Antiquitates & Archaeologica](https://gdz.sub.uni-goettingen.de/collection/antiquitates.und.archaeologia), [Wissenschaftsgeschichte und wissenschaftliche Zeitschriften](https://gdz.sub.uni-goettingen.de/collection/wissenschaftsgeschichte) (18. bis 20. Jahrhundert). Diese Sammlungen sind vor allem für die Philologie, Kultur und Kunst, Philosophie und Geschichte, Anthropologie, Religionswissenschaft, Politikwissenschaft und Medienwissenschaften relevant.

Diese Textsammlungen (über 13 Millionen digitalisierte Seiten) liegen in Bilddateien vor (TIFF, JPG, PDF) und stellen wertvolles Material für maschinelle Lernverfahren zur optischen Zeichenerkennung (OCR) und andere Bildverarbeitungsverfahren dar. Sie werden zum Teil im Projekt [Optical Character Recognition Development (OCR-D)](https://ocr-d.de/) verwendet, [an dem die SUB in Zusammenarbeit mit der GWDG beteiligt ist](https://www.dfg.de/download/pdf/foerderung/programme/lis/absichtserklaerungen_ocrd_2020/goettingen.pdf). Für die kommenden Jahre ist geplant, auch für die älteren Drucke maschinenlesbare Volltexte zu erreichen (VD17, VD18). Die Sammlung der wissenschaftlichen Zeitschriften (17.-21. Jh., verfügbar in [DigiZeitschriften](http://www.digizeitschriften.de/startseite/)) besteht überwiegend aus digitalisierten Volltexten. Diese Sammlung enthält vor allem interessantes und anspruchsvolles Material multimodaler (Text-Bild, performativ) und mehrsprachiger Texte. Für alle digitalisierten Textsammlungen stellt die SUB standardisierte Metadaten (bibliographische und strukturelle Metadaten, z.B. [IIIF-Manifest](https://iiif.io/technical-details/), [METS](http://www.loc.gov/standards/mets/)) zur Verfügung.

Neben dem Göttinger Digitalisierungszentrum pflegt das DARIAH-DE Coordination Office an der SUB das [TextGrid Repository](https://textgridrep.org/) und entwickelt dieses weiter. Es ist eine anerkannte und wertvolle Ressource für die Literaturwissenschaft (Philologie, Komparatistik) und baut es kontinuierlich aus. Es ist ein [CoreTrustSeal](https://www.coretrustseal.org/)-zertifiziertes, gemeinschaftskuratiertes Repository und offen für die Aufnahme neuer Daten (in verschiedenen Sprachen). Das TextGrid Repository enthält die [Digitale Bibliothek](https://textgrid.de/en/digitale-bibliothek) mit Titeln der Weltliteratur von mehr als 600 Autoren sowie weitere Textsammlungen in der standardisierten Extensible Markup Language der Text Encoding Initiative (TEI-XML).

Im Hinblick auf das TextGrid Repository erwägt die COST-Aktion „Distant Reading for European Literary History“, mehrere Sammlungen von Romanen, die zwischen 1840 und 1920 erstmals in mindestens sechs verschiedenen europäischen Sprachen veröffentlicht und in TEI-XML kodiert wurden, beizusteuern.

Für die Indologie enthält das [Göttinger Register für elektronische Texte in indischen Sprachen (GRETIL)](http://gretil.sub.uni-goettingen.de/gretil.html) eine der wertvollsten fächerrelevanten frei verfügbaren Volltextressourcen in Sanskrit, Pali, Prakrit, Neuindisch-Arier, Dravidischen Sprachen, Altjavanisch und Tibetisch.

All diese Beispiele für die Digitalisierung, Kodierung und Archivierung digitaler Textsammlungen stützen sich auf fundierte Fachkenntnisse in den Informationswissenschaften und digitalen Geisteswissenschaften, um eine nachhaltige digitale Infrastruktur für die Geisteswissenschaften auf nationaler und europäischer Ebene aufzubauen ([DARIAH ERIC](https://www.dariah.eu/), [CLARIAH-DE](https://clariah.de/), [SSHOC](https://www.sshopencloud.eu/)).

## Editionen

Datenzentrum in folgenden Clustern der Datendomäne Editionen: Early Modern, Modern and Contemporary Texts (Coord.); Ancient and Medieval Texts

Die SUB ist seit über 15 Jahren als Anbieterin von Informationstechnologie und Informationswissenschaften an der Erstellung digitaler Editionen beteiligt. Dazu gehört sowohl die Beteiligung an zahlreichen drittmittelfinanzierten Redaktionsprojekten als auch die Entwicklung und Bereitstellung von generischen Werkzeugen zur Erstellung und Publikation digitaler Ausgaben ([TextGrid](https://textgrid.de/), [SADE](https://gitlab.gwdg.de/SADE), [TextAPI](https://subugoe.pages.gwdg.de/emo/text-api/)). Darüber hinaus verfügt die SUB über umfangreiche Erfahrungen in der Vermittlung von Fähigkeiten im Bereich der digitalen Bearbeitung und der Werkzeug-Nutzung durch Schulungen, Workshops und Sommerschulen.

Die an der SUB entwickelten digitalen Editionen decken alle Archetypen ab (diplomatische, historisch-kritische und genetische Editionen) und umfassen ein breites Spektrum von Disziplinen:

- Philosophie ([Hannah Arendt Kritische Gesamtausgabe](https://hannah-arendt-edition.net/))
- Wissenschaftsgeschichte ([Goethes Farbenlehre](https://www.sub.uni-goettingen.de/projekte-forschung/projektdetails/projekt/wirkungsgeschichte-von-goethes-werk-zur-farbenlehre-in-berlin-1810-1832/); [Heyne Digital](https://heyne-digital.de/); [Gauß Briefwechsel](https://gauss.adw-goe.de/); [Briefportal Leibniz](https://leibniz-briefportal.adw-goe.de/))
- Bildungsforschung ([Klaus Mollenhauer Gesamtausgabe](https://www.sub.uni-goettingen.de/projekte-forschung/projektdetails/projekt/klaus-mollenhauer-gesamtausgabe/))
- Jüdische Studien ([Maps of God](https://www.sub.uni-goettingen.de/projekte-forschung/projektdetails/projekt/maps-of-god/))
- Kulturanthropologie ([Textdatenbank und Wörterbuch des Klassischen Maya](https://mayawoerterbuch.de/))
- Arabische Studien ([Die syrischen und arabischen Ahiqar-Texte](https://www.sub.uni-goettingen.de/projekte-forschung/projektdetails/projekt/ahiqar/))
- Theologie ([Bibliothek der Neologie](https://bdn-edition.de/))
- Literaturwissenschaft ([Theodor Fontane Notizbücher](https://fontane-nb.dariah.eu/))
- Kunstgeschichte ([ARCHITRAVE](https://architrave.eu/))

Einige dieser Projekte sind Hybridausgaben, die sowohl als Web-Portale als auch als Printpublikationen veröffentlicht wurden. Für diese Anforderung hat die SUB eine anpassungsfähige und wiederverwendbare Toolchain für die Erstellung von Prepress Files auf der Basis der Daten der Extensible Markup Language der [Text Encoding Initiative](https://tei-c.org/) (TEI-XML) entwickelt ([bdnPrint](https://gitlab.gwdg.de/bibliothek-der-neologie/print)). Darüber hinaus hat die SUB ihre Kompetenzen im Bereich der digitalen Editionen (Datenmodellierung, Softwareentwicklung, Projektakquisition und -management) durch die Einrichtung des [Services *Digitale Editionen*](https://www.sub.uni-goettingen.de/digitale-bibliothek/service-digitale-editionen/) konsolidiert, der aus eigenen Mitteln finanziert wird. Dieser Service bietet Beratungsdienste auf lokaler, nationaler und internationaler Ebene an. Die SUB ist auch an zahlreichen Standardisierungsgremien beteiligt, die für die Erstellung digitaler Editionen relevant sind, wie dem TEI-Konsortium, dem [IIIF-Konsortium](https://iiif.io/community/), dem [Dublin Core Governing Board](https://www.dublincore.org/groups/governing-board/), dem [MODS Editorial Committee](https://www.loc.gov/standards/mods/editorial-committee.html), und [LIDO – CIDOC](http://cidoc.mini.icom.museum/organisation/board/).

## Infrastruktur/Betrieb

Die SUB ist eine der größten Bibliotheken in Deutschland und führend in der Entwicklung digitaler Bibliotheken. Sie beherbergt mehrere digitale Sammlungen von erheblicher Bedeutung als Ressourcen für die Forschung in Text+, die vom Göttinger Digitalisierungszentrum zur Verfügung gestellt werden. Gemeinsam mit der Deutschen Nationalbibliothek (DNB) verwaltet die SUB die [Fachstelle Bibliothek](https://pro.deutsche-digitale-bibliothek.de/fachstelle-bibliothek) der Deutschen Digitalen Bibliothek und koordiniert die Aktivitäten der [DINI-AG KIM](https://dini.de/ag/kim/). Sie koordiniert [DARIAH-DE](https://de.dariah.eu/), ist Mitglied des Nationalen Koordinierungsausschusses von [DARIAH-ERIC](https://www.dariah.eu/) und koordiniert [CLARIAH-DE](https://clariah.de/) zusammen mit der UniTÜ. Die SUB bietet einen DOI-Service für die Geisteswissenschaften in Zusammenarbeit mit [DataCite](https://datacite.org/), der bereits über 40.000 Datensätze registriert hat, sowie lokale, nationale und internationale Unterstützung bei der Erstellung digitaler Editionen durch eine interne Einheit ([Service *Digitale Editionen*](https://www.sub.uni-goettingen.de/digitale-bibliothek/service-digitale-editionen/)). Auf internationaler Ebene ist die SUB wissenschaftlicher Koordinator von [OpenAIRE](https://www.openaire.eu/), Partner im europäischen Plug-in der [Research Data Alliance](https://www.rd-alliance.org/) und Partner im EOSC-Projekt SSHOC ([Social Sciences and Humanities Open Cloud](https://www.sshopencloud.eu/)).

Im Bereich Infrastruktur/Betrieb wird sich die SUB auf Community Services und Cross-Cutting Themen konzentrieren. Insbesondere wird sie einen Beitrag zur Metadateninfrastruktur leisten, um die Interoperabilität und Wiederverwendbarkeit der Daten in Text+ zu erhöhen. Die SUB ist Teil zahlreicher Standardisierungsgremien, wie z.B. dem [Konsortium der Text Encoding Initiative](https://tei-c.org/), dem [Dublin Core Governing Board](https://www.dublincore.org/groups/governing-board/), dem [Metadata Object Description Schema Editorial Committee (MODS/MADS)](https://www.loc.gov/standards/mods/editorial-committee.html), dem [International Image Interoperability Framework Consortium (IIIF)](https://iiif.io/), dem [CIDOC Conceptual Reference Model-SIG](http://www.cidoc-crm.org/) und der [LIDO Working Group](http://cidoc.mini.icom.museum/working-groups/lido/) (Lightweight Information Describing Objects). Die SUB ist maßgeblich an der Entwicklung und Weiterentwicklung verschiedener Metadatenstandards beteiligt, z.B. durch ihre Mitwirkung bei der Spezifikation des METS/MODS-Anwendungsprofils für digitalisierte Drucke, dem De-facto-Beschreibungsstandard für digitalisiertes Material in deutschen Bibliotheken.
