---
title: Associations and Alliances

aliases:
- /about-us/professional-associations
- /about-us/other-partners
- /networking/alliances

menu:
  main:
    weight: 30
    parent: vernetzung
---

## Academic Societies

Text+ collaborates with a large number of academic societies in the humanities. This ensures close contact with the diverse community and understanding of its needs. Academic societies play a central role in the [governance structure](/en/ueber-uns/struktur-und-governance/) of Text+, as they send representatives to the Text+ Plenum and nominate candidates for the election of [Coordination Committees](/en/ueber-uns/struktur-und-governance/#coordination-committees).

<!-- TODO: Anchor to coordination committees -->

{{<letter-list>}}
{{<letter letter-file="letters/LoI-AG-Rom.pdf" letter-type="Support">}}
[AG-ROM: Arbeitsgemeinschaft der romanistischen Fachverbände](http://deutscher-romanistenverband.de/interesse-an-romanistik/romanistische-fachverbaende/)

- [Balkanromanistenverband](http://balkanromanistenverband.de/)
- [Frankoromanistenverband](http://francoromanistes.de/)
- [Hispanistenverband](http://hispanistica.de/)
- [Italianistenverband](http://italianistenverband.de/)
- [Katalanistenverband](http://katalanistik.de/)
- [Lusitanistenverband](http://lusitanistenverband.de/)
- [Romanistenverband](http://deutscher-romanistenverband.de/)
  {{</letter>}}
  {{<letter letter-file="letters/LoI-DGfA.pdf" letter-type="Interest">}}
  [Deutsche Gesellschaft für Amerikastudien](https://dgfa.de/)
  {{</letter>}}
  {{<letter letter-file="letters/LOS_GfN.pdf" letter-type="Interest">}}
  [Deutsche Gesellschaft für Namenforschung](https://www.gfn.name/)
  {{</letter>}}
  {{<letter letter-file="letters/LoS-DGfPhil.pdf" letter-type="Interest">}}
  [Deutsche Gesellschaft für Philosophie](https://www.dgphil.de/)
  {{</letter>}}
  {{<letter letter-file="letters/LoI-DGfS.pdf" letter-type="Support">}}
  [Deutsche Gesellschaft für Sprachwissenschaft (DGfS)](https://dgfs.de/)
  {{</letter>}}
  {{<letter letter-file="letters/LoS-DGV.pdf" letter-type="Support">}}
  [Deutsche Gesellschaft für Volkskunde (dgv)](https://www.d-g-v.de/)
  {{</letter>}}
  {{<letter letter-file="letters/LoS-DVPW.pdf" letter-type="Support">}}
  [Deutsche Vereinigung für Politikwissenschaft (DVPW)](https://www.dvpw.de/)
  {{</letter>}}
  {{<letter letter-file="letters/LOS-Anglistenverband.pdf" letter-type="Support">}}
  [Deutscher Anglistenverband](http://www.anglistenverband.de/)
  {{</letter>}}
  {{<letter letter-file="letters/LoI-DGV.pdf" letter-type="Interest">}}
  [Deutscher Germanistenverband](https://deutscher-germanistenverband.de/)
  {{</letter>}}
  {{<letter letter-file="letters/LoS-Slavistenverband.pdf" letter-type="Support">}}
  [Deutscher Slavistenverband](http://www.slavistenverband.de/)
  {{</letter>}}
  {{<letter letter-file="letters/LoI-DHd.pdf" letter-type="Interest">}}
  [Digital Humanities im deutschsprachigen Raum (DHd)](https://dig-hum.de/)
  {{</letter>}}
  {{<letter letter-file="letters/LoI-GAP.pdf" letter-type="Interest">}}
  [Gesellschaft für analytische Philosophie](https://www.gap-im-netz.de/)
  {{</letter>}}
  {{<letter letter-file="letters/LoI-GAL.pdf" letter-type="Support">}}
  [Gesellschaft für Angewandte Linguistik](https://gal-ev.de/)
  {{</letter>}}
  {{<letter letter-file="letters/LoI-GBS.pdf" letter-type="Support">}}
  [Gesellschaft für bedrohte Sprachen](http://gbs.uni-koeln.de/wordpress/)
  {{</letter>}}
  {{<letter letter-file="letters/LoS-GGSG.pdf" letter-type="Interest">}}
  [Gesellschaft für germanistische Sprachgeschichte](https://www.germanistische-sprachgeschichte.de/)
  {{</letter>}}
  {{<letter letter-file="letters/LoS-GFM.pdf" letter-type="Support">}}
  [Gesellschaft für Musikforschung](https://www.musikforschung.de/)
  {{</letter>}}
  {{<letter letter-file="letters/LoS-GSCL.pdf" letter-type="Support">}}
  [Gesellschaft für Sprachtechnologie & Computerlinguistik (GSCL)](https://gscl.org/)
  {{</letter>}}
  {{<letter letter-file="letters/LoS_HI.pdf" letter-type="Support">}}
  [Hochschulverband Informationswissenschaft](http://www.informationswissenschaft.org/)
  {{</letter>}}
  {{<letter letter-file="letters/LoS-Mediävistenverband.pdf" letter-type="Support">}}
  [Mediävistenverband](https://www.mediaevistenverband.de/)
  {{</letter>}}
  {{<letter letter-file="letters/LOS-Akademieunion.pdf" letter-type="Support">}}
  [Union der deutschen Akademien der Wissenschaften](https://www.akademienunion.de/)
  {{</letter>}}
  {{<letter letter-file="letters/LoS-VndS.pdf" letter-type="Support">}}
  [Verein für Niederdeutsche Sprachforschung](http://www.vnds.de/)
  {{</letter>}}
  {{</letter-list>}}

## Alliances

In addition to close contact with professional associations in the German-speaking region, Text+ collaborates with overarching national and international alliances. This ensures the exchange on international topics and developments.
{{<image img="2020-06-30-Networking-Initiativen-Gremien-768x432.png" alt="Overview of the listed alliances below" />}}

{{<letter-list>}}
{{<letter letter-file="letters/LoS-ALLEA.pdf" letter-type="Support">}}
[ALLEA – European Federation of Academies of Sciences and Humanities](https://allea.org/)
{{</letter>}}
{{<letter letter-file="letters/LoI-CLARIN.pdf" letter-type="Interest">}}
[CLARIN ERIC](https://www.clarin.eu/)
{{</letter>}}
{{<letter letter-file="letters/LoI-DARIAH.pdf" letter-type="Interest">}}
[DARIAH ERIC](https://www.dariah.eu/)
{{</letter>}}
{{<letter letter-file="letters/LoS-Data-Cite.pdf" letter-type="Support">}}
[DataCite](https://datacite.org/)
{{</letter>}}
{{<letter letter-file="letters/LoS-DDB.pdf" letter-type="Interest">}}
[DDB – Deutsche Digitale Bibliothek](https://www.deutsche-digitale-bibliothek.de/)
{{</letter>}}
{{<letter letter-file="letters/LoS-Horstmann-EOSC.pdf" letter-type="Support">}}
[EOSC – European Open Science Cloud](https://www.eosc-portal.eu/)
{{</letter>}}
{{<letter letter-file="letters/LoS-Witt-ESFRI.pdf" letter-type="Support">}}
[ESFRI – European Strategy Forum on Research Infrastructures](https://www.esfri.eu/)
{{</letter>}}
{{<letter>}}
[Europeana](https://www.europeana.eu/)
{{</letter>}}
{{<letter letter-file="letters/LoS-HTRC.pdf" letter-type="Support">}}
[HathiTrust Research Center (HTRC)](https://www.hathitrust.org/htrc)
{{</letter>}}
{{<letter letter-file="letters/LoS-LIBER.pdf" letter-type="Support">}}
[LIBER – Ligue des Bibliothèques Européennes de Recherche – Association of European Research Libraries](https://libereurope.eu/)
{{</letter>}}
{{<letter letter-file="letters/LoS-OpenaAIRE.pdf" letter-type="Support">}}
[OpenAIRE](https://www.openaire.eu/)
{{</letter>}}
{{<letter letter-file="letters/LoI-OPERAS.pdf" letter-type="Interest">}}
[OPERAS – Open scholarly communication in the european research area for the social sciences and humanities](https://operas.hypotheses.org/)
{{</letter>}}
{{<letter letter-file="letters/LoS-RDA.pdf" letter-type="Interest">}}
[RDA – Research Data Alliance](https://rd-alliance.org/)
{{</letter>}}
{{</letter-list>}}

## Additional Partners

{{<letter-list>}}
{{<letter letter-file="letters/LoS-AgE.pdf" letter-type="Support">}}
[Arbeitsgemeinschaft für Germanistische Edition](https://www.ag-edition.org/)
{{</letter>}}
{{<letter letter-file="letters/LoS-BerGSAS.pdf" letter-type="Support">}}
[Berlin Graduate School of Ancient Studies (BerGSAS)](https://www.berliner-antike-kolleg.org/bergsas/index.html)
{{</letter>}}
{{<letter letter-file="letters/LoS-Bodleian-Libraries-Oxford.pdf" letter-type="Support">}}
[Bodleian Libraries, University of Oxford](https://www.bodleian.ox.ac.uk/)
{{</letter>}}
{{<letter letter-file="letters/LoS-DDB.pdf" letter-type="Commitment">}}
[Deutsche Digitale Bibliothek](https://www.deutsche-digitale-bibliothek.de/)
{{</letter>}}
{{<letter letter-file="letters/LoS-dbv-s4.pdf" letter-type="Support">}}
[Deutscher Bibliotheksverband](https://www.bibliotheksverband.de/)
{{</letter>}}
{{<letter letter-file="letters/LoS_-UB_Frankfurt_2022.pdf" letter-type="Support">}}
[Fachinformationsdienst (FID) Allgemeine und Vergleichende Literaturwissenschaft](https://www.avldigital.de/)
{{</letter>}}
{{<letter letter-file="letters/LoS-FID-AAC.pdf" letter-type="Support">}}
[Fachinformationsdienst (FID) Anglo-American Culture](https://libaac.de/home/)
{{</letter>}}
{{<letter letter-file="letters/LoS-SBB.pdf" letter-type="Support">}}
[Fachinformationsdienst (FID) Asien – Cross Asia](https://crossasia.org/)
{{</letter>}}
{{<letter letter-file="letters/LoS_FID_Benelux.pdf" letter-type="Support">}}
[Fachinformationsdienst (FID) Benelux / Low Countries Studies](https://www.fid-benelux.de/)
{{</letter>}}
{{<letter letter-file="letters/LoS-FID-BBI.pdf" letter-type="Support">}}
[Fachinformationsdienst (FID) Buch-, Bibliotheks- und Informationswissenschaft](https://katalog.fid-bbi.de/Content/about)
{{</letter>}}
{{<letter letter-file="letters/LoS_FID_Recht.pdf" letter-type="Support">}}
[Fachinformationsdienst (FID) für internationale und interdisziplinäre Rechtsforschung <intR>²](https://vifa-recht.de/)
{{</letter>}}
{{<letter letter-file="letters/LoS-FID-Germanistik.pdf" letter-type="Support">}}
[Fachinformationsdienst (FID) Germanistik](https://www.germanistik-im-netz.de/)
{{</letter>}}
{{<letter letter-file="letters/LoS_FID_Lateinamerika.pdf" letter-type="Support">}}
[Fachinformationsdienst (FID) Lateinamerika, Karibik und Latino Studies](https://fid-lateinamerika.de/)
{{</letter>}}
{{<letter letter-file="letters/LoI-FID-Linguistik.pdf" letter-type="Interest">}}
[Fachinformationsdienst (FID) Linguistik](https://www.ub.uni-frankfurt.de/projekte/fid-linguistik.html)
{{</letter>}}
{{<letter letter-file="letters/LoS-FID-Nahost.pdf" letter-type="Support">}}
[Fachinformationsdienst (FID) Nahost-, Nordafrika- und Islamstudien](https://nahost.fid-lizenzen.de/)
{{</letter>}}
{{<letter letter-file="letters/LoS-FID-Philosophie.pdf" letter-type="Support">}}
[Fachinformationsdienst (FID) Philosophie](https://philportal.de)
{{</letter>}}
{{<letter letter-file="letters/LoS-FID-Romanistik.pdf" letter-type="Support">}}
[Fachinformationsdienst (FID) Romanistik](https://www.fid-romanistik.de/startseite/)
{{</letter>}}
{{<letter letter-file="letters/LoI_DSA_Text.pdf" letter-type="Interest">}}
[Forschungszentrum Deutscher Sprachatlas](http://www.deutscher-sprachatlas.de/)
{{</letter>}}
{{<letter letter-file="letters/LoI-forText.pdf" letter-type="Interest">}}
[forTEXT – Literatur digital erforschen](https://fortext.net/)
{{</letter>}}
{{<letter letter-file="letters/LoS-FIZ-Karlsruhe.pdf" letter-type="Support">}}
[FIZ Karlsruhe – Leibniz-Institut für Informationsinfrastruktur](https://www.fiz-karlsruhe.de/)
{{</letter>}}
{{<letter letter-file="letters/LoI-DI.pdf" letter-type="Support">}}
Inter-Akademieprojekt: [Die Deutschen Inschriften des Mittelalters und der Frühen Neuzeit](http://www.inschriften.net/)
{{</letter>}}
{{<letter letter-file="letters/LoS-BBAW-IG-CIL.pdf" letter-type="Support">}}
Inter-Akademieprojekte: [Inscriptiones Graecae (IG)](http://ig.bbaw.de/) und [Corpus Inscriptionum Latinarum (CIL)](https://cil.bbaw.de/)
{{</letter>}}
{{<letter letter-file="letters/LoS-Leibniz-Association.pdf" letter-type="Support">}}
[Leibniz-Gemeinschaft](https://www.leibniz-gemeinschaft.de/)
{{</letter>}}
{{<letter letter-file="letters/LoS-IEG-Mainz.pdf" letter-type="Support">}}
[Leibniz Institut für europäische Geschichte (IEG)](https://www.ieg-mainz.de/)
{{</letter>}}
{{<letter letter-file="letters/LoS-DIPF.pdf" letter-type="Support">}}
[Leibniz-Institut für Bildungsforschung und Bildungsinformation](https://www.dipf.de/)
{{</letter>}}
{{<letter letter-file="letters/LoS-LRZ.pdf" letter-type="Commitment">}}
[Leibniz-Rechenzentrum (LRZ)](https://www.lrz.de/)
{{</letter>}}
{{<letter letter-file="letters/LoS-ZAS.pdf" letter-type="Support">}}
[Leibniz-Zentrum Allgemeine Sprachwissenschaft (ZAS)](https://www.leibniz-zas.de/de/das-zas/institut/)
{{</letter>}}
{{<letter letter-file="letters/LoS-Zuse.pdf" letter-type="Support">}}
[Zuse-Institut Berlin](https://www.zib.de/de)
{{</letter>}}
{{</letter-list>}}

### Additional Individual Partners

<!-- TODO: anchored URLs -->

{{<letter-list>}}
{{<letter letter-file="letters/LoS-Acquavella-Rauch.pdf" letter-type="Support">}}
Jun.-Prof. Dr. habil. Stefanie Acquavella-Rauch
{{</letter>}}
{{<letter letter-file="letters/LoS-Altenhoehner.pdf" letter-type="Support">}}
Reinhard Altenhöner
{{</letter>}}
{{<letter letter-file="letters/LoS-Bohnenkamp-Renken.pdf" letter-type="Support">}}
Prof. Dr. Anne Bohnenkamp-Renken
{{</letter>}}
{{<letter letter-file="letters/LoS-Bruenger-Weilandt.pdf" letter-type="Support">}}
Sabine Brünger-Weilandt
{{</letter>}}
{{<letter letter-file="letters/LoS-Degkwitz.pdf" letter-type="Support">}}
Prof. Dr. Andreas Degkwitz
{{</letter>}}
{{<letter letter-file="letters/LoC-Friedrich.pdf" letter-type="Support">}}
Prof. Dr. Michael Friedrich
{{</letter>}}
{{<letter letter-file="letters/LoC-Gloning.pdf" letter-type="Support">}}
Prof. Dr. Thomas Gloning
{{</letter>}}
{{<letter letter-file="letters/LoS-Heid.pdf" letter-type="Support">}}
Prof. Dr. phil. Ulrich Heid
{{</letter>}}
{{<letter letter-file="letters/LoS-Heyer.pdf" letter-type="Support">}}
Prof. Dr. Gerhard Heyer
{{</letter>}}
{{<letter letter-file="letters/LoC-Klemm.pdf" letter-type="Support">}}
Prof. Dr. Verena Klemm
{{</letter>}}
{{<letter letter-file="letters/LoS-Mueller-Spitzer.pdf" letter-type="Support">}}
Prof. Dr. Carolin Müller-Spitzer
{{</letter>}}
{{<letter letter-file="letters/LoC-Petras.pdf" letter-type="Support">}}
Prof. Vivien Petras, [Acting OCC Chair](/ueber-uns/textplus-governance/)
{{</letter>}}
{{<letter letter-file="letters/LoS-Pfeiffer.pdf" letter-type="Support">}}
Prof. Dr. Judith Pfeiffer, [Acting SCC Chair Editions](/ueber-uns/textplus-governance/)
{{</letter>}}
{{<letter letter-file="letters/LoC-Richter.pdf" letter-type="Support">}}
Prof. Dr. Sandra Richter, [Acting SCC Chair Collections](/ueber-uns/textplus-governance/)
{{</letter>}}
{{<letter letter-file="letters/LoS-Schneider.pdf" letter-type="Support">}}
PD Dr. habil. Roman Schneider
{{</letter>}}
{{<letter letter-file="letters/LoC-Schroeder.pdf" letter-type="Support">}}
Prof. Dr. Ingrid Schröder, [Acting SCC Chair Lexical Resources](/ueber-uns/textplus-governance/)
{{</letter>}}
{{<letter letter-file="letters/LoS-Selig.pdf" letter-type="Support">}}
Prof. Dr. Maria Selig
{{</letter>}}
{{<letter letter-file="letters/LoS-Ubl.pdf" letter-type="Support">}}
Prof. Dr. Karl Ubl
{{</letter>}}
{{<letter letter-file="letters/LoS-Wagner.pdf" letter-type="Support">}}
Prof. Dr. Petra Wagner
{{</letter>}}
{{<letter letter-file="letters/LoS-Zinsmeister.pdf" letter-type="Support">}}
Prof. Dr. Heike Zinsmeister
{{</letter>}}
{{</letter-list>}}
