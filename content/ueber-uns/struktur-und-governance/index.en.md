---
title: Structure and Governance
type: non-scrolling-toc

aliases:
- /forschungsdaten/datendomaenen/
- /ueber-uns/boards
- /ueber-uns/coordination-committees
- /ueber-uns/governance
  
menu:
  main:
    weight: 5
    parent: ueber-uns

---

# Structure of Text+

Text+ is a consortium of the National Research Data Infrastructure (NFDI) in Germany and is designed for all researchers working with text and language data in the broadest sense, including but not limited to linguistics, literary studies, philologies, including the so-called 'small disciplines,' philosophy, as well as language- and text-based research in the social sciences and political science.

## Work Areas in Text+

Text+ is divided into various task areas. The Collections, Lexical Resources, and Editions task areas represent the data domains that Text+ initially focuses on. The data within these data domains have a long tradition in humanities research and are linked to sophisticated methodological paradigms, each requiring characteristic but also interdisciplinary practices of data generation, utilization, analysis, networking, and curation. These three data domains are also fundamental for interdisciplinary research practices in hermeneutics, palaeography, genealogy, edition philology, lexicography, computer philology, and computer linguistics.

The name *Text+* conveys that this initiative focuses on typically text-based digital research data that are heterogeneous concerning language spaces (also beyond Europe) and modalities of language and writing systems. The plus sign indicates that language-based resources also encompass resources and tools for spoken language and multimodal data.

In addition to these data domains, there are the work areas of Infrastructure/Operations and Project Coordination. The Infrastructure/Operations area covers technical foundations, such as interfaces and shared technical solutions. Overall coordination addresses overarching aspects and the administration of the entire project.

## Text+ as Part of the National Research Data Infrastructure (NFDI)

Text+ operates within the NFDI. Participants in Text+ are part of all committees and working groups of the NFDI.

## Governance

At the core of Text+'s governance is the shared responsibility of infrastructure and community, as well as cooperation across disciplinary boundaries. The listed boards are tasked with continuously evaluating Text+'s portfolio of data, tools, and services and driving its development in collaboration with the community.

Text+ is represented by the Overall Coordination: [Prof. Dr. Erhard Hinrichs](https://www1.ids-mannheim.de/digspra/personal/hinrichs.html) and [Prof. Dr. Philipp Wieder](https://gwdg.de/research-education/researchgroup_wieder/).

In the NFDI Association, Text+ is represented by the elected spokesperson [Prof. Dr. Andreas Witt](https://www1.ids-mannheim.de/digspra/personal/witt.html) and the elected deputy spokesperson [Prof. Dr. Andrea Rapp](https://www.linglit.tu-darmstadt.de/institutlinglit/mitarbeitende/andrearapp/).

{{<image img="Kap.3-Governance-Organigramm-Text-768x691.png" alt="Text+-Governance-Organigram"/>}}

## Boards

The **Scientific Board** holds the scientific leadership of the consortium and decides on portfolio development.

The **Steering Committee** is responsible for implementing the work program and takes charge of the professional and financial monitoring of ongoing work. The Overall Coordination, consisting of Scientific and Operations Speakers, is the link between these committees and is responsible for consortium management and office leadership.

The leaders of the (co-)applying institutions form the **Management Group**. It supports the Steering Committee and Overall Coordination in overarching and strategic matters.

{{<team name="Scientific Board">}}
{{<team-member img="gfx/Herrmann-Berenike.jpg" url="https://ekvv.uni-bielefeld.de/pers_publ/publ/PersonDetail.jsp;jsessionid=9361A63F248C0E3485779F02FB68A18A?personId=262987169" role="Acting SCC Chair Collections" institution="Bielefeld University">}}Prof. Dr. Berenike Herrmann{{</team-member>}}
{{<team-member img="gfx/Hinrichs-Henrich_quelle-privat.jpg" url="https://www1.ids-mannheim.de/digspra/personal/hinrichs.html" role="Scientific Speaker" institution="Leibniz Institute for the German Language">}}Prof. Dr. Erhard Hinrichs{{</team-member>}}
{{<team-member img="gfx/Petras-Vivien-Quelle-privat.jpg" url="https://www.ibi.hu-berlin.de/de/ueber-uns/personen/petras" role="Acting OCC Chair" institution="Humboldt-Universität zu Berlin">}}Prof. Vivien Petras, PhD{{</team-member>}}
{{<team-member img="gfx/pfeiffer_judith_Foto-Humboldt-Stiftung-Wolfgang Hemmann.jpg" url="https://www.ioa.uni-bonn.de/isl/de/pers/pfeiffer" role="Acting SCC Chair Editions" institution="University of Bonn">}}Prof. Dr. Judith Pfeiffer{{</team-member>}}
{{<team-member img="gfx/Rapp-Andrea-Katrin-Binner.jpg" url="https://www.linglit.tu-darmstadt.de/institutlinglit/mitarbeitende/andrearapp/index.de.jsp" role="Scientific Vice Speaker" institution="Technical University of Darmstadt">}}Prof. Dr. Andrea Rapp{{</team-member>}}
{{<team-member img="gfx/Schroder-Ingrid-Quelle-privat.jpg" url="https://www.slm.uni-hamburg.de/germanistik/personen/schroeder.html" role="Acting SCC Chair Lexical Resources" institution="University of Hamburg">}}Prof. Dr. Ingrid Schröder{{</team-member>}}
{{<team-member img="gfx/Wieder-Philipp.jpg" url="https://gwdg.de/research-education/researchgroup_wieder/" role="Operations Speaker" institution="Georg-August University of Göttingen, Lower Saxony State and University Library">}}Prof. Dr. Philipp Wieder{{</team-member>}}
{{<team-member img="gfx/Teich-Elke-Quelle-privat.jpg" url="https://www.uni-saarland.de/lehrstuhl/teich.html" role="Scientific Vice Speaker" institution="University of Saarland">}}Prof. Dr. Elke Teich{{</team-member>}}
{{</team>}}

{{<team name="Steering Group" theme="white">}}
{{<team-member img="gfx/Geyken-Alexander-Quelle-privat.jpg" url="https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/geyken-alexander" role="Data Domain Speaker Lexical Resources" institution="Berlin-Brandenburg Academy of Sciences and Humanities">}}PD Dr. Alexander Geyken{{</team-member>}}
{{<team-member img="gfx/Hinrichs-Henrich_quelle-privat.jpg" url="https://www1.ids-mannheim.de/digspra/personal/hinrichs.html" role="Scientific Speaker" institution="Leibniz Institute for the German Language">}}Prof. Dr. Erhard Hinrichs{{</team-member>}}
{{<team-member img="gfx/Leinen-Peter-Quelle-privat.jpeg" url="https://www.dnb.de/DE/Ueber-uns/Organisation/organisation_node.html#doc57844bodyText4" role="Data Domain Speaker Collections" institution="German National Library">}} Dr. Peter Leinen{{</team-member>}}
{{<team-member img="gfx/Speer-Andreas-Quelle-privat.jpg" url="https://thomasinstitut.uni-koeln.de/mitarbeiterinnen/andreas-speer" role="Data Domain Speaker Editions" institution="North Rhine-Westphalian Academy of Sciences and Arts">}}Prof. Dr. Andreas Speer{{</team-member>}}
{{<team-member img="gfx/Wieder-Philipp.jpg" url="https://gwdg.de/research-education/researchgroup_wieder/" role="Operations Speaker" institution="Georg-August University Göttingen, Lower Saxony State and University Library">}}Prof. Dr. Philipp Wieder{{</team-member>}}
{{</team>}}

{{<team name="Management Group">}}
{{<team-member img="gfx/Dusch-Christine-FotoagenturRuhr-BettinaEngel-Albustin.jpg" url="https://www.awk.nrw/akademie/akademieverwaltung" role="Secretary-General of the North Rhine-Westphalian Academy of Sciences and Arts" institution="">}}Christiane Dusch{{</team-member>}}
{{<team-member img="gfx/Horstmann-Wolfram-SUB.jpg" url="https://www.sub.uni-goettingen.de/kontakt/direktion/dr-wolfram-horstmann/" role="Director of the Lower Saxony State and University Library Göttingen" institution="">}}Prof. Dr. Wolfram Horstmann{{</team-member>}}
{{<team-member img="gfx/Lobin-Henning-Quelle-privat-1.jpg" url="https://www.ids-mannheim.de/zfo/personal/lobin" role="Scientific Director of the Leibniz Institute for the German Language" institution="">}}Prof. Dr. Henning Lobin{{</team-member>}}
{{<team-member img="gfx/Markschies_Christoph_Pablo-Castagnola.jpg" url="https://www.bbaw.de/die-akademie/bbaw-mitglieder/mitglied-christoph-markschies" role="President of the Berlin-Brandenburg Academy of Sciences and Humanities" institution="">}}Prof. Dr. Dr. h. c. mult. Christoph Markschies{{</team-member>}}
{{<team-member img="gfx/Scholze-Frank.jpg" url="https://www.dnb.de/DE/Ueber-uns/Organisation/organisation_node.html;jsessionid=529AE39F5AD2ED2E53DEB67910C7C2FE.internet282#doc57844bodyText1" role="Director-General of the German National Library" institution="">}}Frank Scholze{{</team-member>}}
{{</team>}}

## Coordination Committees

The Coordination Committees consist of three different Scientific Coordination Committees, each responsible for one of the data domains (Collections, Editions, Lexical Resources), and an Operations Coordination Committee. Their task is to continuously evaluate and expand the portfolio of data, tools, and services. The Coordination Committees consist of experts from the respective (subject) domains.

{{<team name="Scientific Coordination Committee: Collections">}}
{{<team-member img="gfx/Herrmann-Berenike.jpg" url="https://ekvv.uni-bielefeld.de/pers_publ/publ/PersonDetail.jsp;jsessionid=9361A63F248C0E3485779F02FB68A18A?personId=262987169" role="Acting SCC Chair Collections" institution="University of Bielefeld">}}Prof. Dr. Berenike Herrmann{{</team-member>}}
{{<team-member img="gfx/Leinen-Peter-Quelle-privat.jpeg" url="https://www.dnb.de/DE/Ueber-uns/Organisation/organisation_node.html#doc57844bodyText4" role="Data Domain Speaker Collections" institution="German National Library">}} Dr. Peter Leinen{{</team-member>}}
{{<team-member img="gfx/Bender_Michael.jpg" url="https://www.linglit.tu-darmstadt.de/institutlinglit/mitarbeitende/michaelbender/index.de.jsp" role="SCC Collections Member" institution="Technical University Darmstadt">}} Dr. Michael Bender{{</team-member>}}
{{<team-member img="gfx/Eisler-Cornelia_Stefan-Wilde.jpg" url="https://www.bkge.de/BKGE/MitarbeiterInnen/Wissenschaftlich/Eisler/" role="SCC Collections Member" institution="Federal Institute for Culture and History of the Germans in Eastern Europe">}}Dr. Cornelia Eisler{{</team-member>}}
{{<team-member img="gfx/kurzawe-daniel.jpg" url="https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/daniel-kurzawe/" role="SCC Collections Member" institution="Lower Saxony State and University Library Göttingen">}}Dr. Daniel Kurzawe{{</team-member>}}
{{<team-member img="gfx/lingnau-anna.jpg" url="https://www.hab.de/author/dr-anna-lingnau/" role="SCC Collections Member" institution="FID Book, Library, and Information Science at the Herzog August Library Wolfenbüttel">}}Dr. Anna Lingnau{{</team-member>}}
{{<team-member img="gfx/Meier-Vieracker-Simon.jpg" url="https://tu-dresden.de/gsw/slk/germanistik/al/die-professur/inhaber" role="SCC Collections Member" institution="Technical University Dresden">}}Prof. Dr. Simon Meier-Vieracker{{</team-member>}}
{{<team-member img="" url="https://www.slawistik.hu-berlin.de/de/member/meyerrol" role="SCC Collections Member" institution="Humboldt-Universität zu Berlin">}}Prof. Dr. Roland Meyer{{</team-member>}}
{{<team-member img="gfx/Plaksin-Anna.jpg" url="https://www.uni-paderborn.de/person/102981" role="SCC Collections Member" institution="University of Paderborn">}}Prof. Dr. Anna Plaksin{{</team-member>}}
{{<team-member img="gfx/Reiners-Selbach-Stefan.jpg" url="https://www.philo.hhu.de/fakultaet-1/dekanat/dekanatsbuero/koodination-digital-humanities" role="SCC Collections Member" institution="Heinrich-Heine-University Düsseldorf">}}Stefan Reiners-Selbach{{</team-member>}}
{{<team-member img="gfx/Seifart-Frank.jpg" url="https://www.leibniz-zas.de/de/personen/details/seifart-frank/frank-seifart" role="SCC Collections Member" institution="Leibniz Centre for General Linguistics, Berlin">}}PD Dr. Frank Seifart{{</team-member>}}
{{<team-member img="gfx/Wagner-Petra.jpg" url="https://www.uni-bielefeld.de/fakultaeten/linguistik-literaturwissenschaft/personen/petra-wagner/" role="SCC Collections Member" institution="University of Bielefeld">}}Prof. Dr. Petra Wagner{{</team-member>}}
{{<team-member img="gfx/Witt-Andreas.jpg" url="https://www.ids-mannheim.de/digspra/personal/witt/" role="SCC Collections Member" institution="Leibniz Institute for the German Language">}}Prof. Dr. Andreas Witt{{</team-member>}}
{{</team>}}

{{<team name="Scientific Coordination Committee: Editions" theme="white">}}
{{<team-member img="gfx/pfeiffer_judith_Foto-Humboldt-Stiftung-Wolfgang Hemmann.jpg" url="https://www.ioa.uni-bonn.de/isl/de/pers/pfeiffer" role="Acting SCC Chair Editions" institution="University of Bonn">}}Prof. Dr. Judith Pfeiffer{{</team-member>}}
{{<team-member img="gfx/Roeder-Torsten.jpg" url="https://www.uni-wuerzburg.de/zpd/zentrum/team/roeder-torsten/" role="Vice Chair SCC Editions" institution="Julius-Maximilians-University Würzburg">}}Dr. Torsten Roeder{{</team-member>}}
{{<team-member img="gfx/Speer-Andreas-Quelle-privat.jpg" url="https://thomasinstitut.uni-koeln.de/mitarbeiterinnen/andreas-speer" role="Data Domain Speaker Editions" institution="North Rhine-Westphalian Academy of Sciences and Arts">}}Prof. Dr. Andreas Speer{{</team-member>}}
{{<team-member img="gfx/Acquavella-Rauch-Stefanie.jpg" url="https://www.musikwissenschaft.uni-mainz.de/personen/prof-dr-stefanie-acquavella-rauch/" role="SCC Editions Member" institution="Johannes Gutenberg University Mainz">}}Prof. Dr. Stefanie Acquavella-Rauch{{</team-member>}}
{{<team-member img="gfx/Baresch_Ariadne.jpg" url="https://www.uni-trier.de/universitaet/fachbereiche-faecher/fachbereich-ii/faecher/computerlinguistik-und-digital-humanities/digital-humanities/team/ariadne-baresch" role="SCC Editions Member" institution="University of Trier">}}Ariadne Baresch, M.A.{{</team-member>}}
{{<team-member img="" url="https://www.adwmainz.de/mitglieder/profil/prof-dr-anne-bohnenkamp-renken.html" role="SCC Editions Member" institution="Frankfurter Goethe-Haus">}}Prof. Dr. Anne Bohnenkamp-Renken{{</team-member>}}
{{<team-member img="gfx/Henny-Krahmer_Ulrike.jpg" url="https://www.germanistik.uni-rostock.de/lehrende/professorinnen-und-professoren/jun-prof-dr-ulrike-henny-krahmer/" role="SCC Editions Member" institution="University of Rostock">}}Jun.-Prof. Ulrike Henny-Krahmer{{</team-member>}}
{{<team-member img="gfx/Horstmann-Jan.jpg" url="https://www.ulb.uni-muenster.de/~personal/horstmann/" role="SCC Editions Member" institution="University of Münster">}}Dr. Jan Horstmann{{</team-member>}}
{{<team-member img="gfx/sahle-patrick.jpg" url="https://www.geschichte.uni-wuppertal.de/de/lehrgebiete/digital-humanities/detail/sahle/" role="SCC Editions Member" institution="University of Wuppertal">}}Prof. Dr. Patrick Sahle{{</team-member>}}
{{<team-member img="gfx/Vetter_Angila.jpg" url="https://www.ub.uni-kiel.de/de/kontakt/angila-vetter" role="SCC Editions Member" institution="University of Augsburg">}}Dr. Angila Vetter{{</team-member>}}
{{<team-member img="gfx/Viehhauser-Gabriel.jpeg" url="https://www.uni-stuttgart.de/universitaet/organisation/personen/Viehhauser/" role="SCC Editions Member" institution="University of Stuttgart">}}Prof. Dr. Gabriel Viehhauser{{</team-member>}}
{{</team>}}

{{<team name="Scientific Coordination Committee: Lexical Resources">}}
{{<team-member img="gfx/Schroder-Ingrid-Quelle-privat.jpg" url="https://www.slm.uni-hamburg.de/germanistik/personen/schroeder.html" role="Acting SCC Chair Lexical Resources" institution="University of Hamburg">}}Prof. Dr. Ingrid Schröder{{</team-member>}}
{{<team-member img="gfx/Geyken-Alexander-Quelle-privat.jpg" url="https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/geyken-alexander" role="Data Domain Speaker Lexical Resources" institution="Berlin-Brandenburg Academy of Sciences and Humanities">}}PD Dr. Alexander Geyken{{</team-member>}}
{{<team-member img="" url="https://www.linglit.tu-darmstadt.de/institutlinglit/mitarbeitende/sabinebartsch/index.de.jsp" role="SCC Lexical Resources Member" institution="Technical University Darmstadt">}}Dr. Sabine Bartsch{{</team-member>}}
{{<team-member img="gfx/Heid-Ulrich.jpg" url="https://www.uni-hildesheim.de/fb3/institute/iwist/mitglieder/heid/" role="SCC Lexical Resources Member" institution="University of Hildesheim">}}Prof. Dr. Ulrich Heid{{</team-member>}}
{{<team-member img="gfx/Nowak-Jessica.png" url="https://www.germanistik.uni-mainz.de/abteilungen/historische-sprachwissenschaft-des-deutschen/jessica-nowak-m-a/" role="SCC Lexical Resources Member" institution="Johannes Gutenberg University Mainz">}}Jun.-Prof. Dr. Jessica Nowak{{</team-member>}}
{{<team-member img="gfx/Osswald-Rainer.jpg" url="https://user.phil.hhu.de/osswald/" role="SCC Lexical Resources Member" institution="Heinrich-Heine-University Düsseldorf">}}Dr. Rainer Osswald{{</team-member>}}
{{<team-member img="" url="https://www.katalog.uu.se/profile/?id=N18-2697" role="SCC Lexical Resources Member" institution="University of Uppsala">}}PD Dr. Michael Prinz{{</team-member>}}
{{<team-member img="" url="https://www.geschkult.fu-berlin.de/e/rod/Team-und-Fellows/assozierte-Forscher/Richter.html" role="SCC Lexical Resources Member" institution="Free University Berlin">}}Prof. Dr. Tonio Sebastian Richter{{</team-member>}}
{{<team-member img="gfx/Selig-Maria.jpg" url="https://www.uni-regensburg.de/sprache-literatur-kultur/romanistik/sprachwissenschaft/selig/" role="SCC Lexical Resources Member" institution="Bavarian Academy of Sciences">}}Prof. Dr. Maria Selig{{</team-member>}}
{{<team-member img="" url="https://www.slm.uni-hamburg.de/germanistik/personen/zinsmeister.html" role="SCC Lexical Resources Member" institution="University of Hamburg">}}Prof. Dr. Heike Zinsmeister{{</team-member>}}
{{</team>}}

{{<team name="Operations Coordination Committee" theme="white">}}
{{<team-member img="gfx/Petras-Vivien-Quelle-privat.jpg" url="https://www.ibi.hu-berlin.de/de/ueber-uns/personen/petras" role="Acting OCC Chair" institution="Humboldt-Universität zu Berlin">}}Prof. Vivien Petras, PhD{{</team-member>}}
{{<team-member img="gfx/Wieder-Philipp.jpg" url="https://gwdg.de/research-education/researchgroup_wieder/" role="Operations Speaker" institution="Georg-August-University Göttingen, Lower Saxony State and University Library">}}Prof. Dr. Philipp Wieder{{</team-member>}}
{{<team-member img="gfx/Henrich_Andreas-Quelle-privat.jpg" url="https://www.uni-bamberg.de/minf/team/henrich/" role="Operation Vice Speaker" institution="Otto-Friedrich-University Bamberg">}}Prof. Dr. Andreas Henrich{{</team-member>}}
{{<team-member img="gfx/Altenhoner-Reinhard.jpg" url="https://staatsbibliothek-berlin.de/die-staatsbibliothek/abteilungen/generaldirektion/info-sv" role="OCC Member" institution="Berlin State Library Prussian Cultural Heritage">}}Reinhard Altenhöner{{</team-member>}}
{{<team-member img="gfx/Degkwitz-Andreas.jpg" url="https://www.ub.hu-berlin.de/de/ueber-uns/kontakt/ansprechpartner/dr.-andreas-degkwitz" role="OCC Member" institution="University Library of Humboldt-Universität zu Berlin">}}Prof. Dr. Andreas Degkwitz{{</team-member>}}
{{<team-member img="gfx/eggert_eric.jpg" url="https://thomasinstitut.uni-koeln.de/mitarbeiterinnen/eric-eggert" role="OCC Member" institution="University of Cologne">}}Eric Eggert{{</team-member>}}
{{<team-member img="gfx/Gerstenberg-Annette.jpg" url="https://www.uni-potsdam.de/de/romanistik-gerstenberg/unterseiten/equipe/das-team/annette-gerstenberg-prof-dr" role="OCC Member" institution="University of Potsdam">}}Prof. Dr. Annette Gerstenberg{{</team-member>}}
{{<team-member img="gfx/Herrmann-Sebastian.jpg" url="https://www.philol.uni-leipzig.de/en/institute-for-american-studies/institute/faculty/sebastian-m-herrmann" role="OCC Member" institution="University of Leipzig">}}Dr. Sebastian Herrmann{{</team-member>}}
{{<team-member img="gfx/Heyer-Gerhard.jpg" url="https://www.saw-leipzig.de/de/mitarbeiter/heyerg" role="OCC Member" institution="Saxon Academy of Sciences and Humanities in Leipzig">}}Prof. Dr. Gerhard Heyer{{</team-member>}}
{{<team-member img="gfx/Razum-Matthias.png" url="https://www.fiz-karlsruhe.de/de/ueber-uns/ueber-uns#management" role="OCC Member" institution="FIZ Karlsruhe – Leibniz Institute for Information Infrastructure">}}Matthias Razum{{</team-member>}}
{{<team-member img="gfx/Renner-Westermann_Heike.jpg" url="https://www.linguistik.de/de/about/team/" role="OCC Member" institution="University of Frankfurt">}}Heike Renner-Westermann{{</team-member>}}
{{<team-member img="gfx/Stein-Regine_SUB.jpg" url="https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/regine-stein/" role="OCC Member" institution="Niedersächsische Staats- und Universitätsbibliothek Göttingen">}}Regine Stein{{</team-member>}}
{{<team-member img="gfx/tepe-markus.jpeg" url="https://uol.de/markus-tepe" role="OCC Member" institution="Carl von Ossietzky University of Oldenburg">}}Prof. Dr. Markus Tepe{{</team-member>}}
{{<team-member img="gfx/Wieneke-Lars.jpg" url="https://www.c2dh.uni.lu/de/people/lars-wieneke" role="OCC Member" institution="University of Luxembourg">}}Dr. Lars Wieneke{{</team-member>}}
{{</team>}}