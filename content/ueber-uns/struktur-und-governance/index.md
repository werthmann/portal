---
title: Struktur und Governance
type: non-scrolling-toc

aliases:
- /forschungsdaten/datendomaenen/
- /ueber-uns/boards
- /ueber-uns/coordination-committees
- /ueber-uns/governance

menu:
  main:
    weight: 5
    parent: ueber-uns

---

# Aufbau von Text+

Text+ ist ein Konsortium der Nationalen Forschungsdateninfrastruktur (NFDI) in Deutschland und richtet sich an alle Forschende, die mit Text- und Sprachdaten im weitesten Sinn arbeiten, einschließlich, aber nicht beschränkt auf Linguistik, Literaturwissenschaft, Philologien auch der sog. ‚Kleinen Fächer‘, Philosophie sowie sprach- und textbasierte Forschung in den Sozialwissenschaften und der Politikwissenschaft.

##  Arbeitsbereiche in Text+

Text+ gliedert sich in verschiedene Arbeitsbereiche. Die Arbeitsbereiche Collections, lexikalische Ressourcen und Editionen stellen die Datendomänen dar, auf die Text+ sich zunächst konzentriert. Die Daten, mit denen diese Datendomänen  arbeiten, haben eine lange Tradition in der geisteswissenschaftlichen Forschung. Sie sind mit ausgereiften methodologischen Paradigmen verknüpft, die jeweils charakteristische, aber auch bereichsübergreifende Praktiken der Datenerzeugung, -nutzung, -analyse, -vernetzung und -kuratierung erfordern. Die drei Datendomänen sind außerdem grundlegend für interdisziplinäre Forschungspraktiken der Hermeneutik, Paläographie, Genealogie, Editionsphilologie, Lexikographie und Computerphilologie sowie Computerlinguistik.

Der Name *Text+* soll vermitteln, dass sich diese Initiative auf typischerweise textbasierte digitale Forschungsdaten konzentriert, die bzgl. Sprachräumen (auch über Europa hinaus) und Modalitäten von Sprache und Schriftsystemen heterogen sind; das Plus-Zeichen weist darauf hin, dass sprachbasierte Ressourcen auch Ressourcen und Werkzeuge für gesprochene Sprache und für multimodale Daten umfassen.

Neben diesen Datendomänen gibt es den Arbeitsbereich Infrastruktur/Operations und der Bereich der Projektkoordination. Im Bereich Infrastruktur/Operations werden technische Grundlagen behandelt, z. B. Schnittstellen und gemeinsame technische Lösungen. Die Gesamtkoordination adressiert die übergreifenden Aspekte und die Verwaltung des Gesamtprojekts.

## Text+ als Teil der Nationalen Forschungsdateninfrastruktur (NFDI)

Text+ agiert innerhalb der NFDI. Beteiligte von Text+ sind Teil aller Gremien und Arbeitsgruppen der NFDI.

## Governance

Im Zentrum der Governance von Text+ steht die gemeinsame Verantwortung von Infrastruktur und Community sowie die Kooperation über Disziplinengrenzen hinweg. Die hier aufgeführten Boards haben die Aufgabe, das Text+-Portfolio an Daten, Werkzeugen und Diensten kontinuierlich zu evaluieren und seine Weiterentwicklung gemeinsam mit der Community voranzutreiben.

Vertreten wird Text+ durch die Gesamtkoordination: [Prof. Dr. Erhard Hinrichs](https://www1.ids-mannheim.de/digspra/personal/hinrichs.html) und [Prof. Dr. Philipp Wieder](https://gwdg.de/research-education/researchgroup_wieder/).

Im NFDI-Verein wird Text+ durch den gewählten Sprecher [Prof. Dr. Andreas Witt](https://www1.ids-mannheim.de/digspra/personal/witt.html) und die gewählte stellvertretende Sprecherin [Prof. Dr. Andrea Rapp](https://www.linglit.tu-darmstadt.de/institutlinglit/mitarbeitende/andrearapp/) repräsentiert.

{{<image img="Kap.3-Governance-Organigramm-Text-768x691.png" alt="Text+-Governance-Organigramm"/>}}

## Boards

Das **Scientific Board** hat die wissenschaftliche Leitung des Konsortiums inne und entscheidet über die Portfolio-Entwicklung.

Die **Steuerungsgruppe** ist verantwortlich für die Umsetzung des Arbeitsprogramms und übernimmt das fachliche und finanzielle Monitoring der laufenden Arbeiten. Bindeglied dieser Gremien ist die Gesamtkoordination, bestehend aus Scientific und Operations Speaker. Ihr obliegt das Management des Konsortiums und die Office-Leitung.

Die Leitenden der (mit-)antragstellenden Institutionen bilden die **Leitungsgruppe**. Sie unterstützt die Steuerungsgruppe sowie die Gesamtkoordination in übergreifenden und strategischen Fragen.

{{<team name="Scientific Board">}}
{{<team-member img="gfx/Herrmann-Berenike.jpg" url="https://ekvv.uni-bielefeld.de/pers_publ/publ/PersonDetail.jsp;jsessionid=9361A63F248C0E3485779F02FB68A18A?personId=262987169" role="Acting SCC Chair Collections" institution="Universität Bielefeld">}}Prof. Dr. Berenike Herrmann{{</team-member>}}
{{<team-member img="gfx/Hinrichs-Henrich_quelle-privat.jpg" url="https://www1.ids-mannheim.de/digspra/personal/hinrichs.html" role="Scientific Speaker" institution="Leibniz-Institut für Deutsche Sprache">}}Prof. Dr. Erhard Hinrichs{{</team-member>}}
{{<team-member img="gfx/Petras-Vivien-Quelle-privat.jpg" url="https://www.ibi.hu-berlin.de/de/ueber-uns/personen/petras" role="Acting OCC Chair" institution="Humboldt-Universität zu Berlin">}}Prof. Vivien Petras, PhD{{</team-member>}}
{{<team-member img="gfx/pfeiffer_judith_Foto-Humboldt-Stiftung-Wolfgang Hemmann.jpg" url="https://www.ioa.uni-bonn.de/isl/de/pers/pfeiffer" role="Acting SCC Chair Editions" institution="Universität Bonn">}}Prof. Dr. Judith Pfeiffer{{</team-member>}}
{{<team-member img="gfx/Rapp-Andrea-Katrin-Binner.jpg" url="https://www.linglit.tu-darmstadt.de/institutlinglit/mitarbeitende/andrearapp/index.de.jsp" role="Scientific Vice Speaker" institution="Technische Universität Darmstadt">}}Prof. Dr. Andrea Rapp{{</team-member>}}
{{<team-member img="gfx/Schroder-Ingrid-Quelle-privat.jpg" url="https://www.slm.uni-hamburg.de/germanistik/personen/schroeder.html" role="Acting SCC Chair Lexical Ressources" institution="Universität Hamburg">}}Prof. Dr. Ingrid Schröder{{</team-member>}}
{{<team-member img="gfx/Wieder-Philipp.jpg" url="https://gwdg.de/research-education/researchgroup_wieder/" role="Operations Speaker" institution="Georg-August-Universität Göttingen, Niedersächsische Staats- und Universitätsbibliothek">}}Prof. Dr. Philipp Wieder{{</team-member>}}
{{<team-member img="gfx/Teich-Elke-Quelle-privat.jpg" url="https://www.uni-saarland.de/lehrstuhl/teich.html" role="Scientific Vice Speaker" institution="Universität des Saarlandes">}}Prof. Dr. Elke Teich{{</team-member>}}
{{</team>}}

{{<team name="Steuerungsgruppe" theme="white">}}
{{<team-member img="gfx/Geyken-Alexander-Quelle-privat.jpg" url="https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/geyken-alexander" role="Data Domain Speaker Lexical Resources" institution="Berlin-Brandenburgische Akademie der Wissenschaften">}}PD Dr. Alexander Geyken{{</team-member>}}
{{<team-member img="gfx/Hinrichs-Henrich_quelle-privat.jpg" url="https://www1.ids-mannheim.de/digspra/personal/hinrichs.html" role="Scientific Speaker" institution="Leibniz-Institut für Deutsche Sprache">}}Prof. Dr. Erhard Hinrichs{{</team-member>}}
{{<team-member img="gfx/Leinen-Peter-Quelle-privat.jpeg" url="https://www.dnb.de/DE/Ueber-uns/Organisation/organisation_node.html#doc57844bodyText4" role="Data Domain Speaker Collections" institution="Deutsche Nationalbibliothek">}} Dr. Peter Leinen{{</team-member>}}
{{<team-member img="gfx/Speer-Andreas-Quelle-privat.jpg" url="https://thomasinstitut.uni-koeln.de/mitarbeiterinnen/andreas-speer" role="Data Domain Speaker Editions" institution="Nordrhein-Westfälische Akademie der Wissenschaften und der Künste">}}Prof. Dr. Andreas Speer{{</team-member>}}
{{<team-member img="gfx/Wieder-Philipp.jpg" url="https://gwdg.de/research-education/researchgroup_wieder/" role="Operations Speaker" institution="Georg-August-Universität Göttingen, Niedersächsische Staats- und Universitätsbibliothek">}}Prof. Dr. Philipp Wieder{{</team-member>}}
{{</team>}}

{{<team name="Leitungsgruppe">}}
{{<team-member img="gfx/Dusch-Christine-FotoagenturRuhr-BettinaEngel-Albustin.jpg" url="https://www.awk.nrw/akademie/akademieverwaltung" role="Generalsekretärin der Nordrhein-Westfälischen Akademie der Wissenschaften und der Künste" institution="">}}Christiane Dusch{{</team-member>}}
{{<team-member img="gfx/Horstmann-Wolfram-SUB.jpg" url="https://www.sub.uni-goettingen.de/kontakt/direktion/dr-wolfram-horstmann/" role="Direktor der Niedersächsischen Staats- und Universitätsbibliothek Göttingen" institution="">}}Prof. Dr. Wolfram Horstmann{{</team-member>}}
{{<team-member img="gfx/Lobin-Henning-Quelle-privat-1.jpg" url="https://www.ids-mannheim.de/zfo/personal/lobin" role="Wissenschaftlicher Direktor des Leibniz-Instituts für Deutsche Sprache" institution="">}}Prof. Dr. Henning Lobin{{</team-member>}}
{{<team-member img="gfx/Markschies_Christoph_Pablo-Castagnola.jpg" url="https://www.bbaw.de/die-akademie/bbaw-mitglieder/mitglied-christoph-markschies" role="Präsident der Berlin-Brandenburgischen Akademie der Wissenschaften" institution="">}}Prof. Dr. Dr. h. c. mult. Christoph Markschies{{</team-member>}}
{{<team-member img="gfx/Scholze-Frank.jpg" url="https://www.dnb.de/DE/Ueber-uns/Organisation/organisation_node.html;jsessionid=529AE39F5AD2ED2E53DEB67910C7C2FE.internet282#doc57844bodyText1" role="Generaldirektor der Deutschen Nationalbibliothek" institution="">}}Frank Scholze{{</team-member>}}
{{</team>}}

## Coordination Committees

Die Coordination Committees setzen sich aus drei verschiedenen Scientific Coordination Committees, die jeweils für eine der Datendomänen (Collections, Editions, Lexical Resources) zuständig sind, und einem Operations Coordination Committee zusammen. Ihre Aufgabe ist es, kontinuierlich das Portfolio an Daten, Werkzeugen und Services zu evaluieren und zu erweitern. Die Coordination Committees setzen sich aus Expertinnen und Experten der jeweiligen (Fach-)Domänen zusammen.

{{<team name="Scientific Coordination Committee: Collections">}}
{{<team-member img="gfx/Herrmann-Berenike.jpg" url="https://ekvv.uni-bielefeld.de/pers_publ/publ/PersonDetail.jsp;jsessionid=9361A63F248C0E3485779F02FB68A18A?personId=262987169" role="Acting SCC Chair Collections" institution="Universität Bielefeld">}}Prof. Dr. Berenike Herrmann{{</team-member>}}
{{<team-member img="gfx/Leinen-Peter-Quelle-privat.jpeg" url="https://www.dnb.de/DE/Ueber-uns/Organisation/organisation_node.html#doc57844bodyText4" role="Data Domain Speaker Collections" institution="Deutsche Nationalbibliothek">}} Dr. Peter Leinen{{</team-member>}}
{{<team-member img="gfx/Bender_Michael.jpg" url="https://www.linglit.tu-darmstadt.de/institutlinglit/mitarbeitende/michaelbender/index.de.jsp" role="SCC Collections Member" institution="Technische Universität Darmstadt">}} Dr. Michael Bender{{</team-member>}}
{{<team-member img="gfx/Eisler-Cornelia_Stefan-Wilde.jpg" url="https://www.bkge.de/BKGE/MitarbeiterInnen/Wissenschaftlich/Eisler/" role="SCC Collections Member" institution="Bundesinstitut für Kultur und Geschichte der Deutschen im östlichen Europa">}}Dr. Cornelia Eisler{{</team-member>}}
{{<team-member img="gfx/kurzawe-daniel.jpg" url="https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/daniel-kurzawe/" role="SCC Collections Member" institution="Niedersächsische Staats- und Universitätsbibliothek Göttingen">}}Dr. Daniel Kurzawe{{</team-member>}}
{{<team-member img="gfx/lingnau-anna.jpg" url="https://www.hab.de/author/dr-anna-lingnau/" role="SCC Collections Member" institution="FID Buch-, Bibliotheks- und Informationswissenschaft an der Herzog August Bibliothek Wolfenbüttel">}}Dr. Anna Lingnau{{</team-member>}}
{{<team-member img="gfx/Meier-Vieracker-Simon.jpg" url="https://tu-dresden.de/gsw/slk/germanistik/al/die-professur/inhaber" role="SCC Collections Member" institution="Technische Universität Dresden">}}Prof. Dr. Simon Meier-Vieracker{{</team-member>}}
{{<team-member img="" url="https://www.slawistik.hu-berlin.de/de/member/meyerrol" role="SCC Collections Member" institution="Humboldt-Universität zu Berlin">}}Prof. Dr. Roland Meyer{{</team-member>}}
{{<team-member img="gfx/Plaksin-Anna.jpg" url="https://www.uni-paderborn.de/person/102981" role="SCC Collections Member" institution="Universität Paderborn">}}Prof. Dr. Anna Plaksin{{</team-member>}}
{{<team-member img="gfx/Reiners-Selbach-Stefan.jpg" url="https://www.philo.hhu.de/fakultaet-1/dekanat/dekanatsbuero/koodination-digital-humanities" role="SCC Collections Member" institution="Heinrich-Heine-Universität Düsseldorf">}}Stefan Reiners-Selbach{{</team-member>}}
{{<team-member img="gfx/Seifart-Frank.jpg" url="https://www.leibniz-zas.de/de/personen/details/seifart-frank/frank-seifart" role="SCC Collections Member" institution="Leibniz-Zentrum Allgemeine Sprachwissenschaft Berlin">}}PD Dr. Frank Seifart{{</team-member>}}
{{<team-member img="gfx/Wagner-Petra.jpg" url="https://www.uni-bielefeld.de/fakultaeten/linguistik-literaturwissenschaft/personen/petra-wagner/" role="SCC Collections Member" institution="Universität Bielefeld">}}Prof. Dr. Petra Wagner{{</team-member>}}
{{<team-member img="gfx/Witt-Andreas.jpg" url="https://www.ids-mannheim.de/digspra/personal/witt/" role="SCC Collections Member" institution="Leibniz-Institut für Deutsche Sprache">}}Prof. Dr. Andreas Witt{{</team-member>}}
{{</team>}}

{{<team name="Scientific Coordination Committee: Editions" theme="white">}}
{{<team-member img="gfx/pfeiffer_judith_Foto-Humboldt-Stiftung-Wolfgang Hemmann.jpg" url="https://www.ioa.uni-bonn.de/isl/de/pers/pfeiffer" role="Acting SCC Chair Editions" institution="Universität Bonn">}}Prof. Dr. Judith Pfeiffer{{</team-member>}}
{{<team-member img="gfx/Roeder-Torsten.jpg" url="https://www.uni-wuerzburg.de/zpd/zentrum/team/roeder-torsten/" role="Vice Chair SCC Editions" institution="Julius-Maximilians-Universität Würzburg">}}Dr. Torsten Roeder{{</team-member>}}
{{<team-member img="gfx/Speer-Andreas-Quelle-privat.jpg" url="https://thomasinstitut.uni-koeln.de/mitarbeiterinnen/andreas-speer" role="Data Domain Speaker Editions" institution="Nordrhein-Westfälische Akademie der Wissenschaften und der Künste">}}Prof. Dr. Andreas Speer{{</team-member>}}
{{<team-member img="gfx/Acquavella-Rauch-Stefanie.jpg" url="https://www.musikwissenschaft.uni-mainz.de/personen/prof-dr-stefanie-acquavella-rauch/" role="SCC Editions Member" institution="Johannes Gutenberg-Universität Mainz">}}Prof. Dr. Stefanie Acquavella-Rauch{{</team-member>}}
{{<team-member img="gfx/Baresch_Ariadne.jpg" url="https://www.uni-trier.de/universitaet/fachbereiche-faecher/fachbereich-ii/faecher/computerlinguistik-und-digital-humanities/digital-humanities/team/ariadne-baresch" role="SCC Editions Member" institution="Universität Trier">}}Ariadne Baresch, M.A.{{</team-member>}}
{{<team-member img="" url="https://www.adwmainz.de/mitglieder/profil/prof-dr-anne-bohnenkamp-renken.html" role="SCC Editions Member" institution="Frankfurter Goethe-Haus">}}Prof. Dr. Anne Bohnenkamp-Renken{{</team-member>}}
{{<team-member img="gfx/Henny-Krahmer_Ulrike.jpg" url="https://www.germanistik.uni-rostock.de/lehrende/professorinnen-und-professoren/jun-prof-dr-ulrike-henny-krahmer/" role="SCC Editions Member" institution="Universität Rostock">}}Jun.-Prof. Ulrike Henny-Krahmer{{</team-member>}}
{{<team-member img="gfx/Horstmann-Jan.jpg" url="https://www.ulb.uni-muenster.de/~personal/horstmann/" role="SCC Editions Member" institution="Westfälische Wilhelms-Universität Münster">}}Dr. Jan Horstmann{{</team-member>}}
{{<team-member img="gfx/sahle-patrick.jpg" url="https://www.geschichte.uni-wuppertal.de/de/lehrgebiete/digital-humanities/detail/sahle/" role="SCC Editions Member" institution="Bergische Universität Wuppertal">}}Prof. Dr. Patrick Sahle{{</team-member>}}
{{<team-member img="gfx/Vetter_Angila.jpg" url="https://www.ub.uni-kiel.de/de/kontakt/angila-vetter" role="SCC Editions Member" institution="Universität Augsburg">}}Dr. Angila Vetter{{</team-member>}}
{{<team-member img="gfx/Viehhauser-Gabriel.jpeg" url="https://www.uni-stuttgart.de/universitaet/organisation/personen/Viehhauser/" role="SCC Editions Member" institution="Universität Stuttgart">}}Prof. Dr. Gabriel Viehhauser{{</team-member>}}
{{</team>}}

{{<team name="Scientific Coordination Committee: Lexical Resources">}}
{{<team-member img="gfx/Schroder-Ingrid-Quelle-privat.jpg" url="https://www.slm.uni-hamburg.de/germanistik/personen/schroeder.html" role="Acting SCC Chair Lexical Ressources" institution="Universität Hamburg">}}Prof. Dr. Ingrid Schröder{{</team-member>}}
{{<team-member img="gfx/Geyken-Alexander-Quelle-privat.jpg" url="https://www.bbaw.de/die-akademie/mitarbeiterinnen-mitarbeiter/geyken-alexander" role="Data Domain Speaker Lexical Resources" institution="Berlin-Brandenburgische Akademie der Wissenschaften">}}PD Dr. Alexander Geyken{{</team-member>}}
{{<team-member img="" url="https://www.linglit.tu-darmstadt.de/institutlinglit/mitarbeitende/sabinebartsch/index.de.jsp" role="SCC Lexical Resources Member" institution="Technische Universität Darmstadt">}}Dr. Sabine Bartsch{{</team-member>}}
{{<team-member img="gfx/Heid-Ulrich.jpg" url="https://www.uni-hildesheim.de/fb3/institute/iwist/mitglieder/heid/" role="SCC Lexical Resources Member" institution="Universität Hildesheim">}}Prof. Dr. Ulrich Heid{{</team-member>}}
{{<team-member img="gfx/Nowak-Jessica.png" url="https://www.germanistik.uni-mainz.de/abteilungen/historische-sprachwissenschaft-des-deutschen/jessica-nowak-m-a/" role="SCC Lexical Resources Member" institution="Johannes Gutenberg-Universität Mainz">}}Jun.-Prof. Dr. Jessica Nowak{{</team-member>}}
{{<team-member img="gfx/Osswald-Rainer.jpg" url="https://user.phil.hhu.de/osswald/" role="SCC Lexical Resources Member" institution="Heinrich-Heine-Universität Düsseldorf">}}Dr. Rainer Osswald{{</team-member>}}
{{<team-member img="" url="https://www.katalog.uu.se/profile/?id=N18-2697" role="SCC Lexical Resources Member" institution="Universität Uppsala">}}PD Dr. Michael Prinz{{</team-member>}}
{{<team-member img="" url="https://www.geschkult.fu-berlin.de/e/rod/Team-und-Fellows/assozierte-Forscher/Richter.html" role="SCC Lexical Resources Member" institution="Freie Universität Berlin">}}Prof. Dr. Tonio Sebastian Richter{{</team-member>}}
{{<team-member img="gfx/Selig-Maria.jpg" url="https://www.uni-regensburg.de/sprache-literatur-kultur/romanistik/sprachwissenschaft/selig/" role="SCC Lexical Resources Member" institution="Bayerische Akademie der Wissenschaften">}}Prof. Dr. Maria Selig{{</team-member>}}
{{<team-member img="" url="https://www.slm.uni-hamburg.de/germanistik/personen/zinsmeister.html" role="SCC Lexical Resources Member" institution="Universität Hamburg">}}Prof. Dr. Heike Zinsmeister{{</team-member>}}
{{</team>}}

{{<team name="Operations Coordination Committee" theme="white">}}
{{<team-member img="gfx/Petras-Vivien-Quelle-privat.jpg" url="https://www.ibi.hu-berlin.de/de/ueber-uns/personen/petras" role="Acting OCC Chair" institution="Humboldt-Universität zu Berlin">}}Prof. Vivien Petras, PhD{{</team-member>}}
{{<team-member img="gfx/Wieder-Philipp.jpg" url="https://gwdg.de/research-education/researchgroup_wieder/" role="Operations Speaker" institution="Georg-August-Universität Göttingen, Niedersächsische Staats- und Universitätsbibliothek">}}Prof. Dr. Philipp Wieder{{</team-member>}}
{{<team-member img="gfx/Henrich_Andreas-Quelle-privat.jpg" url="https://www.uni-bamberg.de/minf/team/henrich/" role="Operation Vice Speaker" institution="Otto-Friedrich-Universität Bamberg">}}Prof. Dr. Andreas Henrich{{</team-member>}}
{{<team-member img="gfx/Altenhoner-Reinhard.jpg" url="https://staatsbibliothek-berlin.de/die-staatsbibliothek/abteilungen/generaldirektion/info-sv" role="OCC Member" institution="Staatsbibliothek zu Berlin Preußischer Kulturbesitz">}}Reinhard Altenhöner{{</team-member>}}
{{<team-member img="gfx/Degkwitz-Andreas.jpg" url="https://www.ub.hu-berlin.de/de/ueber-uns/kontakt/ansprechpartner/dr.-andreas-degkwitz" role="OCC Member" institution="Universitätsbibliothek der Humboldt-Universität zu Berlin">}}Prof. Dr. Andreas Degkwitz{{</team-member>}}
{{<team-member img="gfx/eggert_eric.jpg" url="https://thomasinstitut.uni-koeln.de/mitarbeiterinnen/eric-eggert" role="OCC Member" institution="Universität zu Köln">}}Eric Eggert{{</team-member>}}
{{<team-member img="gfx/Gerstenberg-Annette.jpg" url="https://www.uni-potsdam.de/de/romanistik-gerstenberg/unterseiten/equipe/das-team/annette-gerstenberg-prof-dr" role="OCC Member" institution="Universität Potsdam">}}Prof. Dr. Annette Gerstenberg{{</team-member>}}
{{<team-member img="gfx/Herrmann-Sebastian.jpg" url="https://www.philol.uni-leipzig.de/en/institute-for-american-studies/institute/faculty/sebastian-m-herrmann" role="OCC Member" institution="Universität Leipzig">}}Dr. Sebastian Herrmann{{</team-member>}}
{{<team-member img="gfx/Heyer-Gerhard.jpg" url="https://www.saw-leipzig.de/de/mitarbeiter/heyerg" role="OCC Member" institution="Sächsische Akademie der Wissenschaften zu Leipzig">}}Prof. Dr. Gerhard Heyer{{</team-member>}}
{{<team-member img="gfx/Razum-Matthias.png" url="https://www.fiz-karlsruhe.de/de/ueber-uns/ueber-uns#management" role="OCC Member" institution="FIZ Karlsruhe – Leibniz-Institut für Informationsinfrastruktur">}}Matthias Razum{{</team-member>}}
{{<team-member img="gfx/Renner-Westermann_Heike.jpg" url="https://www.linguistik.de/de/about/team/" role="OCC Member" institution="Universität Frankfurt">}}Heike Renner-Westermann{{</team-member>}}
{{<team-member img="gfx/Stein-Regine_SUB.jpg" url="https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/regine-stein/" role="OCC Member" institution="Niedersächsische Staats- und Universitätsbibliothek Göttingen">}}Regine Stein{{</team-member>}}
{{<team-member img="gfx/tepe-markus.jpeg" url="https://uol.de/markus-tepe" role="OCC Member" institution="Carl von Ossietzky Universität Oldenburg">}}Prof. Dr. Markus Tepe{{</team-member>}}
{{<team-member img="gfx/Wieneke-Lars.jpg" url="https://www.c2dh.uni.lu/de/people/lars-wieneke" role="OCC Member" institution="Universität Luxemburg">}}Dr. Lars Wieneke{{</team-member>}}
{{</team>}}
