---
title: Editions

menu:
  main:
    weight: 20
    parent: ueber-uns
---

# Editions

{{<image img="gfx/2023-08-30-Website Editionen-editpiaf.png" license="Theodor Fontane: Notizbücher. Digitale genetisch-kritische und kommentierte Edition. Hrsg. von Gabriele Radecke. https://fontane-nb.dariah.eu"/>}}

Editions are critical representations of historical documents as used in humanities research and beyond. They involve the reliable, method-guided preservation, presentation, and annotation of all types of texts in various languages and writing systems. Among the various editorial models are documentary or diplomatic editions, editions on the genesis of documents, and historical-critical editions.

## Tasks & Community Approach
Members of the Editions data domain in Text+ aim to develop an open infrastructure for the editions community that addresses the perspectives and needs of editors, researchers, teachers, and learners from the planning phase and encourages participation.

Our offering focuses on the following:

### 1. Editions Registry
Central to this is the Editions Registry, a curated, cross-institutional directory of editions regardless of their media form (digital, hybrid, printed). The registry draws from various sources, including databases from funding institutions, library tracking systems, existing catalogs, information from specialist information services, etc. Users are also given the opportunity to input editions, supplement information, or make corrections via a form. The Editions Registry is part of the overarching Text+ Registry that interconnects resources from different data domains. It provides the data contained in it as 'Linked Open Data' (LOD) and offers corresponding interfaces.

### 2. Consulting
Our consulting services include coordinated, personal, and tailored advice—from application advice and questions about research data management to discussing research ideas and processes to specialized questions about all phases of editing. Based on existing competencies and experiences, consultants are selectively chosen for each request. Consulting is integrated into the [central Helpdesk](https://textplus.gwdg.de/contact/) of Text+, but can also be initiated through individual contact. In addition, we will soon offer an open consultation hour.

### 3. Workshops, Tools & Training, and Curricular Recommendations
We offer a broad program of workshops and tutorials and develop curricular recommendations for current and upcoming research standards, technologies, and applications related to editions. Our offering is complemented by a curated platform for edition software and tools.

### 4. Networking, Standardization, and Participation
Partners in the Editions data domain are involved in several consortia and are also members of the NFDI e.V.. Through their institutions, they are engaged in the [NFDI sections](https://www.nfdi.de/sektionen/), bringing the interests and needs of the editions community into consortium-wide structures, services, and offerings of the National Research Data Infrastructure.

## Participating Institutions (As of 2023)

* [Academy of Sciences and Literature Mainz](https://www.adwmainz.de/)
* [Göttingen Academy of Sciences and Humanities](https://adw-goe.de/)
* [Berlin-Brandenburg Academy of Sciences and Humanities](https://www.bbaw.de/)
* [German Academy of Sciences Leopoldina](https://www.leopoldina.org/)
* [Heidelberg Academy of Sciences](https://www.hadw-bw.de/)
* [Duke August Library Wolfenbüttel](https://www.hab.de/)
* [Darmstadt University of Applied Sciences](https://h-da.de/)
* [Classical Foundation Weimar](https://www.klassik-stiftung.de/)
* [Leibniz Institute for European History](https://www.ieg-mainz.de/)
* [Max Weber Foundation – German Humanities Institutes Abroad](https://www.maxweberstiftung.de/)
* [Lower Saxony State and University Library Göttingen](https://www.sub.uni-goettingen.de/sub-aktuell/)
* [North Rhine-Westphalian Academy of Sciences, Humanities and the Arts](https://www.awk.nrw/)
* [Salomon Ludwig Steinheim Institute for German-Jewish History at the University of Duisburg-Essen](http://www.steinheim-institut.de/)
* [Technical University of Darmstadt](https://www.tu-darmstadt.de/)
* [University of Paderborn](https://www.uni-paderborn.de/)
* [University of Rostock](https://www.uni-rostock.de/)
* [University and State Library Darmstadt](https://www.ulb.tu-darmstadt.de/)

### Contact Persons

* Spokesperson: [Andreas Speer](https://cceh.uni-koeln.de/personen/andreas-speer/)
* Coordinator: [Kilian Hensen](https://cceh.uni-koeln.de/personen/kilian-hensen/)

Feel free to also use the [Text+ Helpdesk](/kontakt).
