---
title: Editionen

menu:
  main:
    weight: 20
    parent: ueber-uns


---

# Editionen

{{<image img="gfx/2023-08-30-Website Editionen-editpiaf.png" license="Theodor Fontane: Notizbücher. Digitale genetisch-kritische und kommentierte Edition. Hrsg. von Gabriele Radecke. https://fontane-nb.dariah.eu"/>}}

Editionen sind kritische Repräsentationen historischer Dokumente, wie sie in der geisteswissenschaftlichen Forschung und darüber hinaus verwendet werden. Sie bestehen aus der zuverlässigen methodengeleiteten Bewahrung, Präsentation und Kommentierung aller Arten von Texten in verschiedenen Sprachen und Schriftsystemen. Unter der Vielzahl editorischer Modelle finden sich dokumentarische oder diplomatische Editionen, Editionen zur Entstehungsgeschichte von Dokumenten und historisch-kritische Editionen.


## Aufgaben & Community Approach
Die Mitglieder der Datendomäne Editionen in Text+ haben sich zum Ziel gesetzt, eine offene Infrastruktur für die Editions-Community zu entwickeln, die die Perspektiven und Bedarfe von Editor:innen, Forschenden, Lehrenden und Lernenden bereits in der Planungsphase aufgreift und zum Mitmachen anregt.

Unser Angebot setzt folgende Schwerpunkte:


### 1. Editionsverzeichnis
Im Zentrum steht die Editions Registry, ein kuratiertes, institutionenübergreifendes Verzeichnis von Editionen unabhängig von ihrer medialen Form (digital, hybrid, gedruckt). Die Registry speist sich aus verschiedenen Quellen: neben Datenbanken von Förderinstitutionen, bibliothekarischen Nachweissystemen, bestehenden Katalogen, Informationen von Fachinformationsdiensten etc., wird auch Nutzenden selbst die Möglichkeit gegeben, Editionen über ein Formular einzugeben, Informationen zu ergänzen oder zu korrigieren. Die Editions Registry ist Teil der übergreifenden Text+ Registry, die Ressourcen der verschiedenen Datendomänen miteinander vernetzt. Sie stellt die in ihr enthaltenen Daten als ‚Linked Open Data‘ (LOD) zur Verfügung und bietet entsprechende Schnittstellen an. 


### 2. Consulting
Unser Beratungsangebot umfasst eine koordinierte, persönliche und passgenaue Beratung - von der Antragsberatung über Fragen zum Forschungsdatenmanagement oder die Diskussion von Forschungsideen und-prozessen bis hin zur Spezialfragen zu allen Phasen des Edierens. Anhand der vorhandenen Kompetenzen und Erfahrungen werden die Beratenden für jede Anfrage gezielt ausgewählt. Die Beratung ist in den [zentralen Helpdesk](https://textplus.gwdg.de/contact/) von Text+ eingebunden, kann aber auch über die individuelle Kontaktaufnahme erfolgen. Ergänzend bieten wir in Kürze eine offene Sprechstunde an.


### 3. Workshops, Tools & Trainings und Curriculare Empfehlungen
Wir bieten ein breites Programm an Workshops und Tutorien an und entwickeln curriculare Empfehlungen zu aktuellen und kommenden Forschungsstandards, -technologien und -anwendungen rund um das Thema Editionen. Unser Angebot wird durch eine kuratierte Plattform für Editions-Software und -Tools ergänzt.


### 4. Vernetzung, Standardisierung und Beteiligung
Die Partner der Datendomäne Editionen sind an mehreren Konsortien beteiligt und zudem Mitglieder des NFDI e.V..
Über ihre Institutionen engagieren sie sich dort in den [NFDI Sektionen](https://www.nfdi.de/sektionen/). Auf diese Weise bringen sie die Interessen und Bedarfe der Editions-Community in konsortienübergreifende Strukturen, Dienste und Angebote der Nationalen Forschungsdateninfrastruktur ein.


## Beteiligte Institutionen (Stand: 2023)

* [Akademie der Wissenschaften und der Literatur Mainz](https://www.adwmainz.de/)
* [Niedersächsische Akademie der Wissenschaften zu Göttingen](https://adw-goe.de/)
* [Berlin-Brandenburgische Akademie der Wissenschaften](https://www.bbaw.de/)
* [Deutsche Akademie der Naturforscher Leopoldina – Nationale Akademie der Wissenschaften](https://www.leopoldina.org/)
* [Heidelberger Akademie der Wissenschaften](https://www.hadw-bw.de/)
* [Herzog August Bibliothek Wolfenbüttel](https://www.hab.de/)
* [Hochschule Darmstadt](https://h-da.de/)
* [Klassik Stiftung Weimar](https://www.klassik-stiftung.de/)
* [Leibniz-Institut für Europäische Geschichte](https://www.ieg-mainz.de/)
* [Max Weber Stiftung – Deutsche Geisteswissenschaftliche Institute im Ausland](https://www.maxweberstiftung.de/)
* [Niedersächsische Staats- und Universitätsbibliothek Göttingen](https://www.sub.uni-goettingen.de/)
* [Nordrhein-Westfälische Akademie der Wissenschaften und der Künste](https://www.awk.nrw/)
* [Salomon Ludwig Steinheim-Institut für deutsch-jüdische Geschichte an der Universität Duisburg-Essen](http://www.steinheim-institut.de/)
* [Technische Universität Darmstadt](https://www.tu-darmstadt.de/)
* [Universität Paderborn](https://www.uni-paderborn.de/)
* [Universität Rostock](https://www.uni-rostock.de/)
* [Universitäts- und Landesbibliothek Darmstadt](https://www.ulb.tu-darmstadt.de/)


### Ansprechersonen

* Sprecher: [Andreas Speer](https://cceh.uni-koeln.de/personen/andreas-speer/)
* Koordinator: [Kilian Hensen](https://cceh.uni-koeln.de/personen/kilian-hensen/)

Nutzen Sie auch gern den [Text\+ Helpdesk](/kontakt).
