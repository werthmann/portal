---
title: Infrastructure/Operations

menu:
  main:
    weight: 40
    parent: ueber-uns
---

# Infrastructure/Operations

{{<image img="gfx/Website IO.jpg" license="Generated with Stable Diffusion, prompt by Alex Steckel"/>}}

## Core Questions
* What does Text+ offer me, especially regarding research data management?
* How can I use these offerings?
* Who supports me with questions?
* Where can I find this support?
* What research data can I find at Text+ and what can I use it for?
* Can I submit my own research data to Text+?

This brief list of potential user questions outlines the tasks of the Text+ task area Infrastructure/Operations (IO in short), which coordinates infrastructure development in Text+. What tasks does IO undertake based on these questions?

## Goals
IO pursues the goal of an integrated and coordinated Text+ portfolio consisting of interoperable offerings and services (Data Services, Community Activities, Software Services). IO supports the development of the portfolio with basic services and infrastructure, expertise, and development resources.

The Text+ data domains – Editions, Lexical Resources, and Collections – are thus the primary target audience for IO. IO works with them on cross-cutting topics, the so-called cross domain topics. Cross-cutting topics include helpdesk, research data management planning, web portal, data center integration, registries, search & retrieval, metadata, GND agency Text+, and software services.

## External Connection
Beyond this internal view, IO serves as an interface to the infrastructure developments in the NFDI as a whole and ensures that developments, standards, and specific offerings align with the NFDI. The five sections of the [NFDI Association](https://www.nfdi.de/verein/), collaboration with the humanities and cultural consortia ([NFDI4Culture](https://nfdi4culture.de/de/index.html), [NFDI4Memory](https://4memory.de/), [NFDI4Objects](https://www.nfdi4objects.net/), Text+), and the NFDI basic services initiative [Base4NFDI](https://base4nfdi.de/) play an important role. On the national level and beyond, IO is also connected to relevant research infrastructures in Europe. For Text+, these are particularly the two European Research Infrastructure Consortia [CLARIN](https://www.clarin.eu/) and [DARIAH](https://www.dariah.eu/).

## Tasks
In summary, within Text+, IO is tasked with the following:

* Providing a FAIR-compliant platform for generic services, such as PID service, generic search, schema management services.
* Linking existing and developing data and service portfolios of data domains while adhering to standards and norm data.
* Supporting the linking to or integration of resources and services in the NFDI and beyond (including Base4NFDI).
* Enabling easy access to the service and data offerings of Text+ through user-friendly interfaces and APIs.
* Supporting a reliable and sustainable operation of the Text+ infrastructure.
