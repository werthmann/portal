---
title: Infrastructure/Operations

menu:
  main:
    weight: 40
    parent: ueber-uns
---

# Infrastructure/Operations

{{<image img="gfx/Website IO.jpg" license="Generated with Stable Diffusion, prompt by Alex Steckel"/>}}

## Kernfragen
* Welche Angebote, insbesondere hinsichtlich Forschungsdatenmanagement hat Text+ für mich?
* Wie kann ich die Angebote nutzen?
* Wer unterstützt mich bei Fragen?
* Wo finde ich diese Unterstützung?
* Welche Forschungsdaten finde ich bei Text+ und wofür kann ich sie nutzen?
* Kann ich eigene Forschungsdaten an Text+ übergeben? 

Diese kurze Liste von möglichen Fragestellungen von Nutzenden umreißt das Aufgabenfeld der Text+ Task Area Infrastructure/Operations (kurz IO), welche die Infrastrukturentwicklung in Text+ koordiniert. Welche Aufgaben folgen daraus für IO?

## Ziele
IO verfolgt das Ziel eines integrierten und aufeinander abgestimmten Text+ Angebotsportfolios, das aus interoperablen Angeboten und Diensten (Data Services, Community Activities, Software Services) besteht. Die Entwicklungsarbeiten für das Angebotsportfolio werden von IO mit Basisdiensten und -infrastruktur, Expertise und Entwicklungsressourcen unterstützt.

Die Text+ Datendomänen – Editionen, lexikalische Ressourcen und Sammlungen – sind somit die erste Zielgruppe für IO. Mit ihnen zusammen bearbeitet IO Querschnittsthemen, die sogenannten Cross Domain Topics. Querschnittsthemen sind Helpdesk, Forschungsdatenmanagementplanung, Webportal, Anbindung Datenzentren, Registries, Search & Retrieval, Metadaten, GND-Agentur Text+, Software Services.

## Außenanbindung
Über diese Binnensicht hinaus, fungiert IO als Schnittstelle zu den Infrastruktur-Entwicklungen in der NFDI insgesamt und stellt sicher, dass die Entwicklungen, Standards und konkreten Angebote zur NFDI passen. Hierbei spielen die fünf Sektionen des [NFDI-Vereins](https://www.nfdi.de/verein/), die Zusammenarbeit der geistes- und kulturwissenschaftlichen Konsortien ([NFDI4Culture](https://nfdi4culture.de/de/index.html), [NFDI4Memory](https://4memory.de/), [NFDI4Objects](https://www.nfdi4objects.net/), Text+) sowie die NFDI-Basisdiensteinitiative [Base4NFDI](https://base4nfdi.de/) eine wichtige Rolle. Über die nationale Ebene hinaus ist IO auch mit den einschlägigen Forschungsinfrastrukturen auf europäischer Ebene verbunden. Für Text+ sind dies insbesondere die beiden European Research Infrastructure Consortia [CLARIN](https://www.clarin.eu/) und [DARIAH](https://www.dariah.eu/).

## Aufgaben
Zusammengefasst ist IO innerhalb von Text+ mit den folgenden Aufgaben betraut:

*	Bereitstellung einer FAIR-konform Plattform für generische Dienste, bspw. PID-Service, generische Suche, Schema-Management Services.
*	Verknüpfung existierender und in Entwicklung befindlicher Daten- und Diensteportfolios der Datendomänen unter Beachtung von Standards und Normdaten.
*	Unterstützung bei der Verknüpfung zu bzw. Integration von Ressourcen und Diensten in der NFDI und darüber hinaus (u.a. Base4NFDI).
*	Ermöglichung eines einfachen Zugangs zum Dienste- und Datenangebot von Text+ durch nutzungsfreundlich gestaltete Interfaces und APIs.
*	Unterstützung eines verlässlichen und nachhaltigen Betriebs der Text+ Infrastruktur.