---
title: Collections

menu:
  main:
    weight: 10
    parent: ueber-uns
---

# Collections

{{<image img="gfx/Website Collections.jpg" license="Background image: Pexels (Pixabay), https://pixabay.com/de/users/pexels-2286921"/>}}

The Collections data domain encompasses language and text-based collections of written, spoken, or signed language and texts that were written based on scientific criteria. This includes text collections and corpora, mono- and multimodal recordings of language, as well as language- and text-related experimental or measurement data. The collections are provided in the distributed infrastructure of Text+ by various certified data centers, each with its own specializations.

For knowledge transfer and assistance with questions related to Collections and their integration, the [Text+ Helpdesk](/contact) is the primary contact point. It also serves as a contact address for projects wishing to integrate their research data into the Text+ infrastructure.

The data domain pays special attention to legal and ethical issues, serving as a point of contact within the consortium and beyond. For instance, derived text formats that enable the exploration of copyrighted resources without violating copyright are a particular focus. Other priorities include the community-based development of software services and the topic of Linked Open Data.

## Contact Persons
The spokesperson for the Collections data domain is [Dr. Peter Leinen](mailto:p.leinen@dnb.de), and it is coordinated by [Philippe Genêt](mailto:p.genet@dnb.de). Both work at the [German National Library](https://www.dnb.de/).

## Participating Institutions
- [Academy of Sciences in Hamburg](https://www.awhamburg.de/)
- [Albert Ludwig University of Freiburg](https://www.uni-freiburg.de/)
- [Bavarian Academy of Sciences](https://www.badw.de/die-akademie.html)
- [Berlin-Brandenburg Academy of Sciences and Humanities](http://www.bbaw.de/)
- [German National Library](https://www.dnb.de/)
- [German Literature Archive Marbach](https://www.dla-marbach.de/)
- [Eberhard Karls University of Tübingen](https://uni-tuebingen.de/)
- [GWDG - Gesellschaft für wissenschaftliche Datenverarbeitung mbH Göttingen](https://www.gwdg.de/)
- [Heidelberg Academy of Sciences](https://www.hadw-bw.de/)
- [Julius Maximilian University of Würzburg](https://www.uni-wuerzburg.de/startseite/)
- [Classical Foundation Weimar](https://www.klassik-stiftung.de/)
- [Leibniz Institute for the German Language, Mannheim](https://www1.ids-mannheim.de/)
- [Ludwig Maximilian University of Munich](https://www.uni-muenchen.de/index.html)
- [Lower Saxony State and University Library Göttingen](https://www.sub.uni-goettingen.de/sub-aktuell/)
- [University of Saarland](https://www.uni-saarland.de/start.html)
- [University of Duisburg-Essen](https://www.uni-due.de/)
- [University of Hamburg](https://www.uni-hamburg.de/)
- [University of Trier](https://www.uni-trier.de/)
- [University of Cologne](https://www.uni-koeln.de/)
